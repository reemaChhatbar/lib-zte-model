package com.tantaluminnovations.zte.model;

public class DTC {

	private String code;
	private Integer state;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getDtcCodeAsString(){
		return "P" + code;
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		DTC dtc = (DTC) o;

		if (code != null ? !code.equals(dtc.code) : dtc.code != null)
			return false;
		return state != null ? state.equals(dtc.state) : dtc.state == null;
	}

	@Override public int hashCode() {
		int result = code != null ? code.hashCode() : 0;
		result = 31 * result + (state != null ? state.hashCode() : 0);
		return result;
	}
}
