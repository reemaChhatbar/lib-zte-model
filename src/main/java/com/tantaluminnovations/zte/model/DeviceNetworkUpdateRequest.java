package com.tantaluminnovations.zte.model;

import java.util.HashMap;
import java.util.Map;

public class DeviceNetworkUpdateRequest {
	
	private String internalMac;
	private String externalMac;
	private String ssid;
	private String ssidPassword;

	public String getInternalMac() {
		return internalMac;
	}

	public void setInternalMac(String internalMac) {
		this.internalMac = internalMac;
	}

	public String getExternalMac() {
		return externalMac;
	}

	public void setExternalMac(String externalMac) {
		this.externalMac = externalMac;
	}

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public String getSsidPassword() {
		return ssidPassword;
	}

	public void setSsidPassword(String ssidPassword) {
		this.ssidPassword = ssidPassword;
	}
	
	public Map<String, String> getAsMap(){
		HashMap<String, String> map = new HashMap<>();
		if(internalMac != null) map.put("internalMac", internalMac);
		if(externalMac != null) map.put("externalMac", externalMac);
		if(ssid != null) map.put("ssid", ssid);
		if(ssidPassword != null) map.put("ssidPassword", ssidPassword);
		return map;
	}
}
