package com.tantaluminnovations.zte.model;

import java.util.*;
import java.util.stream.*;

public enum ParameterType {
	UNKNOWN(-0x1, 0, Void.class),
	FACTORY_RESET(0x0, 2, Integer.class),
	RAPID_ACCELERATION_THRESHOLD(0x1, 1, Double.class, 0.1),
	RAPID_DECELERATION_THRESHOLD(0x2, 1, Double.class, 0.1),
	SHARP_TURN_THRESHOLD(0x3, 1, Double.class, 0.01),
	FATIGUE_DRIVING_SECONDS(0x4, 2, Integer.class),
	OVER_SPEED_KMpH_THRESHOLD(0x5, 1, Integer.class),
	LOW_VOLTAGE_THRESHOLD(0x6, 1, Double.class, 0.1),
	WAKE_VIBRATION_LEVEL(0x7, 1, Integer.class),
	SUSPECTED_COLLISION_THRESHOLD(0x8, 1, Double.class, 0.1),
	EXCESSIVE_IDLE_SECONDS(0x9, 2, Integer.class),
	REPORT_SPEED(0xA, 1, Boolean.class),
	GPS_REPORT_FREQUENCY_SECONDS(0xB, 2, Integer.class),
	//Only in message spec V2.0+
	VEHICLE_DATA_REPORT_FREQUENCY_SECONDS(0xC, 2, Integer.class),
	WAKEUP_DISTURBANCE_REPORT_TIME_SECONDS(0xD, 2, Integer.class),
	HEARTBEAT_FREQUENCY_HOURS(0xE, 2, Integer.class),
	CAR_EMISSIONS_LITRES(0xF, 2, Double.class, 0.1),
	ENABLE_OBD(0x10, 2, Integer.class),
	//End of V2.0+ messages
	CHANGE_DEVICE_IP_AND_PORT(0x200, 40, String.class),
	CHANGE_WIFI_IP_AND_SUBNET(0x201, null, String.class),
	CHANGE_WIFI_SSID(0x202, null, String.class),
	CHANGE_WIFI_PASSWORD(0x203, null, String.class),
	CHANGE_WIFI_ROUTER_APN(0x204, null, String.class),
	CHANGE_MODEM_DNS_SERVERS(0x205, null, String.class),
	CHANGE_MODEM_APN(0x206, null, String.class),
	CHANGE_WIFI_ACCESS(0x207, 1, Integer.class),
	CHANGE_INQUIRY_NETWORK_TYPE(0x208, 1, Integer.class),
	CHANGE_INQUIRY_OPERATOR_NAME(0x209, null, String.class),
	CHANGE_INQUIRY_SIGNAL_STRENGTH(0x20A, 1, Integer.class);


	private final int parameterId;
	private final Integer valueLength;
	private final Class valueType;
	private final Double valuePrecision;

	ParameterType(int parameterId, Integer valueLength, Class valueType) {
		this(parameterId, valueLength, valueType, null);
	}

	ParameterType(int parameterId, Integer valueLength, Class valueType, Double valuePrecision) {
		this.parameterId = parameterId;
		this.valueLength = valueLength;
		this.valueType = valueType;
		this.valuePrecision = valuePrecision;
		CodeMap.INSTANCE.put(this);
	}

	public int getParameterId() {
		return parameterId;
	}

	public int getValueLength() {
		return valueLength;
	}

	public Class getValueType() {
		return valueType;
	}

	public Double getValuePrecision() {
		return valuePrecision;
	}

	public static ParameterType fromParameterId(int code) {
		ParameterType value = ParameterType.CodeMap.INSTANCE.get(Integer.valueOf(code));
		return value != null?value:UNKNOWN;
	}

	public static ParameterType safeValueOf(String value) {
		try {
			return valueOf(value);
		} catch (IllegalArgumentException e){
			return UNKNOWN;
		}
	}

	public ZteDeviceParameter permanentParameter(Object value) {
		ZteDeviceParameter zteDeviceParameter = new ZteDeviceParameter();
		zteDeviceParameter.setType(this);
		if(value.getClass().isAssignableFrom(this.getValueType())){
			zteDeviceParameter.setValue(value);
		} else if (value.getClass() == String.class) {
			String valueAsString = (String) value;
			if(this.getValueType() == Boolean.class){
				zteDeviceParameter.setValue(Boolean.valueOf(valueAsString));
			} else if(this.getValueType() == Byte.class){
				zteDeviceParameter.setValue(Byte.parseByte(valueAsString));
			} else if(this.getValueType() == Short.class){
				zteDeviceParameter.setValue(Short.parseShort(valueAsString));
			} else if(this.getValueType() == Integer.class){
				zteDeviceParameter.setValue(Integer.parseInt(valueAsString));
			} else if(this.getValueType() == Long.class){
				zteDeviceParameter.setValue(Long.parseLong(valueAsString));
			} else if(this.getValueType() == Double.class){
				zteDeviceParameter.setValue(Double.parseDouble(valueAsString));
			} else if(this.getValueType() == Float.class){
				zteDeviceParameter.setValue(Float.parseFloat(valueAsString));
			} else {
				return null;
			}
		} else {
			return null;
		}
		zteDeviceParameter.setState(0);
		return zteDeviceParameter;
	};

	public static List<ParameterType> allParameters(){
		return CodeMap.INSTANCE.allParameters();
	}

	public static List<ParameterType> policyParameters(){
		return Arrays.asList(
			RAPID_ACCELERATION_THRESHOLD,
			RAPID_DECELERATION_THRESHOLD,
			SHARP_TURN_THRESHOLD,
			FATIGUE_DRIVING_SECONDS,
			OVER_SPEED_KMpH_THRESHOLD,
			LOW_VOLTAGE_THRESHOLD,
			WAKE_VIBRATION_LEVEL,
			SUSPECTED_COLLISION_THRESHOLD,
			EXCESSIVE_IDLE_SECONDS,
			REPORT_SPEED,
			GPS_REPORT_FREQUENCY_SECONDS,
			VEHICLE_DATA_REPORT_FREQUENCY_SECONDS,
			WAKEUP_DISTURBANCE_REPORT_TIME_SECONDS,
			HEARTBEAT_FREQUENCY_HOURS,
			CAR_EMISSIONS_LITRES,
			ENABLE_OBD);
	}

	private enum CodeMap {
		INSTANCE;

		private Map<Integer, ParameterType> codeMap = new HashMap();

		private CodeMap() {
		}

		private void put(ParameterType value) {
			this.codeMap.put(Integer.valueOf(value.getParameterId()), value);
		}

		private ParameterType get(Integer key) {
			return (ParameterType)this.codeMap.get(key);
		}

		public List<ParameterType> allParameters() {
			return this.codeMap.values().stream().collect(Collectors.toList());
		}
	}
}
