package com.tantaluminnovations.zte.model;

public class ZteDeviceParameter {

	private int state;
	private ParameterType type;
	private int timeSec;
	private Object value;

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getType() {
		return type.toString();
	}

	public void setType(ParameterType type) {
		this.type = type;
	}

	public int getTimeSec() {
		return timeSec;
	}

	public void setTimeSec(int timeSec) {
		this.timeSec = timeSec;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public int getValueLength(){
		if(type != null){
			if (type.getValueType() == String.class && value != null) {
				return 1 + ((String) value).length();
			} else {
				return type.getValueLength();
			}
		}
		return 0;
	}

	public int getLength() {
		return 4 + getValueLength();
	}

	public int getParameterId() {
		return type.getParameterId();
	}

	public Class getValueClass() {
		return type.getValueType();
	}

	public Double getValuePrecision() {
		return type.getValuePrecision();
	}
}
