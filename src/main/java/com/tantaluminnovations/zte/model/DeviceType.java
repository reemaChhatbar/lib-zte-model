package com.tantaluminnovations.zte.model;

import java.util.Arrays;

public enum DeviceType {
	BRONZE, COPPER, SAMSUNG, CALAMP, ZTE;

	public static DeviceType safeValueOf(String device_type) {
		return (DeviceType) Arrays.stream(values()).filter((t) -> {
			return t.name().equalsIgnoreCase(device_type);
		}).findAny().orElseGet(() -> {
			return BRONZE;
		});
	}
}
