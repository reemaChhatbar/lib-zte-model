package com.tantaluminnovations.zte.model;

public enum MessageStatus {
	PENDING, SENT, UPDATED, FAILED, NONAPPLICABLE;
}
