package com.tantaluminnovations.zte.model;

public class ChangeStatusRequest {

	private MessageStatus messageStatus;

	public MessageStatus getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(MessageStatus messageStatus) {
		this.messageStatus = messageStatus;
	}

	@Override public String toString() {
		return "ChangeStatusRequest{" +
			   "messageStatus=" + messageStatus +
			   '}';
	}
}
