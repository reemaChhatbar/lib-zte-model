package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class UpgradeStatusPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private UpgradeState upgradeStatus;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public UpgradeState getUpgradeStatus() {
		return upgradeStatus;
	}

	public void setUpgradeStatus(UpgradeState upgradeStatus) {
		this.upgradeStatus = upgradeStatus;
	}

	public void setUpgradeStatus(int code) {
		this.upgradeStatus = UpgradeState.fromCode(code);
	}

	@Override public String toString() {
		return "UpgradeStatusPayload{" +
			   "reportTime=" + reportTime +
			   ", upgradeStatus=" + upgradeStatus +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UpgradeStatusPayload that = (UpgradeStatusPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		return upgradeStatus == that.upgradeStatus;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (upgradeStatus != null ? upgradeStatus.hashCode() : 0);
		return result;
	}

	public enum UpgradeState{
		UNKNOWN(-0x1),
		SUCCESS(0x0),
		DOWNLOADED(0x1),
		FAILURE_CHECKSUM_FRAME(0x11),
		FAILURE_CHECKSUM_UPDATE(0x12),
		FAILURE_FILENAME(0x21),
		FAILURE_FILE_LENGTH(0x22),
		FAILURE_IMCOMPATIBLE_HARDWARE(0x31),
		FAILURE_TIMEOUT(0x32);


		private final int code;

		public int getCode() {
			return code;
		}

		UpgradeState(int code) {
			this.code = code;
		}

		public static UpgradeState fromCode(int code){

			for (UpgradeState state: UpgradeState.values()) {
				if (state.getCode() == code){
					return state;
				}
			}
			return UNKNOWN;
		}


	}
}
