package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class IccidPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private String iccid;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public String getIccid() {
		return iccid;
	}

	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	@Override public String toString() {
		return "IccidPayload{" +
			   "reportTime=" + reportTime +
			   ", iccid='" + iccid + '\'' +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		IccidPayload that = (IccidPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		return iccid != null ? iccid.equals(that.iccid) : that.iccid == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (iccid != null ? iccid.hashCode() : 0);
		return result;
	}
}
