package com.tantaluminnovations.zte.model.payload;

public class VehicleDataFlowPayload extends ZteEventPayload {

	private Integer dataNumber;
	private Integer rpm;
	private Integer speedKmH;
	private Boolean milState;
	private Integer milDistanceKm;
	private Integer historicalDistanceMetres;
	private Integer historicalFuelUsedMl;
	private Integer historicalDrivingTimeSecs;
	private Integer engineCoolantTemperatureCelsius;
	private Double throttlePosition;
	private Double engineDutyPercentage;
	private Double intakeAirFlowGramsPerSecond;
	private Integer intakeAirTemperatureCelsius;
	private Integer intakeAirPressureKPA;
	private Double batteryVoltage;
	private Double fuelLevelInput;
	private Integer noDtcsDistanceTravelledKM;

	public Integer getDataNumber() {
		return dataNumber;
	}

	public void setDataNumber(Integer dataNumber) {
		this.dataNumber = dataNumber;
	}

	public Integer getRpm() {
		return rpm;
	}

	public void setRpm(Integer rpm) {
		this.rpm = rpm;
	}

	public Integer getSpeedKmH() {
		return speedKmH;
	}

	public void setSpeedKmH(Integer speedKmH) {
		this.speedKmH = speedKmH;
	}

	public Boolean getMilState() {
		return milState;
	}

	public void setMilState(Boolean milState) {
		this.milState = milState;
	}

	public Integer getMilDistanceKm() {
		return milDistanceKm;
	}

	public void setMilDistanceKm(Integer milDistanceKm) {
		this.milDistanceKm = milDistanceKm;
	}

	public Integer getHistoricalDistanceMetres() {
		return historicalDistanceMetres;
	}

	public void setHistoricalDistanceMetres(Integer historicalDistanceMetres) {
		this.historicalDistanceMetres = historicalDistanceMetres;
	}

	public Integer getHistoricalFuelUsedMl() {
		return historicalFuelUsedMl;
	}

	public void setHistoricalFuelUsedMl(Integer historicalFuelUsedMl) {
		this.historicalFuelUsedMl = historicalFuelUsedMl;
	}

	public Integer getHistoricalDrivingTimeSecs() {
		return historicalDrivingTimeSecs;
	}

	public void setHistoricalDrivingTimeSecs(Integer historicalDrivingTimeSecs) {
		this.historicalDrivingTimeSecs = historicalDrivingTimeSecs;
	}

	public Integer getEngineCoolantTemperatureCelsius() {
		return engineCoolantTemperatureCelsius;
	}

	public void setEngineCoolantTemperatureCelsius(Integer engineCoolantTemperatureCelsius) {
		this.engineCoolantTemperatureCelsius = engineCoolantTemperatureCelsius;
	}

	public Double getThrottlePosition() {
		return throttlePosition;
	}

	public void setThrottlePosition(Double throttlePosition) {
		this.throttlePosition = throttlePosition;
	}

	public Double getEngineDutyPercentage() {
		return engineDutyPercentage;
	}

	public void setEngineDutyPercentage(Double engineDutyPercentage) {
		this.engineDutyPercentage = engineDutyPercentage;
	}

	public Double getIntakeAirFlowGramsPerSecond() {
		return intakeAirFlowGramsPerSecond;
	}

	public void setIntakeAirFlowGramsPerSecond(Double intakeAirFlowGramsPerSecond) {
		this.intakeAirFlowGramsPerSecond = intakeAirFlowGramsPerSecond;
	}

	public Integer getIntakeAirTemperatureCelsius() {
		return intakeAirTemperatureCelsius;
	}

	public void setIntakeAirTemperatureCelsius(Integer intakeAirTemperatureCelsius) {
		this.intakeAirTemperatureCelsius = intakeAirTemperatureCelsius;
	}

	public Integer getIntakeAirPressureKPA() {
		return intakeAirPressureKPA;
	}

	public void setIntakeAirPressureKPA(Integer intakeAirPressureKPA) {
		this.intakeAirPressureKPA = intakeAirPressureKPA;
	}

	public Double getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(Double batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	public Double getFuelLevelInput() {
		return fuelLevelInput;
	}

	public void setFuelLevelInput(Double fuelLevelInput) {
		this.fuelLevelInput = fuelLevelInput;
	}

	public Integer getNoDtcsDistanceTravelledKM() {
		return noDtcsDistanceTravelledKM;
	}

	public void setNoDtcsDistanceTravelledKM(Integer noDtcsDistanceTravelledKM) {
		this.noDtcsDistanceTravelledKM = noDtcsDistanceTravelledKM;
	}

	@Override public String toString() {
		return "VehicleDataFlowPayload{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", dataNumber=" + dataNumber +
			   ", rpm=" + rpm +
			   ", speedKmH=" + speedKmH +
			   ", milState=" + milState +
			   ", milDistanceKm=" + milDistanceKm +
			   ", historicalDistanceMetres=" + historicalDistanceMetres +
			   ", historicalFuelUsedMl=" + historicalFuelUsedMl +
			   ", historicalDrivingTimeSecs=" + historicalDrivingTimeSecs +
			   ", engineCoolantTemperatureCelsius=" + engineCoolantTemperatureCelsius +
			   ", throttlePosition=" + throttlePosition +
			   ", engineDutyPercentage=" + engineDutyPercentage +
			   ", intakeAirFlowGramsPerSecond=" + intakeAirFlowGramsPerSecond +
			   ", intakeAirTemperatureCelsius=" + intakeAirTemperatureCelsius +
			   ", intakeAirPressureKPA=" + intakeAirPressureKPA +
			   ", batteryVoltage=" + batteryVoltage +
			   ", fuelLevelInput=" + fuelLevelInput +
			   ", noDtcsDistanceTravelledKM=" + noDtcsDistanceTravelledKM +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		VehicleDataFlowPayload that = (VehicleDataFlowPayload) o;

		if (dataNumber != null ? !dataNumber.equals(that.dataNumber) : that.dataNumber != null)
			return false;
		if (rpm != null ? !rpm.equals(that.rpm) : that.rpm != null)
			return false;
		if (speedKmH != null ? !speedKmH.equals(that.speedKmH) : that.speedKmH != null)
			return false;
		if (milState != null ? !milState.equals(that.milState) : that.milState != null)
			return false;
		if (milDistanceKm != null ? !milDistanceKm.equals(that.milDistanceKm) : that.milDistanceKm != null)
			return false;
		if (historicalDistanceMetres != null ?
				!historicalDistanceMetres.equals(that.historicalDistanceMetres) :
				that.historicalDistanceMetres != null)
			return false;
		if (historicalFuelUsedMl != null ?
				!historicalFuelUsedMl.equals(that.historicalFuelUsedMl) :
				that.historicalFuelUsedMl != null)
			return false;
		if (historicalDrivingTimeSecs != null ?
				!historicalDrivingTimeSecs.equals(that.historicalDrivingTimeSecs) :
				that.historicalDrivingTimeSecs != null)
			return false;
		if (engineCoolantTemperatureCelsius != null ?
				!engineCoolantTemperatureCelsius.equals(that.engineCoolantTemperatureCelsius) :
				that.engineCoolantTemperatureCelsius != null)
			return false;
		if (throttlePosition != null ? !throttlePosition.equals(that.throttlePosition) : that.throttlePosition != null)
			return false;
		if (engineDutyPercentage != null ?
				!engineDutyPercentage.equals(that.engineDutyPercentage) :
				that.engineDutyPercentage != null)
			return false;
		if (intakeAirFlowGramsPerSecond != null ?
				!intakeAirFlowGramsPerSecond.equals(that.intakeAirFlowGramsPerSecond) :
				that.intakeAirFlowGramsPerSecond != null)
			return false;
		if (intakeAirTemperatureCelsius != null ?
				!intakeAirTemperatureCelsius.equals(that.intakeAirTemperatureCelsius) :
				that.intakeAirTemperatureCelsius != null)
			return false;
		if (intakeAirPressureKPA != null ?
				!intakeAirPressureKPA.equals(that.intakeAirPressureKPA) :
				that.intakeAirPressureKPA != null)
			return false;
		if (batteryVoltage != null ? !batteryVoltage.equals(that.batteryVoltage) : that.batteryVoltage != null)
			return false;
		if (fuelLevelInput != null ? !fuelLevelInput.equals(that.fuelLevelInput) : that.fuelLevelInput != null)
			return false;
		return noDtcsDistanceTravelledKM != null ?
				noDtcsDistanceTravelledKM.equals(that.noDtcsDistanceTravelledKM) :
				that.noDtcsDistanceTravelledKM == null;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (dataNumber != null ? dataNumber.hashCode() : 0);
		result = 31 * result + (rpm != null ? rpm.hashCode() : 0);
		result = 31 * result + (speedKmH != null ? speedKmH.hashCode() : 0);
		result = 31 * result + (milState != null ? milState.hashCode() : 0);
		result = 31 * result + (milDistanceKm != null ? milDistanceKm.hashCode() : 0);
		result = 31 * result + (historicalDistanceMetres != null ? historicalDistanceMetres.hashCode() : 0);
		result = 31 * result + (historicalFuelUsedMl != null ? historicalFuelUsedMl.hashCode() : 0);
		result = 31 * result + (historicalDrivingTimeSecs != null ? historicalDrivingTimeSecs.hashCode() : 0);
		result = 31 * result + (engineCoolantTemperatureCelsius != null ?
				engineCoolantTemperatureCelsius.hashCode() :
				0);
		result = 31 * result + (throttlePosition != null ? throttlePosition.hashCode() : 0);
		result = 31 * result + (engineDutyPercentage != null ? engineDutyPercentage.hashCode() : 0);
		result = 31 * result + (intakeAirFlowGramsPerSecond != null ? intakeAirFlowGramsPerSecond.hashCode() : 0);
		result = 31 * result + (intakeAirTemperatureCelsius != null ? intakeAirTemperatureCelsius.hashCode() : 0);
		result = 31 * result + (intakeAirPressureKPA != null ? intakeAirPressureKPA.hashCode() : 0);
		result = 31 * result + (batteryVoltage != null ? batteryVoltage.hashCode() : 0);
		result = 31 * result + (fuelLevelInput != null ? fuelLevelInput.hashCode() : 0);
		result = 31 * result + (noDtcsDistanceTravelledKM != null ? noDtcsDistanceTravelledKM.hashCode() : 0);
		return result;
	}
}
