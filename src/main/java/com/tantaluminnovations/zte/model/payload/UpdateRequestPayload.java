package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;
import java.util.*;

public class UpdateRequestPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private String fileName;
	private Integer fileStartPosition;
	private byte[] filePortion;
	private Integer requestLength;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getFileStartPosition() {
		return fileStartPosition;
	}

	public void setFileStartPosition(Integer fileStartPosition) {
		this.fileStartPosition = fileStartPosition;
	}

	public byte[] getFilePortion() {
		return filePortion;
	}

	public void setFilePortion(byte[] filePortion) {
		this.filePortion = filePortion;
	}

	public Integer getRequestLength() {
		return requestLength;
	}

	public void setRequestLength(Integer requestLength) {
		this.requestLength = requestLength;
	}

	@Override public String toString() {
		return "UpdateRequestPayload{" +
			   "reportTime=" + reportTime +
			   ", fileName='" + fileName + '\'' +
			   ", fileStartPosition=" + fileStartPosition +
			   ", filePortion=" + Arrays.toString(filePortion) +
			   ", requestLength=" + requestLength +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UpdateRequestPayload that = (UpdateRequestPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		if (fileName != null ? !fileName.equals(that.fileName) : that.fileName != null)
			return false;
		if (fileStartPosition != null ?
				!fileStartPosition.equals(that.fileStartPosition) :
				that.fileStartPosition != null)
			return false;
		if (!Arrays.equals(filePortion, that.filePortion))
			return false;
		return requestLength != null ? requestLength.equals(that.requestLength) : that.requestLength == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
		result = 31 * result + (fileStartPosition != null ? fileStartPosition.hashCode() : 0);
		result = 31 * result + Arrays.hashCode(filePortion);
		result = 31 * result + (requestLength != null ? requestLength.hashCode() : 0);
		return result;
	}
}
