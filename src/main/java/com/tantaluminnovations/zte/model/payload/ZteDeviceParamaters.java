package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.util.*;

public class ZteDeviceParamaters implements ZteExtendedPayload {

	List<ZteDeviceParameter> zteDeviceParameterList;

	public List<ZteDeviceParameter> getZteDeviceParameterList() {
		return zteDeviceParameterList;
	}

	public void setZteDeviceParameterList(
			List<ZteDeviceParameter> zteDeviceParameterList) {
		this.zteDeviceParameterList = zteDeviceParameterList;
	}

	@Override public String toString() {
		return "ZteDeviceParamaters{" +
			   "zteDeviceParameterList=" + zteDeviceParameterList +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ZteDeviceParamaters that = (ZteDeviceParamaters) o;

		return zteDeviceParameterList != null ?
				zteDeviceParameterList.equals(that.zteDeviceParameterList) :
				that.zteDeviceParameterList == null;
	}

	@Override public int hashCode() {
		return zteDeviceParameterList != null ? zteDeviceParameterList.hashCode() : 0;
	}
}
