package com.tantaluminnovations.zte.model.payload;

public class RapidAccelDecelPayload extends ZteEventPayload {

	private Integer initialSpeedKmH;
	private Integer finalSpeedKmH;
	private Double accelerationMpS2;

	public RapidAccelDecelPayload(ZteEventPayload eventPayload) {
		super(eventPayload);
	}

	public RapidAccelDecelPayload() {
	}

	public Integer getInitialSpeedKmH() {
		return initialSpeedKmH;
	}

	public void setInitialSpeedKmH(Integer initialSpeedKmH) {
		this.initialSpeedKmH = initialSpeedKmH;
	}

	public Integer getFinalSpeedKmH() {
		return finalSpeedKmH;
	}

	public void setFinalSpeedKmH(Integer finalSpeedKmH) {
		this.finalSpeedKmH = finalSpeedKmH;
	}

	public Double getAccelerationMpS2() {
		return accelerationMpS2;
	}

	public void setAccelerationMpS2(Double accelerationMpS2) {
		this.accelerationMpS2 = accelerationMpS2;
	}

	@Override public String toString() {
		return "RapidAccelDecelPayload{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", initialSpeedKmH=" + initialSpeedKmH +
			   ", finalSpeedKmH=" + finalSpeedKmH +
			   ", accelerationMpS2=" + accelerationMpS2 +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		RapidAccelDecelPayload that = (RapidAccelDecelPayload) o;

		if (initialSpeedKmH != null ? !initialSpeedKmH.equals(that.initialSpeedKmH) : that.initialSpeedKmH != null)
			return false;
		if (finalSpeedKmH != null ? !finalSpeedKmH.equals(that.finalSpeedKmH) : that.finalSpeedKmH != null)
			return false;
		return accelerationMpS2 != null ?
				accelerationMpS2.equals(that.accelerationMpS2) :
				that.accelerationMpS2 == null;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (initialSpeedKmH != null ? initialSpeedKmH.hashCode() : 0);
		result = 31 * result + (finalSpeedKmH != null ? finalSpeedKmH.hashCode() : 0);
		result = 31 * result + (accelerationMpS2 != null ? accelerationMpS2.hashCode() : 0);
		return result;
	}
}
