package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class ImsiPayload implements ZteExtendedPayload{

	private Instant reportTime;
	private String imsi;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	@Override public String toString() {
		return "ImsiPayload{" +
			   "reportTime=" + reportTime +
			   ", imsi='" + imsi + '\'' +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ImsiPayload that = (ImsiPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		return imsi != null ? imsi.equals(that.imsi) : that.imsi == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (imsi != null ? imsi.hashCode() : 0);
		return result;
	}
}
