package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

public class SetVehicleInfoResponse implements ZteExtendedPayload {

	private boolean updateSuccessful;

	public boolean isUpdateSuccessful() {
		return updateSuccessful;
	}

	public void setUpdateSuccessful(boolean updateSuccessful) {
		this.updateSuccessful = updateSuccessful;
	}

	@Override public String toString() {
		return "SetVehicleInfoResponse{" +
			   "updateSuccessful=" + updateSuccessful +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		SetVehicleInfoResponse that = (SetVehicleInfoResponse) o;

		return updateSuccessful == that.updateSuccessful;
	}

	@Override public int hashCode() {
		return (updateSuccessful ? 1 : 0);
	}
}
