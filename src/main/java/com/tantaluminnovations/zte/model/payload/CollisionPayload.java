package com.tantaluminnovations.zte.model.payload;

public class CollisionPayload extends ZteEventPayload {

	private Double magnitudeG;

	public Double getMagnitudeG() {
		return magnitudeG;
	}

	public void setMagnitudeG(Double magnitudeG) {
		this.magnitudeG = magnitudeG;
	}

	@Override public String toString() {
		return "CollisionPayload{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", magnitudeG=" + magnitudeG +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		CollisionPayload that = (CollisionPayload) o;

		return magnitudeG != null ? magnitudeG.equals(that.magnitudeG) : that.magnitudeG == null;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (magnitudeG != null ? magnitudeG.hashCode() : 0);
		return result;
	}
}
