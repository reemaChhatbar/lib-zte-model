package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;
import java.util.*;

public class DTCPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private Integer obdDtcCount;
	private List<DTC> obdDtcs;
	private Integer privateDtcCount;
	private List<DTC> privateDtcs;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public Integer getObdDtcCount() {
		return obdDtcCount;
	}

	public void setObdDtcCount(Integer obdDtcCount) {
		this.obdDtcCount = obdDtcCount;
	}

	public List<DTC> getObdDtcs() {
		return obdDtcs;
	}

	public void setObdDtcs(List<DTC> obdDtcs) {
		this.obdDtcs = obdDtcs;
	}

	public Integer getPrivateDtcCount() {
		return privateDtcCount;
	}

	public void setPrivateDtcCount(Integer privateDtcCount) {
		this.privateDtcCount = privateDtcCount;
	}

	public List<DTC> getPrivateDtcs() {
		return privateDtcs;
	}

	public void setPrivateDtcs(List<DTC> privateDtcs) {
		this.privateDtcs = privateDtcs;
	}

	@Override public String toString() {
		return "DTCPayload{" +
			   "reportTime=" + reportTime +
			   ", obdDtcCount=" + obdDtcCount +
			   ", obdDtcs=" + obdDtcs +
			   ", privateDtcCount=" + privateDtcCount +
			   ", privateDtcs=" + privateDtcs +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		DTCPayload that = (DTCPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		if (obdDtcCount != null ? !obdDtcCount.equals(that.obdDtcCount) : that.obdDtcCount != null)
			return false;
		if (obdDtcs != null ? !obdDtcs.equals(that.obdDtcs) : that.obdDtcs != null)
			return false;
		if (privateDtcCount != null ? !privateDtcCount.equals(that.privateDtcCount) : that.privateDtcCount != null)
			return false;
		return privateDtcs != null ? privateDtcs.equals(that.privateDtcs) : that.privateDtcs == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (obdDtcCount != null ? obdDtcCount.hashCode() : 0);
		result = 31 * result + (obdDtcs != null ? obdDtcs.hashCode() : 0);
		result = 31 * result + (privateDtcCount != null ? privateDtcCount.hashCode() : 0);
		result = 31 * result + (privateDtcs != null ? privateDtcs.hashCode() : 0);
		return result;
	}
}
