package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.util.*;

public class SetParametersResponse implements ZteExtendedPayload {

	private final Map<Integer, Boolean> parameterUpdateStatuses;

	public SetParametersResponse() {
		parameterUpdateStatuses = new HashMap<>();
	}

	public void put(Integer key, Boolean success){
		parameterUpdateStatuses.put(key, success);
	}

	public Map<Integer, Boolean> getParameterUpdateStatuses() {
		return parameterUpdateStatuses;
	}
}
