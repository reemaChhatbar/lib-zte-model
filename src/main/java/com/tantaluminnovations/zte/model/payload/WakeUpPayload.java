package com.tantaluminnovations.zte.model.payload;

public class WakeUpPayload extends SleepPayload{

	private int typeCode;

	public int getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(int typeCode) {
		this.typeCode = typeCode;
	}

	@Override public String toString() {
		return "WakeUpPayload{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", typeCode=" + typeCode +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		WakeUpPayload that = (WakeUpPayload) o;

		return typeCode == that.typeCode;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + typeCode;
		return result;
	}
}
