package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class DeviceInfoPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private String phone;
	private String iccid;
	private String imsi;
	private String wifiMac;
	private String btMac;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIccid() {
		return iccid;
	}

	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getWifiMac() {
		return wifiMac;
	}

	public void setWifiMac(String wifiMac) {
		this.wifiMac = wifiMac;
	}

	public String getBtMac() {
		return btMac;
	}

	public void setBtMac(String btMac) {
		this.btMac = btMac;
	}

	@Override public String toString() {
		return "DeviceInfoPayload{" +
			   "reportTime=" + reportTime +
			   ", phone='" + phone + '\'' +
			   ", iccid='" + iccid + '\'' +
			   ", imsi='" + imsi + '\'' +
			   ", wifiMac='" + wifiMac + '\'' +
			   ", btMac='" + btMac + '\'' +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		DeviceInfoPayload that = (DeviceInfoPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		if (phone != null ? !phone.equals(that.phone) : that.phone != null)
			return false;
		if (iccid != null ? !iccid.equals(that.iccid) : that.iccid != null)
			return false;
		if (imsi != null ? !imsi.equals(that.imsi) : that.imsi != null)
			return false;
		if (wifiMac != null ? !wifiMac.equals(that.wifiMac) : that.wifiMac != null)
			return false;
		return btMac != null ? btMac.equals(that.btMac) : that.btMac == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (phone != null ? phone.hashCode() : 0);
		result = 31 * result + (iccid != null ? iccid.hashCode() : 0);
		result = 31 * result + (imsi != null ? imsi.hashCode() : 0);
		result = 31 * result + (wifiMac != null ? wifiMac.hashCode() : 0);
		result = 31 * result + (btMac != null ? btMac.hashCode() : 0);
		return result;
	}
}
