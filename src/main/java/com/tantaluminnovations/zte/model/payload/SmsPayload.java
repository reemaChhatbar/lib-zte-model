package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class SmsPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private Instant smsTime;
	private String phone;
	private String smsEncoding;
	private String smsMessage;
	private boolean smsEnded;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public Instant getSmsTime() {
		return smsTime;
	}

	public void setSmsTime(Instant smsTime) {
		this.smsTime = smsTime;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSmsEncoding() {
		return smsEncoding;
	}

	public void setSmsEncoding(String smsEncoding) {
		this.smsEncoding = smsEncoding;
	}

	public String getSmsMessage() {
		return smsMessage;
	}

	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}

	public boolean isSmsEnded() {
		return smsEnded;
	}

	public void setSmsEnded(boolean smsEnded) {
		this.smsEnded = smsEnded;
	}

	@Override public String toString() {
		return "SmsPayload{" +
			   "reportTime=" + reportTime +
			   ", smsTime=" + smsTime +
			   ", phone='" + phone + '\'' +
			   ", smsEncoding='" + smsEncoding + '\'' +
			   ", smsMessage='" + smsMessage + '\'' +
			   ", smsEnded=" + smsEnded +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		SmsPayload that = (SmsPayload) o;

		if (smsEnded != that.smsEnded)
			return false;
		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		if (smsTime != null ? !smsTime.equals(that.smsTime) : that.smsTime != null)
			return false;
		if (phone != null ? !phone.equals(that.phone) : that.phone != null)
			return false;
		if (smsEncoding != null ? !smsEncoding.equals(that.smsEncoding) : that.smsEncoding != null)
			return false;
		return smsMessage != null ? smsMessage.equals(that.smsMessage) : that.smsMessage == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (smsTime != null ? smsTime.hashCode() : 0);
		result = 31 * result + (phone != null ? phone.hashCode() : 0);
		result = 31 * result + (smsEncoding != null ? smsEncoding.hashCode() : 0);
		result = 31 * result + (smsMessage != null ? smsMessage.hashCode() : 0);
		result = 31 * result + (smsEnded ? 1 : 0);
		return result;
	}
}
