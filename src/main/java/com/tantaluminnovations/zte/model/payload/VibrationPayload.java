package com.tantaluminnovations.zte.model.payload;

public class VibrationPayload extends ZteEventPayload {

	private Integer magnitudeMG;

	public Integer getMagnitudeMG() {
		return magnitudeMG;
	}

	public void setMagnitudeMG(Integer magnitudeMG) {
		this.magnitudeMG = magnitudeMG;
	}

	@Override public String toString() {
		return "VibrationPayload{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", magnitudeMG=" + magnitudeMG +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		VibrationPayload that = (VibrationPayload) o;

		return magnitudeMG != null ? magnitudeMG.equals(that.magnitudeMG) : that.magnitudeMG == null;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (magnitudeMG != null ? magnitudeMG.hashCode() : 0);
		return result;
	}
}
