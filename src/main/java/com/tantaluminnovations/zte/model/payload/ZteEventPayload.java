package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class ZteEventPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private GPSData gpsData;

	public ZteEventPayload(ZteEventPayload eventPayload){
		this.reportTime = eventPayload.getReportTime();
		this.gpsData = eventPayload.getGpsData();
	}

	public ZteEventPayload() {
	}

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public GPSData getGpsData() {
		return gpsData;
	}

	public void setGpsData(GPSData gpsData) {
		this.gpsData = gpsData;
	}

	@Override public String toString() {
		return "ZteEventPayload{" +
			   "reportTime=" + reportTime +
			   ", gpsData=" + gpsData +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ZteEventPayload that = (ZteEventPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		return gpsData != null ? gpsData.equals(that.gpsData) : that.gpsData == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (gpsData != null ? gpsData.hashCode() : 0);
		return result;
	}
}
