package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class TripSummaryPayload implements ZteExtendedPayload {

	private Instant ignitionOn;
	private GPSData ignitionOnGPSData;
	private Instant ignitionOff;
	private GPSData ignitionOffGPSData;
	private Instant reportTime;
	private Integer distanceMetres;
	private Integer fuelUsedMl;
	private Integer maxSpeedKmH;
	private Integer idleTimeSec;
	private Integer idleFuelUsedMl;
	private Integer rapidAccelerationCount;
	private Integer rapidDecelerationCount;
	private Integer hardCorneringCount;
	private Integer historicalDistanceMetres;
	private Integer historicalFuelUsedMl;
	private Integer historicalTimeDrivingSecs;
	private Integer distanceSource;

	public Instant getIgnitionOn() {
		return ignitionOn;
	}

	public void setIgnitionOn(Instant ignitionOn) {
		this.ignitionOn = ignitionOn;
	}

	public GPSData getIgnitionOnGPSData() {
		return ignitionOnGPSData;
	}

	public void setIgnitionOnGPSData(GPSData ignitionOnGPSData) {
		this.ignitionOnGPSData = ignitionOnGPSData;
	}

	public Instant getIgnitionOff() {
		return ignitionOff;
	}

	public void setIgnitionOff(Instant ignitionOff) {
		this.ignitionOff = ignitionOff;
	}

	public GPSData getIgnitionOffGPSData() {
		return ignitionOffGPSData;
	}

	public void setIgnitionOffGPSData(GPSData ignitionOffGPSData) {
		this.ignitionOffGPSData = ignitionOffGPSData;
	}

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public Integer getDistanceMetres() {
		return distanceMetres;
	}

	public void setDistanceMetres(Integer distanceMetres) {
		this.distanceMetres = distanceMetres;
	}

	public Integer getFuelUsedMl() {
		return fuelUsedMl;
	}

	public void setFuelUsedMl(Integer fuelUsedMl) {
		this.fuelUsedMl = fuelUsedMl;
	}

	public Integer getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public void setMaxSpeedKmH(Integer maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
	}

	public Integer getIdleTimeSec() {
		return idleTimeSec;
	}

	public void setIdleTimeSec(Integer idleTimeSec) {
		this.idleTimeSec = idleTimeSec;
	}

	public Integer getIdleFuelUsedMl() {
		return idleFuelUsedMl;
	}

	public void setIdleFuelUsedMl(Integer idleFuelUsedMl) {
		this.idleFuelUsedMl = idleFuelUsedMl;
	}

	public Integer getRapidAccelerationCount() {
		return rapidAccelerationCount;
	}

	public void setRapidAccelerationCount(Integer rapidAccelerationCount) {
		this.rapidAccelerationCount = rapidAccelerationCount;
	}

	public Integer getRapidDecelerationCount() {
		return rapidDecelerationCount;
	}

	public void setRapidDecelerationCount(Integer rapidDecelerationCount) {
		this.rapidDecelerationCount = rapidDecelerationCount;
	}

	public Integer getHardCorneringCount() {
		return hardCorneringCount;
	}

	public void setHardCorneringCount(Integer hardCorneringCount) {
		this.hardCorneringCount = hardCorneringCount;
	}

	public Integer getHistoricalDistanceMetres() {
		return historicalDistanceMetres;
	}

	public void setHistoricalDistanceMetres(Integer historicalDistanceMetres) {
		this.historicalDistanceMetres = historicalDistanceMetres;
	}

	public Integer getHistoricalFuelUsedMl() {
		return historicalFuelUsedMl;
	}

	public void setHistoricalFuelUsedMl(Integer historicalFuelUsedMl) {
		this.historicalFuelUsedMl = historicalFuelUsedMl;
	}

	public Integer getHistoricalTimeDrivingSecs() {
		return historicalTimeDrivingSecs;
	}

	public void setHistoricalTimeDrivingSecs(Integer historicalTimeDrivingSecs) {
		this.historicalTimeDrivingSecs = historicalTimeDrivingSecs;
	}

	public void setDistanceSource(Integer distanceSource) {
		this.distanceSource = distanceSource;
	}

	public Integer getDistanceSource() {
		return distanceSource;
	}

	@Override public String toString() {
		return "TripSummaryPayload{" +
			   "ignitionOn=" + ignitionOn +
			   ", ignitionOnGPSData=" + ignitionOnGPSData +
			   ", ignitionOff=" + ignitionOff +
			   ", ignitionOffGPSData=" + ignitionOffGPSData +
			   ", reportTime=" + reportTime +
			   ", distanceMetres=" + distanceMetres +
			   ", fuelUsedMl=" + fuelUsedMl +
			   ", maxSpeedKmH=" + maxSpeedKmH +
			   ", idleTimeSec=" + idleTimeSec +
			   ", idleFuelUsedMl=" + idleFuelUsedMl +
			   ", rapidAccelerationCount=" + rapidAccelerationCount +
			   ", rapidDecelerationCount=" + rapidDecelerationCount +
			   ", hardCorneringCount=" + hardCorneringCount +
			   ", historicalDistanceMetres=" + historicalDistanceMetres +
			   ", historicalFuelUsedMl=" + historicalFuelUsedMl +
			   ", historicalTimeDrivingSecs=" + historicalTimeDrivingSecs +
			   ", distanceSource=" + distanceSource +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		TripSummaryPayload that = (TripSummaryPayload) o;

		if (ignitionOn != null ? !ignitionOn.equals(that.ignitionOn) : that.ignitionOn != null)
			return false;
		if (ignitionOnGPSData != null ?
				!ignitionOnGPSData.equals(that.ignitionOnGPSData) :
				that.ignitionOnGPSData != null)
			return false;
		if (ignitionOff != null ? !ignitionOff.equals(that.ignitionOff) : that.ignitionOff != null)
			return false;
		if (ignitionOffGPSData != null ?
				!ignitionOffGPSData.equals(that.ignitionOffGPSData) :
				that.ignitionOffGPSData != null)
			return false;
		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		if (distanceMetres != null ? !distanceMetres.equals(that.distanceMetres) : that.distanceMetres != null)
			return false;
		if (fuelUsedMl != null ? !fuelUsedMl.equals(that.fuelUsedMl) : that.fuelUsedMl != null)
			return false;
		if (maxSpeedKmH != null ? !maxSpeedKmH.equals(that.maxSpeedKmH) : that.maxSpeedKmH != null)
			return false;
		if (idleTimeSec != null ? !idleTimeSec.equals(that.idleTimeSec) : that.idleTimeSec != null)
			return false;
		if (idleFuelUsedMl != null ? !idleFuelUsedMl.equals(that.idleFuelUsedMl) : that.idleFuelUsedMl != null)
			return false;
		if (rapidAccelerationCount != null ?
				!rapidAccelerationCount.equals(that.rapidAccelerationCount) :
				that.rapidAccelerationCount != null)
			return false;
		if (rapidDecelerationCount != null ?
				!rapidDecelerationCount.equals(that.rapidDecelerationCount) :
				that.rapidDecelerationCount != null)
			return false;
		if (hardCorneringCount != null ?
				!hardCorneringCount.equals(that.hardCorneringCount) :
				that.hardCorneringCount != null)
			return false;
		if (historicalDistanceMetres != null ?
				!historicalDistanceMetres.equals(that.historicalDistanceMetres) :
				that.historicalDistanceMetres != null)
			return false;
		if (historicalFuelUsedMl != null ?
				!historicalFuelUsedMl.equals(that.historicalFuelUsedMl) :
				that.historicalFuelUsedMl != null)
			return false;
		if (historicalTimeDrivingSecs != null ?
				!historicalTimeDrivingSecs.equals(that.historicalTimeDrivingSecs) :
				that.historicalTimeDrivingSecs != null)
			return false;
		return distanceSource != null ? distanceSource.equals(that.distanceSource) : that.distanceSource == null;
	}

	@Override public int hashCode() {
		int result = ignitionOn != null ? ignitionOn.hashCode() : 0;
		result = 31 * result + (ignitionOnGPSData != null ? ignitionOnGPSData.hashCode() : 0);
		result = 31 * result + (ignitionOff != null ? ignitionOff.hashCode() : 0);
		result = 31 * result + (ignitionOffGPSData != null ? ignitionOffGPSData.hashCode() : 0);
		result = 31 * result + (reportTime != null ? reportTime.hashCode() : 0);
		result = 31 * result + (distanceMetres != null ? distanceMetres.hashCode() : 0);
		result = 31 * result + (fuelUsedMl != null ? fuelUsedMl.hashCode() : 0);
		result = 31 * result + (maxSpeedKmH != null ? maxSpeedKmH.hashCode() : 0);
		result = 31 * result + (idleTimeSec != null ? idleTimeSec.hashCode() : 0);
		result = 31 * result + (idleFuelUsedMl != null ? idleFuelUsedMl.hashCode() : 0);
		result = 31 * result + (rapidAccelerationCount != null ? rapidAccelerationCount.hashCode() : 0);
		result = 31 * result + (rapidDecelerationCount != null ? rapidDecelerationCount.hashCode() : 0);
		result = 31 * result + (hardCorneringCount != null ? hardCorneringCount.hashCode() : 0);
		result = 31 * result + (historicalDistanceMetres != null ? historicalDistanceMetres.hashCode() : 0);
		result = 31 * result + (historicalFuelUsedMl != null ? historicalFuelUsedMl.hashCode() : 0);
		result = 31 * result + (historicalTimeDrivingSecs != null ? historicalTimeDrivingSecs.hashCode() : 0);
		result = 31 * result + (distanceSource != null ? distanceSource.hashCode() : 0);
		return result;
	}
}
