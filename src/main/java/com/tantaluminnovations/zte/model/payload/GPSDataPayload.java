package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.util.*;

public class GPSDataPayload implements ZteExtendedPayload {

	private List<GPSData> gpsDataPoints;

	public List<GPSData> getGpsDataPoints() {
		return gpsDataPoints;
	}

	public void setGpsDataPoints(List<GPSData> gpsDataPoints) {
		this.gpsDataPoints = gpsDataPoints;
	}

	@Override public String toString() {
		return "GPSDataPayload{" +
			   "gpsDataPoints=" + gpsDataPoints +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		GPSDataPayload that = (GPSDataPayload) o;

		return gpsDataPoints != null ? gpsDataPoints.equals(that.gpsDataPoints) : that.gpsDataPoints == null;
	}

	@Override public int hashCode() {
		return gpsDataPoints != null ? gpsDataPoints.hashCode() : 0;
	}
}
