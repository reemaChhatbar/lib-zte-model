package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class DeviceBugPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private ErrorType errorType;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}

	public void setErrorType(int errorType) {
		this.errorType = ErrorType.fromCode(errorType);
	}

	public enum ErrorType{
		UNKNOWN(-0x1),
		G_SENSOR(0x1),
		FLASH(0x2),
		GPS(0x3),
		CAN_BUS_COMMUNICATION(0x4);


		private final int code;

		public int getCode() {
			return code;
		}

		ErrorType(int code) {
			this.code = code;
		}

		public static ErrorType fromCode(int code){

			for (ErrorType errorType: ErrorType.values()) {
				if (errorType.getCode() == code){
					return errorType;
				}
			}
			return UNKNOWN;
		}


	}

	@Override public String toString() {
		return "DeviceBugPayload{" +
			   "reportTime=" + reportTime +
			   ", errorType=" + errorType +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		DeviceBugPayload that = (DeviceBugPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		return errorType == that.errorType;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (errorType != null ? errorType.hashCode() : 0);
		return result;
	}
}
