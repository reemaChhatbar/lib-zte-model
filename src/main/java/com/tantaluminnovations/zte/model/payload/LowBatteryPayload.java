package com.tantaluminnovations.zte.model.payload;

public class LowBatteryPayload extends ZteEventPayload {

	private Double voltage;

	public Double getVoltage() {
		return voltage;
	}

	public void setVoltage(Double voltage) {
		this.voltage = voltage;
	}

	@Override public String toString() {
		return "LowBatteryPayload{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", voltage=" + voltage +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		LowBatteryPayload that = (LowBatteryPayload) o;

		return voltage != null ? voltage.equals(that.voltage) : that.voltage == null;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (voltage != null ? voltage.hashCode() : 0);
		return result;
	}
}
