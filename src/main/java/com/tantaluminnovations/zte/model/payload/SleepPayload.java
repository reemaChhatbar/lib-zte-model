package com.tantaluminnovations.zte.model.payload;

public class SleepPayload extends ZteEventPayload {

	private double batteryVoltage;

	public double getBatteryVoltage() {
		return batteryVoltage;
	}

	public void setBatteryVoltage(double batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}

	@Override public String toString() {
		return "SleepPayload{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", batteryVoltage=" + batteryVoltage +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		SleepPayload that = (SleepPayload) o;

		return Double.compare(that.batteryVoltage, batteryVoltage) == 0;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(batteryVoltage);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
}
