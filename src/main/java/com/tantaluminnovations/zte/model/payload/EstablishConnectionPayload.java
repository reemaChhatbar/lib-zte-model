package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class EstablishConnectionPayload implements ZteExtendedPayload {

	private int protocolVersion;
	private int majorVersion;
	private int minorVersion;
	private int revisionValue;
	private String mcuSoftwareVersion;
	private String modemSoftwareVersion;
	private Instant reportTime;

	public int getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(int protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public int getMajorVersion() {
		return majorVersion;
	}

	public void setMajorVersion(int majorVersion) {
		this.majorVersion = majorVersion;
	}

	public int getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(int minorVersion) {
		this.minorVersion = minorVersion;
	}

	public int getRevisionValue() {
		return revisionValue;
	}

	public void setRevisionValue(int revisionValue) {
		this.revisionValue = revisionValue;
	}

	public String getMcuSoftwareVersion() {
		return mcuSoftwareVersion;
	}

	public void setMcuSoftwareVersion(String mcuSoftwareVersion) {
		this.mcuSoftwareVersion = mcuSoftwareVersion;
	}

	public String getModemSoftwareVersion() {
		return modemSoftwareVersion;
	}

	public void setModemSoftwareVersion(String modemSoftwareVersion) {
		this.modemSoftwareVersion = modemSoftwareVersion;
	}

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	@Override public String toString() {
		return "EstablishConnectionPayload{" +
			   "protocolVersion=" + protocolVersion +
			   ", majorVersion=" + majorVersion +
			   ", minorVersion=" + minorVersion +
			   ", revisionValue=" + revisionValue +
			   ", mcuSoftwareVersion='" + mcuSoftwareVersion + '\'' +
			   ", modemSoftwareVersion='" + modemSoftwareVersion + '\'' +
			   ", reportTime=" + reportTime +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		EstablishConnectionPayload that = (EstablishConnectionPayload) o;

		if (protocolVersion != that.protocolVersion)
			return false;
		if (majorVersion != that.majorVersion)
			return false;
		if (minorVersion != that.minorVersion)
			return false;
		if (revisionValue != that.revisionValue)
			return false;
		if (mcuSoftwareVersion != null ?
				!mcuSoftwareVersion.equals(that.mcuSoftwareVersion) :
				that.mcuSoftwareVersion != null)
			return false;
		if (modemSoftwareVersion != null ?
				!modemSoftwareVersion.equals(that.modemSoftwareVersion) :
				that.modemSoftwareVersion != null)
			return false;
		return reportTime != null ? reportTime.equals(that.reportTime) : that.reportTime == null;
	}

	@Override public int hashCode() {
		int result = protocolVersion;
		result = 31 * result + majorVersion;
		result = 31 * result + minorVersion;
		result = 31 * result + revisionValue;
		result = 31 * result + (mcuSoftwareVersion != null ? mcuSoftwareVersion.hashCode() : 0);
		result = 31 * result + (modemSoftwareVersion != null ? modemSoftwareVersion.hashCode() : 0);
		result = 31 * result + (reportTime != null ? reportTime.hashCode() : 0);
		return result;
	}
}
