package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.util.*;

public class IgnitionOnOffPayload extends ZteEventPayload {

	private Integer type;
	private List<DTC> dtcs;
	private List<DTC> privateDtcs;

	public IgnitionOnOffPayload(ZteEventPayload eventPayload) {
		super(eventPayload);
	}

	public IgnitionOnOffPayload() {
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public List<DTC> getDtcs() {
		return dtcs;
	}

	public void setDtcs(List<DTC> dtcs) {
		this.dtcs = dtcs;
	}

	public List<DTC> getPrivateDtcs() {
		return privateDtcs;
	}

	public void setPrivateDtcs(List<DTC> privateDtcs) {
		this.privateDtcs = privateDtcs;
	}

	@Override public String toString() {
		return "IgnitionOnOffPayload{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", type=" + type +
			   ", dtcs=" + dtcs +
			   ", privateDtcs=" + privateDtcs +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		IgnitionOnOffPayload that = (IgnitionOnOffPayload) o;

		if (type != null ? !type.equals(that.type) : that.type != null)
			return false;
		if (dtcs != null ? !dtcs.equals(that.dtcs) : that.dtcs != null)
			return false;
		return privateDtcs != null ? privateDtcs.equals(that.privateDtcs) : that.privateDtcs == null;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (type != null ? type.hashCode() : 0);
		result = 31 * result + (dtcs != null ? dtcs.hashCode() : 0);
		result = 31 * result + (privateDtcs != null ? privateDtcs.hashCode() : 0);
		return result;
	}
}
