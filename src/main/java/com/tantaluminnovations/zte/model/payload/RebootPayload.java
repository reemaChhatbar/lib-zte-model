package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;
import java.util.*;

public class RebootPayload implements ZteExtendedPayload {

	private Instant powerOnTime;
	private Instant lastPowerDownTime;
	private RebootType type;

	public Instant getPowerOnTime() {
		return powerOnTime;
	}

	public void setPowerOnTime(Instant powerOnTime) {
		this.powerOnTime = powerOnTime;
	}

	public Instant getLastPowerDownTime() {
		return lastPowerDownTime;
	}

	public void setLastPowerDownTime(Instant lastPowerDownTime) {
		this.lastPowerDownTime = lastPowerDownTime;
	}

	public RebootType getType() {
		return type;
	}

	public void setType(RebootType type) {
		this.type = type;
	}

	public void setType(int code) {
		this.type = RebootType.fromCode(code);
	}

	@Override public String toString() {
		return "RebootPayload{" +
			   "powerOnTime=" + powerOnTime +
			   ", lastPowerDownTime=" + lastPowerDownTime +
			   ", type=" + type +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		RebootPayload that = (RebootPayload) o;

		if (powerOnTime != null ? !powerOnTime.equals(that.powerOnTime) : that.powerOnTime != null)
			return false;
		if (lastPowerDownTime != null ?
				!lastPowerDownTime.equals(that.lastPowerDownTime) :
				that.lastPowerDownTime != null)
			return false;
		return type == that.type;
	}

	@Override public int hashCode() {
		int result = powerOnTime != null ? powerOnTime.hashCode() : 0;
		result = 31 * result + (lastPowerDownTime != null ? lastPowerDownTime.hashCode() : 0);
		result = 31 * result + (type != null ? type.hashCode() : 0);
		return result;
	}

	public enum RebootType{
		UNKNOWN(-1),
		NORMAL(0),
		REBOOT(1),
		UPGRADE(2),
		//ZTE Message spec V2.0+
		DEVICE_PLUGGED_IN(3),
		RESTART_KEY_POWER_ON(4);

		private final int code;

		public int getCode() {
			return code;
		}

		RebootType(int code) {
			this.code = code;
		}

		public static RebootType fromCode(int code) {
			for (RebootType state: RebootType.values()) {
				if (state.getCode() == code){
					return state;
				}
			}
			return UNKNOWN;
		}

		private static enum CodeMap {
			INSTANCE;

			private Map<Integer, RebootType> codeMap = new HashMap();

			private CodeMap() {
			}

			private void put(RebootType value) {
				this.codeMap.put(Integer.valueOf(value.getCode()), value);
			}

			private RebootType get(Integer key) {
				return (RebootType)this.codeMap.get(key);
			}

		}


	}
}
