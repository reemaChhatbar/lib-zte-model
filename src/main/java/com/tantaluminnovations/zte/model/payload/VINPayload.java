package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class VINPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private Integer protocolType;
	private Integer vinLength;
	private String vin;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public Integer getProtocolType() {
		return protocolType;
	}

	public void setProtocolType(Integer protocolType) {
		this.protocolType = protocolType;
	}

	public Integer getVinLength() {
		return vinLength;
	}

	public void setVinLength(Integer vinLength) {
		this.vinLength = vinLength;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	@Override public String toString() {
		return "VINPayload{" +
			   "reportTime=" + reportTime +
			   ", protocolType=" + protocolType +
			   ", vinLength=" + vinLength +
			   ", vin='" + vin + '\'' +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		VINPayload that = (VINPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		if (protocolType != null ? !protocolType.equals(that.protocolType) : that.protocolType != null)
			return false;
		if (vinLength != null ? !vinLength.equals(that.vinLength) : that.vinLength != null)
			return false;
		return vin != null ? vin.equals(that.vin) : that.vin == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (protocolType != null ? protocolType.hashCode() : 0);
		result = 31 * result + (vinLength != null ? vinLength.hashCode() : 0);
		result = 31 * result + (vin != null ? vin.hashCode() : 0);
		return result;
	}
}
