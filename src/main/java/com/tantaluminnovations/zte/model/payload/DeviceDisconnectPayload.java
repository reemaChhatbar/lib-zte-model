package com.tantaluminnovations.zte.model.payload;

public class DeviceDisconnectPayload extends ZteEventPayload{

	private DeviceUnpluggedState deviceUnpluggedState;

	public DeviceUnpluggedState getDeviceUnpluggedState() {
		return deviceUnpluggedState;
	}

	public void setDeviceUnpluggedState(
			DeviceUnpluggedState deviceUnpluggedState) {
		this.deviceUnpluggedState = deviceUnpluggedState;
	}

	public void setDeviceUnpluggedState(int code) {
		this.deviceUnpluggedState = DeviceUnpluggedState.fromCode(code);
	}

	@Override public String toString() {
		return "DeviceDisconnectPayload{" +
			   "deviceUnpluggedState=" + deviceUnpluggedState +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		DeviceDisconnectPayload that = (DeviceDisconnectPayload) o;

		return deviceUnpluggedState == that.deviceUnpluggedState;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (deviceUnpluggedState != null ? deviceUnpluggedState.hashCode() : 0);
		return result;
	}

	public enum DeviceUnpluggedState{
		UNKNOWN(-0x1),
		NORMAL(0x0),
		VEHICLE_IGNITION_STILL_ON(0x1);

		private final int code;

		public int getCode() {
			return code;
		}

		DeviceUnpluggedState(int code) {
			this.code = code;
		}

		public static DeviceUnpluggedState fromCode(int code){

			for (DeviceUnpluggedState state: DeviceUnpluggedState.values()) {
				if (state.getCode() == code){
					return state;
				}
			}
			return UNKNOWN;
		}


	}
}
