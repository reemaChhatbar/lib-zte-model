package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class AcceleratorStatusPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private boolean recalibrationSuccess;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public boolean isRecalibrationSuccess() {
		return recalibrationSuccess;
	}

	public void setRecalibrationSuccess(boolean recalibrationSuccess) {
		this.recalibrationSuccess = recalibrationSuccess;
	}

	@Override public String toString() {
		return "AcceleratorStatusPayload{" +
			   "reportTime=" + reportTime +
			   ", recalibrationSuccess=" + recalibrationSuccess +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		AcceleratorStatusPayload that = (AcceleratorStatusPayload) o;

		if (recalibrationSuccess != that.recalibrationSuccess)
			return false;
		return reportTime != null ? reportTime.equals(that.reportTime) : that.reportTime == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (recalibrationSuccess ? 1 : 0);
		return result;
	}
}
