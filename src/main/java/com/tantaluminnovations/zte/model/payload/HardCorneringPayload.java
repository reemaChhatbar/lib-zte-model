package com.tantaluminnovations.zte.model.payload;

public class HardCorneringPayload extends ZteEventPayload {

	private Double turningAccelerationGpS2;

	public HardCorneringPayload(ZteEventPayload eventPayload) {
		super(eventPayload);
	}

	public HardCorneringPayload() {
	}

	public Double getTurningAccelerationGpS2() {
		return turningAccelerationGpS2;
	}

	public void setTurningAccelerationGpS2(Double turningAccelerationGpS2) {
		this.turningAccelerationGpS2 = turningAccelerationGpS2;
	}

	@Override public String toString() {
		return "HardCorneringPayload{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", turningAccelerationGpS2=" + turningAccelerationGpS2 +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		HardCorneringPayload that = (HardCorneringPayload) o;

		return turningAccelerationGpS2 != null ?
				turningAccelerationGpS2.equals(that.turningAccelerationGpS2) :
				that.turningAccelerationGpS2 == null;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (turningAccelerationGpS2 != null ? turningAccelerationGpS2.hashCode() : 0);
		return result;
	}
}
