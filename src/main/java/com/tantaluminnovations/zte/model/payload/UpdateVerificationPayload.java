package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class UpdateVerificationPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private String fileName;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override public String toString() {
		return "UpdateVerificationPayload{" +
			   "reportTime=" + reportTime +
			   ", fileName='" + fileName + '\'' +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UpdateVerificationPayload that = (UpdateVerificationPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		return fileName != null ? fileName.equals(that.fileName) : that.fileName == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
		return result;
	}
}
