package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

public class UpdateAvailabilityResponse implements ZteExtendedPayload {

	private UpdateAvailabilityState responseState;

	public UpdateAvailabilityState getResponseState() {
		return responseState;
	}

	public void setResponseState(UpdateAvailabilityState responseState) {
		this.responseState = responseState;
	}

	public void setResponseState(int responseState) {
		this.responseState = UpdateAvailabilityState.fromCode(responseState);
	}

	@Override public String toString() {
		return "UpdateAvailabilityResponse{" +
			   "responseState=" + responseState +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UpdateAvailabilityResponse that = (UpdateAvailabilityResponse) o;

		return responseState == that.responseState;
	}

	@Override public int hashCode() {
		return responseState != null ? responseState.hashCode() : 0;
	}

	public enum UpdateAvailabilityState {
		UNKNOWN(-0x1),
		UPDATE_IMMEDIATELY(0x0),
		CURRENTLY_DRIVING_UPDATE_LATER(0x1),
		REPORTING_DATA_INCOMPLETE(0x2),
		INCORRECT_DATA_LENGTH(0x3),
		INVALID_CRC(0x4),
		ALREADY_UPDATED(0x5)
		;

		private final int code;

		public int getCode() {
			return code;
		}

		UpdateAvailabilityState(int code) {
			this.code = code;
		}

		public static UpdateAvailabilityState fromCode(int code){

			for (UpdateAvailabilityState state: UpdateAvailabilityState.values()) {
				if (state.getCode() == code){
					return state;
				}
			}
			return UNKNOWN;
		}


	}
}
