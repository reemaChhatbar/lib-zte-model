package com.tantaluminnovations.zte.model.payload;

import com.tantaluminnovations.zte.model.*;

import java.time.*;

public class AgpsRequestPayload implements ZteExtendedPayload {

	private Instant reportTime;
	private Double requestLongitude;
	private Double requestLatitude;
	private Integer requestLongitudeDirection;
	private Integer requestLatitudeDirection;

	public Instant getReportTime() {
		return reportTime;
	}

	public void setReportTime(Instant reportTime) {
		this.reportTime = reportTime;
	}

	public Double getRequestLongitude() {
		return requestLongitude;
	}

	public void setRequestLongitude(Double requestLongitude) {
		this.requestLongitude = requestLongitude;
	}

	public Double getRequestLatitude() {
		return requestLatitude;
	}

	public void setRequestLatitude(Double requestLatitude) {
		this.requestLatitude = requestLatitude;
	}

	public Integer getRequestLongitudeDirection() {
		return requestLongitudeDirection;
	}

	public void setRequestLongitudeDirection(Integer requestLongitudeDirection) {
		this.requestLongitudeDirection = requestLongitudeDirection;
	}

	public Integer getRequestLatitudeDirection() {
		return requestLatitudeDirection;
	}

	public void setRequestLatitudeDirection(Integer requestLatitudeDirection) {
		this.requestLatitudeDirection = requestLatitudeDirection;
	}

	@Override public String toString() {
		return "AgpsRequestPayload{" +
			   "reportTime=" + reportTime +
			   ", requestLongitude=" + requestLongitude +
			   ", requestLatitude=" + requestLatitude +
			   ", requestLongitudeDirection=" + requestLongitudeDirection +
			   ", requestLatitudeDirection=" + requestLatitudeDirection +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		AgpsRequestPayload that = (AgpsRequestPayload) o;

		if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null)
			return false;
		if (requestLongitude != null ? !requestLongitude.equals(that.requestLongitude) : that.requestLongitude != null)
			return false;
		if (requestLatitude != null ? !requestLatitude.equals(that.requestLatitude) : that.requestLatitude != null)
			return false;
		if (requestLongitudeDirection != null ?
				!requestLongitudeDirection.equals(that.requestLongitudeDirection) :
				that.requestLongitudeDirection != null)
			return false;
		return requestLatitudeDirection != null ?
				requestLatitudeDirection.equals(that.requestLatitudeDirection) :
				that.requestLatitudeDirection == null;
	}

	@Override public int hashCode() {
		int result = reportTime != null ? reportTime.hashCode() : 0;
		result = 31 * result + (requestLongitude != null ? requestLongitude.hashCode() : 0);
		result = 31 * result + (requestLatitude != null ? requestLatitude.hashCode() : 0);
		result = 31 * result + (requestLongitudeDirection != null ? requestLongitudeDirection.hashCode() : 0);
		result = 31 * result + (requestLatitudeDirection != null ? requestLatitudeDirection.hashCode() : 0);
		return result;
	}
}
