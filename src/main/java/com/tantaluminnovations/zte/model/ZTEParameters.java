package com.tantaluminnovations.zte.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZTEParameters{

	private String deviceKey;

	public String getDeviceKey() {
		return deviceKey;
	}

	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}

	@Override public String toString() {
		return "ZTEParameters{" +
			   "deviceKey='" + deviceKey + '\'' +
			   '}';
	}
}
