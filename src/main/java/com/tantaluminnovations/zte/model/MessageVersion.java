package com.tantaluminnovations.zte.model;

public class MessageVersion implements Comparable<MessageVersion>{

	private int majorVersion;
	private int minorVersion;
	private int revision;

	public MessageVersion(int majorVersion, int minorVersion, int revision) {
		this.majorVersion = majorVersion;
		this.minorVersion = minorVersion;
		this.revision = revision;
	}

	public MessageVersion(int majorVersion, int minorVersion) {
		this.majorVersion = majorVersion;
		this.minorVersion = minorVersion;
	}

	public MessageVersion(String version) {
		setVersion(version);
	}

	public MessageVersion() {
	}

	public void setVersion(String version){
		if(version.matches("[0-9]+\\.[0-9]+\\.?[0-9]*")){
			String[] values = version.split("\\.");
			majorVersion = Integer.parseInt(values[0]);
			minorVersion = Integer.parseInt(values[1]);
			if(values.length == 3){
				revision = Integer.parseInt(values[2]);
			}
		} else {
			majorVersion = 0;
			minorVersion = 0;
			revision = 0;
		}

	}

	public String getVersion(){
		return toString();
	}

	@Override public String toString() {
		return majorVersion + "." + minorVersion + "." + revision;
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		MessageVersion that = (MessageVersion) o;

		if (majorVersion != that.majorVersion)
			return false;
		if (minorVersion != that.minorVersion)
			return false;
		return revision == that.revision;
	}

	@Override public int hashCode() {
		int result = majorVersion;
		result = 31 * result + minorVersion;
		result = 31 * result + revision;
		return result;
	}

	@Override public int compareTo(MessageVersion o) {
		if(o == null) return 1;
		int compareValue = majorVersion - o.majorVersion;
		compareValue = compareValue != 0 ? compareValue : minorVersion - o.minorVersion;
		compareValue = compareValue != 0 ? compareValue : revision - o.revision;
		return compareValue;
	}
}
