package com.tantaluminnovations.zte.model;

import java.time.Instant;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.tantaluminnovations.ttp.model.FuelType;
import com.tantaluminnovations.ttp.model.TripType;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Device {

	private String deviceUuid;
	private FuelType fuelType;
	private int engineSize, modelYear;
	private String make, model;
	private String companyId;
	private DeviceType deviceType;
	private String vehicleCategory;
	private ZTEParameters additionalParameters;
	private String agentVersion;

	public String getDeviceUuid() {
		return deviceUuid;
	}

	public void setDeviceUuid(String deviceUuid) {
		this.deviceUuid = deviceUuid;
	}

	public FuelType getFuelType() {
		return fuelType;
	}

	public void setFuelType(FuelType fuelType) {
		this.fuelType = fuelType;
	}

	public int getEngineSize() {
		return engineSize;
	}

	public void setEngineSize(int engineSize) {
		this.engineSize = engineSize;
	}

	public int getModelYear() {
		return modelYear;
	}

	public void setModelYear(int modelYear) {
		this.modelYear = modelYear;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	public String getVehicleCategory() {
		return vehicleCategory;
	}

	public void setVehicleCategory(String vehicleCategory) {
		this.vehicleCategory = vehicleCategory;
	}

	public ZTEParameters getAdditionalParameters() {
		return additionalParameters;
	}

	public void setAdditionalParameters(ZTEParameters additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	public String getAgentVersion() {
		return agentVersion;
	}

	public void setAgentVersion(String agentVersion) {
		this.agentVersion = agentVersion;
	}

	@Override public String toString() {
		return "Device{" +
			   "deviceUuid='" + deviceUuid + '\'' +
			   ", fuelType=" + fuelType +
			   ", engineSize=" + engineSize +
			   ", modelYear=" + modelYear +
			   ", make='" + make + '\'' +
			   ", model='" + model + '\'' +
			   ", companyId='" + companyId + '\'' +
			   ", deviceType=" + deviceType +
			   ", vehicleCategory='" + vehicleCategory + '\'' +
			   ", additionalParameters=" + additionalParameters +
			   ", agentVersion='" + agentVersion + '\'' +
			   '}';
	}
}
