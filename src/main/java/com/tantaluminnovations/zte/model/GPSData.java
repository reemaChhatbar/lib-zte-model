package com.tantaluminnovations.zte.model;

import com.tantaluminnovations.ttp.model.*;

import java.math.BigDecimal;
import java.time.*;

public class GPSData {

	private LatLng location;
	private Integer heading;
	private Integer altitude;
	private Instant time;
	private Integer validity;
	private Double speedMpS;
	private Double pdop;
	private Double hdop;
	private Double vdop;
	private Integer satelliteCount;

	public LatLng getLocation() {
		return location;
	}

	public void setLocation(LatLng location) {
		this.location = location;
	}

	public Integer getHeading() {
		return heading;
	}

	public void setHeading(Integer heading) {
		this.heading = heading;
	}

	public Integer getAltitude() {
		return altitude;
	}

	public void setAltitude(Integer altitude) {
		this.altitude = altitude;
	}

	public Instant getTime() {
		return time;
	}

	public void setTime(Instant time) {
		this.time = time;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public Double getSpeedMpS() {
		return speedMpS;
	}

	public void setSpeedMpS(Double speedMpS) {
		this.speedMpS = speedMpS;
	}

	public Double getPdop() {
		return pdop;
	}

	public void setPdop(Double pdop) {
		this.pdop = pdop;
	}

	public Double getHdop() {
		return hdop;
	}

	public void setHdop(Double hdop) {
		this.hdop = hdop;
	}

	public Double getVdop() {
		return vdop;
	}

	public void setVdop(Double vdop) {
		this.vdop = vdop;
	}

	public void setSatelliteCount(Integer satelliteCount) {
		this.satelliteCount = satelliteCount;
	}

	public Integer getSatelliteCount() {
		return satelliteCount;
	}

	public GPSQuality getGPSQuality() { //TODO potential to apply quality by hdop/vdop/pdop
		if(satelliteCount == null){
			return GPSQuality.UNKNOWN;
		} else if(satelliteCount >= 6){
			return GPSQuality.FIX_3D;
		} else if( satelliteCount >= 3){
			return GPSQuality.FIX_2D;
		} else {
			return GPSQuality.NO_FIX;
		}
	}

	public Integer getSpeedKnots() {
		return (int) (speedMpS * 1.9438444924574);
	}

	public Double getSpeedKMpH() {
		return speedMpS * 3.6;
	}

	@Override public String toString() {
		return "GPSData{" +
			   "location=" + location +
			   ", heading=" + heading +
			   ", altitude=" + altitude +
			   ", time=" + time +
			   ", validity=" + validity +
			   ", speedMpS=" + speedMpS +
			   ", pdop=" + pdop +
			   ", hdop=" + hdop +
			   ", vdop=" + vdop +
			   ", satelliteCount=" + satelliteCount +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		GPSData gpsData = (GPSData) o;

		if (location != null ? !location.equals(gpsData.location) : gpsData.location != null)
			return false;
		if (heading != null ? !heading.equals(gpsData.heading) : gpsData.heading != null)
			return false;
		if (altitude != null ? !altitude.equals(gpsData.altitude) : gpsData.altitude != null)
			return false;
		if (time != null ? !time.equals(gpsData.time) : gpsData.time != null)
			return false;
		if (validity != null ? !validity.equals(gpsData.validity) : gpsData.validity != null)
			return false;
		if (speedMpS != null ? !speedMpS.equals(gpsData.speedMpS) : gpsData.speedMpS != null)
			return false;
		if (pdop != null ? !pdop.equals(gpsData.pdop) : gpsData.pdop != null)
			return false;
		if (hdop != null ? !hdop.equals(gpsData.hdop) : gpsData.hdop != null)
			return false;
		if (vdop != null ? !vdop.equals(gpsData.vdop) : gpsData.vdop != null)
			return false;
		return satelliteCount != null ? satelliteCount.equals(gpsData.satelliteCount) : gpsData.satelliteCount == null;
	}

	@Override public int hashCode() {
		int result = location != null ? location.hashCode() : 0;
		result = 31 * result + (heading != null ? heading.hashCode() : 0);
		result = 31 * result + (altitude != null ? altitude.hashCode() : 0);
		result = 31 * result + (time != null ? time.hashCode() : 0);
		result = 31 * result + (validity != null ? validity.hashCode() : 0);
		result = 31 * result + (speedMpS != null ? speedMpS.hashCode() : 0);
		result = 31 * result + (pdop != null ? pdop.hashCode() : 0);
		result = 31 * result + (hdop != null ? hdop.hashCode() : 0);
		result = 31 * result + (vdop != null ? vdop.hashCode() : 0);
		result = 31 * result + (satelliteCount != null ? satelliteCount.hashCode() : 0);
		return result;
	}
}
