package com.tantaluminnovations.zte.model;

import com.fasterxml.jackson.annotation.*;

import java.util.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZteMessageInformation extends ZteMessage {

	private byte[] decryptedBytes;
	private byte[] encryptedBytes;
	private String clientHost;
	private Integer clientPort;

	public ZteMessageInformation(){}

	public ZteMessageInformation(ZteMessage zteMessage){
		this.setFrameId(zteMessage.getFrameId());
		this.setDataTypeMajor(zteMessage.getDataTypeMajor());
		this.setDataTypeMinor(zteMessage.getDataTypeMinor());
		this.setFrameType(zteMessage.getFrameType());
		this.setImei(zteMessage.getImei());
		this.setDataLength(zteMessage.getDataLength());
		this.setLength(zteMessage.getLength());
	}

	public byte[] getDecryptedBytes() {
		return decryptedBytes;
	}

	public void setDecryptedBytes(byte[] decryptedBytes) {
		this.decryptedBytes = decryptedBytes;
	}

	public byte[] getEncryptedBytes() {
		return encryptedBytes;
	}

	public void setEncryptedBytes(byte[] encryptedBytes) {
		this.encryptedBytes = encryptedBytes;
	}

	public String getClientHost() {
		return clientHost;
	}

	public void setClientHost(String clientHost) {
		this.clientHost = clientHost;
	}

	public Integer getClientPort() {
		return clientPort;
	}

	public void setClientPort(Integer clientPort) {
		this.clientPort = clientPort;
	}

	@Override public String toString() {
		return "ZteMessageInformation{" +
			   super.toString().split("\\{")[1].split("}")[0] +
			   ", decryptedBytes=" + Arrays.toString(decryptedBytes) +
			   ", encryptedBytes=" + Arrays.toString(encryptedBytes) +
			   ", clientHost='" + clientHost + '\'' +
			   ", clientPort=" + clientPort +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;

		ZteMessageInformation that = (ZteMessageInformation) o;

		if (!Arrays.equals(decryptedBytes, that.decryptedBytes))
			return false;
		if (!Arrays.equals(encryptedBytes, that.encryptedBytes))
			return false;
		if (clientHost != null ? !clientHost.equals(that.clientHost) : that.clientHost != null)
			return false;
		return clientPort != null ? clientPort.equals(that.clientPort) : that.clientPort == null;
	}

	@Override public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + Arrays.hashCode(decryptedBytes);
		result = 31 * result + Arrays.hashCode(encryptedBytes);
		result = 31 * result + (clientHost != null ? clientHost.hashCode() : 0);
		result = 31 * result + (clientPort != null ? clientPort.hashCode() : 0);
		return result;
	}
}
