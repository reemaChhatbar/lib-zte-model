package com.tantaluminnovations.zte.model;

import java.time.*;
import java.util.*;

public class OutboundMessage {

	private UUID outboundId;
	private String deviceUuid;
	private String companyId;
	private Map<String, String> properties;
	private Instant created;
	private Instant updated;
	private int retries;
	private short messageIdShort;
	private String outboundMessageType;
	private String status;

	public UUID getOutboundId() {
		return outboundId;
	}

	public void setOutboundId(UUID outboundId) {
		this.outboundId = outboundId;
	}

	public String getDeviceUuid() {
		return deviceUuid;
	}

	public void setDeviceUuid(String deviceUuid) {
		this.deviceUuid = deviceUuid;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	public Instant getCreated() {
		return created;
	}

	public void setCreated(Instant created) {
		this.created = created;
	}

	public Instant getUpdated() {
		return updated;
	}

	public void setUpdated(Instant updated) {
		this.updated = updated;
	}

	public short getMessageIdShort() {
		return messageIdShort;
	}

	public void setMessageIdShort(short messageIdShort) {
		this.messageIdShort = messageIdShort;
	}

	public int getRetries() {
		return retries;
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}

	public String getOutboundMessageType() {
		return outboundMessageType;
	}

	public void setOutboundMessageType(String outboundMessageType) {
		this.outboundMessageType = outboundMessageType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override public String toString() {
		return "OutboundMessage{" +
			   "outboundId=" + outboundId +
			   ", deviceUuid='" + deviceUuid + '\'' +
			   ", companyId='" + companyId + '\'' +
			   ", properties=" + properties +
			   ", created=" + created +
			   ", updated=" + updated +
			   ", retries=" + retries +
			   ", messageIdShort=" + messageIdShort +
			   ", outboundMessageType='" + outboundMessageType + '\'' +
			   ", status='" + status + '\'' +
			   '}';
	}
}
