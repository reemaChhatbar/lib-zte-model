package com.tantaluminnovations.zte.model;

public enum SoftwareUpdateProperty {

	FILE_NAME, FILE_URL, AGENT_VERSION;
}
