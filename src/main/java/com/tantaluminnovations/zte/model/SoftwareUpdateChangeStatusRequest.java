package com.tantaluminnovations.zte.model;

public class SoftwareUpdateChangeStatusRequest {

	private MessageStatus messageStatus;
	private String agentVersion;

	public MessageStatus getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(MessageStatus messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getAgentVersion() {
		return agentVersion;
	}

	public void setAgentVersion(String agentVersion) {
		this.agentVersion = agentVersion;
	}

	@Override public String toString() {
		return "SoftwareUpdateChangeStatusRequest{" +
			   "messageStatus=" + messageStatus +
			   ", agentVersion='" + agentVersion + '\'' +
			   '}';
	}

}
