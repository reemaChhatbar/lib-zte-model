package com.tantaluminnovations.zte.model;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZteMessage {

	private int frameId;
	private int frameType;
	private String imei;
	private int length;
	private int dataLength;
	private Integer dataTypeMajor;
	private Integer dataTypeMinor;
	private ZteExtendedPayload extendedPayload;

	public int getFrameId() {
		return frameId;
	}

	public void setFrameId(int frameId) {
		this.frameId = frameId;
	}

	public int getFrameType() {
		return frameType;
	}

	public void setFrameType(int frameType) {
		this.frameType = frameType;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getDataLength() {
		return dataLength;
	}

	public void setDataLength(int dataLength) {
		this.dataLength = dataLength;
	}

	public Integer getDataTypeMajor() {
		return dataTypeMajor;
	}

	public void setDataTypeMajor(Integer dataTypeMajor) {
		this.dataTypeMajor = dataTypeMajor;
	}

	public Integer getDataTypeMinor() {
		return dataTypeMinor;
	}

	public void setDataTypeMinor(Integer dataTypeMinor) {
		this.dataTypeMinor = dataTypeMinor;
	}

	public ZteExtendedPayload getExtendedPayload() {
		return extendedPayload;
	}

	public void setExtendedPayload(ZteExtendedPayload extendedPayload) {
		this.extendedPayload = extendedPayload;
	}

	@Override public String toString() {
		return "ZteMessage{" +
			   "frameId=" + frameId +
			   ", frameType=" + frameType +
			   ", imei='" + imei + '\'' +
			   ", length=" + length +
			   ", dataLength=" + dataLength +
			   ", dataTypeMajor=" + dataTypeMajor +
			   ", dataTypeMinor=" + dataTypeMinor +
			   ", extendedPayload=" + extendedPayload +
			   '}';
	}

	@Override public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ZteMessage that = (ZteMessage) o;

		if (frameId != that.frameId)
			return false;
		if (frameType != that.frameType)
			return false;
		if (length != that.length)
			return false;
		if (dataLength != that.dataLength)
			return false;
		if (imei != null ? !imei.equals(that.imei) : that.imei != null)
			return false;
		if (dataTypeMajor != null ? !dataTypeMajor.equals(that.dataTypeMajor) : that.dataTypeMajor != null)
			return false;
		if (dataTypeMinor != null ? !dataTypeMinor.equals(that.dataTypeMinor) : that.dataTypeMinor != null)
			return false;
		return extendedPayload != null ? extendedPayload.equals(that.extendedPayload) : that.extendedPayload == null;
	}

	@Override public int hashCode() {
		int result = frameId;
		result = 31 * result + frameType;
		result = 31 * result + (imei != null ? imei.hashCode() : 0);
		result = 31 * result + length;
		result = 31 * result + dataLength;
		result = 31 * result + (dataTypeMajor != null ? dataTypeMajor.hashCode() : 0);
		result = 31 * result + (dataTypeMinor != null ? dataTypeMinor.hashCode() : 0);
		result = 31 * result + (extendedPayload != null ? extendedPayload.hashCode() : 0);
		return result;
	}
}
