package com.tantaluminnovations.zte.model;

public enum PlatformUpdateProperty {

	TTM_ENGINE_SIZE,
	TTM_SET_WIFI_ENABLED,
	TTM_SET_WIFI_SSID,
	TTM_SET_WIFI_PASSWORD,
	TTM_REQUEST_WIFI;

}
