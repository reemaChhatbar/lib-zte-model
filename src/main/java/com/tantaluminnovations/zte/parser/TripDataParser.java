package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;

import java.time.*;

import static java.nio.ByteOrder.*;

public class TripDataParser {

	private final GPSParser gpsParser;
	private final VehicleSecureDataParser vehicleSecureDataParser;

	protected TripDataParser(GPSParser gpsParser, VehicleSecureDataParser vehicleDataParser) {
		this.gpsParser = gpsParser;
		this.vehicleSecureDataParser = vehicleDataParser;
	}

	protected void parseTripData(BytesIterator iterator, ZteMessage message) {
		switch (message.getDataTypeMinor()) {
		case 0: //Trip Summary Data
			parseTripSummary(iterator, message);
			break;
		case 1: //Ignition On
		case 2: //Ignition Off
			parseIgnitionOnOff(iterator, message);
			break;
		default:
			iterator.skip(message.getDataLength());

		}
	}

	protected void parseTripSummary(BytesIterator iterator, ZteMessage message) {
		TripSummaryPayload summaryPayload = new TripSummaryPayload();
		iterator
				.nextInt(4, BIG_ENDIAN, v -> summaryPayload.setIgnitionOn(Instant.ofEpochSecond(v)))
				.nextBytesBinary(24, v -> summaryPayload.setIgnitionOnGPSData(gpsParser.convertToGPSData(v)))
				.nextInt(4, BIG_ENDIAN, v -> summaryPayload.setIgnitionOff(Instant.ofEpochSecond(v)))
				.nextBytesBinary(24, v -> summaryPayload.setIgnitionOffGPSData(gpsParser.convertToGPSData(v)))
				.nextBytesBinary(3, binary -> {
					summaryPayload.setDistanceSource(Integer.parseUnsignedInt(binary.substring(0, 1), 2));
					summaryPayload.setDistanceMetres(Integer.parseUnsignedInt(binary.substring(1), 2));
				})
				.nextInt(3, BIG_ENDIAN, summaryPayload::setFuelUsedMl)
				.nextInt(1, summaryPayload::setMaxSpeedKmH)
				.nextInt(2, BIG_ENDIAN, summaryPayload::setIdleTimeSec)
				.nextInt(2, BIG_ENDIAN, summaryPayload::setIdleFuelUsedMl)
				.nextInt(1, summaryPayload::setRapidAccelerationCount)
				.nextInt(1, summaryPayload::setRapidDecelerationCount)
				.nextInt(1, summaryPayload::setHardCorneringCount)
				.nextInt(4, BIG_ENDIAN, summaryPayload::setHistoricalDistanceMetres)
				.nextInt(4, BIG_ENDIAN, summaryPayload::setHistoricalFuelUsedMl)
				.nextInt(4, BIG_ENDIAN, summaryPayload::setHistoricalTimeDrivingSecs);
		summaryPayload.setReportTime(summaryPayload.getIgnitionOff());
		message.setExtendedPayload(summaryPayload);
	}

	protected void parseIgnitionOnOff(BytesIterator iterator, ZteMessage message) {
		IgnitionOnOffPayload ignitionOnOffPayload = new IgnitionOnOffPayload();
		iterator.nextInt(1, ignitionOnOffPayload::setType)
				.nextInt(4, BIG_ENDIAN, v -> ignitionOnOffPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextBytesBinary(24, v -> ignitionOnOffPayload.setGpsData(gpsParser.convertToGPSData(v)));
		DTCPayload dtcPayload = vehicleSecureDataParser.getDtcPayload(iterator);
		ignitionOnOffPayload.setDtcs(dtcPayload.getObdDtcs());
		ignitionOnOffPayload.setPrivateDtcs(dtcPayload.getPrivateDtcs());
		message.setExtendedPayload(ignitionOnOffPayload);
	}
}