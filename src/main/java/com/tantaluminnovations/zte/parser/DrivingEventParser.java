package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;

import java.nio.*;
import java.time.*;

public class DrivingEventParser {

	private final GPSParser gpsParser;

	protected DrivingEventParser(GPSParser gpsParser) {
		this.gpsParser = gpsParser;
	}

	protected void parseDrivingData(BytesIterator iterator, ZteMessage message) {
		ZteEventPayload eventPayload = new ZteEventPayload();
		iterator
				.nextInt(4, ByteOrder.BIG_ENDIAN, v -> eventPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextBytesBinary(24, v -> eventPayload.setGpsData(gpsParser.convertToGPSData(v)));

		switch (message.getDataTypeMinor()) {
		case 0: //Hard acceleration
		case 1: //Hard deceleration
			parseRapidAccelDecel(iterator, message, eventPayload);
			break;
		case 2: //Hard Cornering
			parseHardCornering(iterator, message, eventPayload);
			break;
		case 3: //Exceed Idle
		case 4: //Driving Tired - Triggered after 4 hours of driving
			message.setExtendedPayload(eventPayload);
			break;
		default:
			iterator.skip(message.getDataLength());
		}
	}

	protected void parseRapidAccelDecel(BytesIterator iterator, ZteMessage message, ZteEventPayload eventPayload) {
		RapidAccelDecelPayload rapidAccelDecelPayload = new RapidAccelDecelPayload(eventPayload);
		iterator
				.nextInt(1, rapidAccelDecelPayload::setInitialSpeedKmH)
				.nextInt(1, rapidAccelDecelPayload::setFinalSpeedKmH)
				.nextInt(1, v -> {
					if (message.getDataTypeMinor() == 0) {
						rapidAccelDecelPayload.setAccelerationMpS2(v / 10.0);
					} else {
						rapidAccelDecelPayload.setAccelerationMpS2(-v / 10.0);
					}
				});
		message.setExtendedPayload(rapidAccelDecelPayload);
	}

	protected void parseHardCornering(BytesIterator iterator, ZteMessage message, ZteEventPayload eventPayload) {
		HardCorneringPayload hardCorneringPayload = new HardCorneringPayload(eventPayload);
		iterator.nextInt(1, v -> hardCorneringPayload.setTurningAccelerationGpS2(v / 10.0));
		message.setExtendedPayload(hardCorneringPayload);
	}
}