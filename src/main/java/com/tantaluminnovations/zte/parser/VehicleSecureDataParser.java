package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;
import org.apache.commons.lang3.mutable.*;

import javax.xml.bind.*;
import java.nio.*;
import java.time.*;
import java.util.*;

import static java.nio.ByteOrder.BIG_ENDIAN;

public class VehicleSecureDataParser {

	private final GPSParser gpsParser;
	private final MessageVersion deviceUnpluggedStateVersion = new MessageVersion(2, 1);

	protected VehicleSecureDataParser(GPSParser gpsParser) {
		this.gpsParser = gpsParser;
	}

	protected void parseVehicleSecureData(BytesIterator iterator, ZteMessage message, MessageVersion messageVersion) {
		switch (message.getDataTypeMinor()) {
		case 0: //DTCs
			parseDTCs(iterator, message);
			break;
		case 1: //Low Battery
			parseLowBatteryPayload(iterator, message);
			break;
		case 2: //Vibration after ignition off
			parseVibrationPayload(iterator, message);
			break;
		case 7: //Device disconnect
		case 8: //Suspected tow event
			parseDeviceDisconnect(iterator, message, messageVersion);
			break;
		case 5: //Suspected collision
			parseSuspectedCollision(iterator, message);
			break;
		default:
			iterator.skip(message.getDataLength());
		}
	}

	protected void parseDTCs(BytesIterator iterator, ZteMessage message) {
		MutableObject<Instant> reportTime = new MutableObject();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> reportTime.setValue(Instant.ofEpochSecond(v)));
		DTCPayload dtcPayload = getDtcPayload(iterator);
		dtcPayload.setReportTime(reportTime.getValue());

		message.setExtendedPayload(dtcPayload);
	}

	DTCPayload getDtcPayload(BytesIterator iterator) {
		DTCPayload dtcPayload = new DTCPayload();

		iterator
				.nextInt(1, ByteOrder.BIG_ENDIAN, dtcPayload::setObdDtcCount);

		List<DTC> obdDtcs = new ArrayList<DTC>();
		for (int i = 0; i < dtcPayload.getObdDtcCount(); i++) {
			DTC dtc = new DTC();
			iterator.nextShort(2, ByteOrder.BIG_ENDIAN,
					v -> dtc.setCode(DatatypeConverter.printHexBinary(ByteBuffer.allocate(2).putShort(v).array())))
					.nextInt(1, dtc::setState);
			obdDtcs.add(dtc);
		}

		dtcPayload.setObdDtcs(obdDtcs);
		dtcPayload.setObdDtcCount(obdDtcs.size());

		List<DTC> privateDtcs = new ArrayList<DTC>();
		iterator.nextInt(1, ByteOrder.BIG_ENDIAN, dtcPayload::setPrivateDtcCount);
		for (int i = 0; i < dtcPayload.getPrivateDtcCount(); i++) {
			DTC dtc = new DTC();
			iterator.nextShort(2, ByteOrder.BIG_ENDIAN,
					v -> dtc.setCode(DatatypeConverter.printHexBinary(ByteBuffer.allocate(2).putShort(v).array())))
					.nextInt(1, dtc::setState);
			privateDtcs.add(dtc);
		}

		dtcPayload.setPrivateDtcs(privateDtcs);
		dtcPayload.setPrivateDtcCount(privateDtcs.size());

		return dtcPayload;
	}

	protected void parseLowBatteryPayload(BytesIterator iterator, ZteMessage message) {
		LowBatteryPayload lbPayload = new LowBatteryPayload();
		iterator
				.nextInt(4, ByteOrder.BIG_ENDIAN, v -> lbPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextBytesBinary(24, v -> lbPayload.setGpsData(gpsParser.convertToGPSData(v)))
				.nextInt(1, v -> lbPayload.setVoltage(v * 0.1));
		message.setExtendedPayload(lbPayload);
	}

	protected void parseVibrationPayload(BytesIterator iterator, ZteMessage message) {
		VibrationPayload vibrationPayload = new VibrationPayload();
		iterator
				.nextInt(4, ByteOrder.BIG_ENDIAN, v -> vibrationPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextBytesBinary(24, v -> vibrationPayload.setGpsData(gpsParser.convertToGPSData(v)))
				.nextInt(2, ByteOrder.BIG_ENDIAN, vibrationPayload::setMagnitudeMG);
		message.setExtendedPayload(vibrationPayload);
	}

	protected void parseSuspectedCollision(BytesIterator iterator, ZteMessage message) {
		CollisionPayload collisionPayload = new CollisionPayload();
		iterator
				.nextInt(4, ByteOrder.BIG_ENDIAN, v -> collisionPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextBytesBinary(24, v -> collisionPayload.setGpsData(gpsParser.convertToGPSData(v)))
				.nextInt(1, v -> collisionPayload.setMagnitudeG(v * 0.1));
		message.setExtendedPayload(collisionPayload);
	}

	protected void parseDeviceDisconnect(BytesIterator iterator, ZteMessage message, MessageVersion messageVersion) {
		DeviceDisconnectPayload deviceDisconnectPayload = new DeviceDisconnectPayload();
		iterator
				.nextInt(4, BIG_ENDIAN, v -> deviceDisconnectPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextBytesBinary(24, v -> deviceDisconnectPayload.setGpsData(gpsParser.convertToGPSData(v)));
		if(messageVersion.compareTo(deviceUnpluggedStateVersion) >= 0){
			iterator.nextInt(1, deviceDisconnectPayload::setDeviceUnpluggedState); //ZTE Message spec V2.1+
		}
		message.setExtendedPayload(deviceDisconnectPayload);
	}

	
}