package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;
import org.apache.commons.lang3.mutable.*;

import java.util.*;

import static java.nio.ByteOrder.*;

public class ServerToDeviceResponseParser {

	private final VehicleSecureDataParser vehicleSecureDataParser;

	protected ServerToDeviceResponseParser(VehicleSecureDataParser vehicleSecureDataParser) {
		this.vehicleSecureDataParser = vehicleSecureDataParser;
	}

	protected void parsePublishAcknowledgeMessage(BytesIterator iterator, ZteMessage message) {
		switch (message.getDataTypeMajor()) {
		case 0xF1:
			switch (message.getDataTypeMinor()) {
			case 0: //Device update response
				parseDeviceUpdateResponse(iterator, message);
				break;
			case 1: //Vehicle Detection response (DTC codes)
				message.setExtendedPayload(vehicleSecureDataParser.getDtcPayload(iterator));
			case 2: //Set Parameters Response
				parseSetParametersResponse(iterator, message);
				break;
			case 3: //Parameter Inquiry Response
				parseParameterInquiryReponse(iterator, message);
				break;
			case 6: //Set Vehicle Information Response
				SetVehicleInfoResponse setVehicleInfoResponse = new SetVehicleInfoResponse();
				iterator.nextBool(setVehicleInfoResponse::setUpdateSuccessful);
				message.setExtendedPayload(setVehicleInfoResponse);
			case 4: //Log File Inquiry
			case 7: //Recalibrate Accelerator Ack
			default:
				iterator.skip(message.getDataLength() - 2);
			}
			break;
		default:
			iterator.skip(message.getDataLength() - 2);

		}
	}

	private void parseParameterInquiryReponse(BytesIterator iterator, ZteMessage message) {
		int dataBytesParsed = 2;
		List<ZteDeviceParameter> parameters = new LinkedList<ZteDeviceParameter>();
		while (dataBytesParsed < message.getDataLength()) {
			ZteDeviceParameter zteDeviceParameter = new ZteDeviceParameter();
			iterator.nextBytesBinary(2, v -> zteDeviceParameter.setType(ParameterType.fromParameterId
					(Integer.parseUnsignedInt(v.substring(4), 2))));
			dataBytesParsed = dataBytesParsed + 2;
			if (zteDeviceParameter.getValueClass() == String.class) {
				MutableInt valueLength = new MutableInt();
				iterator.nextInt(1, valueLength::setValue)
						.nextString(valueLength.getValue(), zteDeviceParameter::setValue);
				dataBytesParsed = dataBytesParsed + valueLength.getValue() + 1;
			} else if (zteDeviceParameter.getValueClass() == Double.class) {
				iterator.nextInt(zteDeviceParameter.getValueLength(), BIG_ENDIAN,
						v -> zteDeviceParameter.setValue(v * zteDeviceParameter.getValuePrecision()));
				dataBytesParsed = dataBytesParsed + zteDeviceParameter.getValueLength();
			} else {
				if(zteDeviceParameter.getValueLength() > 0) {
					iterator.nextInt(zteDeviceParameter.getValueLength(), BIG_ENDIAN, zteDeviceParameter::setValue);
					dataBytesParsed = dataBytesParsed + zteDeviceParameter.getValueLength();
				}
			}
			parameters.add(zteDeviceParameter);
		}
		ZteDeviceParamaters zteDeviceParamaters = new ZteDeviceParamaters();
		zteDeviceParamaters.setZteDeviceParameterList(parameters);
		message.setExtendedPayload(zteDeviceParamaters);
	}

	private void parseSetParametersResponse(BytesIterator iterator, ZteMessage message) {
		SetParametersResponse parametersPayload = new SetParametersResponse();
		for (int i = 0; 3 * i < message.getDataLength() - 2; i++) {
			MutableInt key = new MutableInt();
			MutableBoolean value = new MutableBoolean();
			iterator.nextBytesBinary(2, v -> key.setValue(Integer.parseUnsignedInt(v.substring(4), 2)))
					.nextBool(value::setValue);
			parametersPayload.put(key.getValue(), value.getValue());
		}
		message.setExtendedPayload(parametersPayload);
	}

	private void parseDeviceUpdateResponse(BytesIterator iterator, ZteMessage message) {
		UpdateAvailabilityResponse deviceUpdateResponse = new UpdateAvailabilityResponse();
		iterator.nextInt(1, deviceUpdateResponse::setResponseState);
		message.setExtendedPayload(deviceUpdateResponse);
	}
}