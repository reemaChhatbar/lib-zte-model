package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;
import org.apache.commons.lang3.mutable.*;

import java.nio.*;
import java.time.*;

public class DeviceToServerParser {

	protected DeviceToServerParser() {
	}

	protected void parseDeviceToServerRequest(BytesIterator iterator, ZteMessage message) {
		switch (message.getDataTypeMinor()) {
		case 1: //Request update package
			parseUpdateRequest(iterator, message);
			break;
		case 2: //Request AGPSData
			parseAgpsRequest(iterator, message);
			break;
		case 3: //Update Package verification
			parseUpdateVerification(iterator, message);
			break;
		default:
			iterator.skip(message.getDataLength());
		}
	}

	protected void parseUpdateRequest(BytesIterator iterator, ZteMessage message) {
		UpdateRequestPayload updateRequestPayload = new UpdateRequestPayload();
		MutableInt fileNameLength = new MutableInt();

		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> updateRequestPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, fileNameLength::setValue)
				.nextString(fileNameLength.getValue(), updateRequestPayload::setFileName)
				.nextInt(4, ByteOrder.BIG_ENDIAN, updateRequestPayload::setFileStartPosition)
				.nextInt(2, ByteOrder.BIG_ENDIAN, updateRequestPayload::setRequestLength);

		message.setExtendedPayload(updateRequestPayload);
	}

	protected void parseAgpsRequest(BytesIterator iterator, ZteMessage message) {
		AgpsRequestPayload agpsRequestPayload = new AgpsRequestPayload();

		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> agpsRequestPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextBytesBinary(4, v -> {
					agpsRequestPayload.setRequestLongitudeDirection(Integer.valueOf(v.substring(0, 1)));
					agpsRequestPayload.setRequestLongitude(Long.valueOf(v.substring(1), 2) * 0.00001d);
				})
				.nextBytesBinary(4, v -> {
					agpsRequestPayload.setRequestLatitudeDirection(Integer.valueOf(v.substring(0, 1)));
					agpsRequestPayload.setRequestLatitude(Long.valueOf(v.substring(1), 2) * 0.00001d);
				});
		message.setExtendedPayload(agpsRequestPayload);
	}

	protected void parseUpdateVerification(BytesIterator iterator, ZteMessage message) {
		UpdateVerificationPayload updateVerificationPayload = new UpdateVerificationPayload();
		MutableInt fileNameLength = new MutableInt();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN,
				v -> updateVerificationPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, fileNameLength::setValue)
				.nextString(fileNameLength.getValue(), updateVerificationPayload::setFileName);
		message.setExtendedPayload(updateVerificationPayload);
	}
}