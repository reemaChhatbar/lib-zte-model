package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.model.*;
import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;
import org.apache.commons.lang3.mutable.*;

import java.nio.*;
import java.time.*;
import java.util.*;

public class GPSParser {

	protected GPSParser() {
	}

	protected void parseGPSData(BytesIterator iterator, ZteMessage message) {
		GPSDataPayload gpsPayload = new GPSDataPayload();
		ArrayList<GPSData> gpsDataPoints = new ArrayList<GPSData>();
		MutableInt gpsDataCount = new MutableInt();
		iterator.nextInt(1, ByteOrder.BIG_ENDIAN, gpsDataCount::setValue);
		for (int i = 0; i < gpsDataCount.getValue(); i++) {
			iterator.nextBytesBinary(24, v -> gpsDataPoints.add(convertToGPSData(v)));
		}
		gpsPayload.setGpsDataPoints(gpsDataPoints);
		message.setExtendedPayload(gpsPayload);
	}

	protected GPSData convertToGPSData(String bytesBinary) {
		GPSData gpsData = new GPSData();
		gpsData.setTime(Instant.ofEpochSecond(Integer.parseUnsignedInt(bytesBinary.substring(0, 32), 2)));
		//char 32 is position source
		gpsData.setValidity(Integer.parseUnsignedInt(bytesBinary.substring(33, 34), 2)); //Live or buffered
		//char 34 determines whether longitude is east(0) or west(1)
		int longitudeMultiplier = bytesBinary.substring(34,35).equals("0") ? 1: -1;
		//char 35 determines whether latitude is north(1) or south(0)
		int latitudeMultiplier = bytesBinary.substring(35,36).equals("0") ? -1: 1;
		gpsData.setSatelliteCount(Integer.parseUnsignedInt(bytesBinary.substring(36, 40), 2));
		gpsData.setAltitude(Integer.parseUnsignedInt(bytesBinary.substring(40, 55), 2) - 10000);
		LatLng location = new LatLng(0, 0);
		location.setLng(Integer.parseUnsignedInt(bytesBinary.substring(55, 80), 2) * 0.00001 * longitudeMultiplier);
		location.setLat(Integer.parseUnsignedInt(bytesBinary.substring(80, 104), 2) * 0.00001 * latitudeMultiplier);
		gpsData.setLocation(location);
		gpsData.setSpeedMpS(Integer.parseUnsignedInt(bytesBinary.substring(104, 116), 2) * 0.1);
		//char 116-118 reserved
		gpsData.setHeading(Integer.parseUnsignedInt(bytesBinary.substring(118, 128), 2));
		if (bytesBinary.length() > 128) {
			gpsData.setPdop(Integer.parseUnsignedInt(bytesBinary.substring(128, 140), 2) * 0.1);
			gpsData.setHdop(Integer.parseUnsignedInt(bytesBinary.substring(140, 152), 2) * 0.1);
			gpsData.setVdop(Integer.parseUnsignedInt(bytesBinary.substring(152, 164), 2) * 0.1);
		}
		return gpsData;
	}
}