package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.exception.*;
import com.tantaluminnovations.zte.model.*;
import org.apache.commons.lang3.mutable.*;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.nio.*;
import java.util.*;
import java.util.zip.*;

import static java.nio.ByteOrder.*;

public class ZteParser {

	private final GPSParser gpsParser = new GPSParser();
	private final DrivingEventParser drivingDataParser = new DrivingEventParser(gpsParser);
	private final VehicleDataParser vehicleDataParser = new VehicleDataParser(gpsParser);
	private final VehicleSecureDataParser vehicleSecureDataParser = new VehicleSecureDataParser(gpsParser);
	private final TripDataParser tripDataParser = new TripDataParser(gpsParser, vehicleSecureDataParser);
	private final DeviceToServerParser deviceToServerRequest = new DeviceToServerParser();
	private final ServerToDeviceResponseParser serverToDeviceResponseParser = new ServerToDeviceResponseParser(
			vehicleSecureDataParser);
	private final DeviceStateParser deviceInformationParser = new DeviceStateParser(gpsParser);
	private final ConnectionParser connectionParser = new ConnectionParser();

	public ZteMessage decryptAndParseMessage(byte[] messageBytes, byte[] key, Optional<MessageVersion>
			messageVersion) {
		byte[] decryptedBytes = key != null ? decryptMessage(messageBytes, key) : messageBytes;
		if (checkMessageSum(decryptedBytes)) {
			return parseMessage(decryptedBytes, true, messageVersion.orElse(new MessageVersion()));
		} else {
			throw new ZteMessageException("Failed checksum");
		}
	}

	public ZteMessageInformation decryptAndParseBasicMessage(byte[] messageBytes, byte[] key,
			Optional<MessageVersion> messageVersion) {
		byte[] decryptedBytes = key != null ? decryptMessage(messageBytes, key) : messageBytes;
		if (checkMessageSum(decryptedBytes)) {
			ZteMessageInformation zteMessage = new ZteMessageInformation(parseMessage(decryptedBytes, false,
					messageVersion.orElse(new MessageVersion())));
			zteMessage.setDecryptedBytes(decryptedBytes);
			zteMessage.setEncryptedBytes(messageBytes);
			return zteMessage;
		} else {
			throw new ZteMessageException("Failed checksum");
		}
	}

	public ZteMessage parseUnencryptedMessage(byte[] messageBytes, Optional<MessageVersion> messageVersion) {
		return parseMessage(messageBytes, true, messageVersion.orElse(new MessageVersion()));
	}

	public String getDeviceId(byte[] messageBytes) {
		BytesIterator iterator = new BytesIterator(messageBytes);
		final String[] imei = new String[1];
		iterator
				.skip(2) //Frame Header
				.skip(2) //Message Length
				.nextString(15, value -> imei[0] = value.toString()); //IMEI

		return imei[0];
	}

	private ZteMessage parseMessage(byte[] messageBytes, boolean parseFullMessage,
			MessageVersion messageVersion) {
		BytesIterator iterator = new BytesIterator(messageBytes);
		ZteMessage message = new ZteMessage();

		MutableInt checksum = new MutableInt();
		MutableInt frameEnd = new MutableInt();

		iterator
				.skip(2) //Frame Header
				.nextInt(2, BIG_ENDIAN, message::setLength) //Message Length
				.nextString(15, message::setImei) //IMEI
				.nextInt(1, message::setFrameType)//Frame Type
				.nextInt(2, BIG_ENDIAN, message::setFrameId)//Frame Number
				.nextInt(2, BIG_ENDIAN, message::setDataLength);//DataLength

		switch (message.getFrameType()) {//Effective Data
		case 0x01: //Connect Messages
			connectionParser.parseConnectionMessage(iterator, message);
			break;
		case 0x03: //Publish messages
		case 0x04: //Publish Acknowledgement Messages
			iterator
					.nextInt(1, message::setDataTypeMajor)
					.nextInt(1, message::setDataTypeMinor);
			if (message.getFrameType() == 0x03 && parseFullMessage) {
				parsePublishMessage(iterator, message, messageVersion);
			} else if (parseFullMessage) {
				serverToDeviceResponseParser.parsePublishAcknowledgeMessage(iterator, message);
			} else {
				iterator.skip(message.getDataLength() - 2);
			}
			break;
		default:
			iterator.skip(message.getDataLength());
			break;
		}

		int paddingLength = determinePadding(message.getDataLength());

		iterator
				.nextLong(4, BIG_ENDIAN, checksum::setValue) //Checksum
				.skip(paddingLength)
				.nextInt(2, BIG_ENDIAN, frameEnd::setValue);//Frame end

		if (!frameEnd.getValue().equals(0xAAAA))
			throw new ZteMessageException("Did not reach end of message");

		return message;
	}

	private int determinePadding(int dataLength) {
		int padding = 8 - Math.floorMod(9 + dataLength, 8);
		return padding == 8 ? 0 : padding;
	}

	private boolean checkMessageSum(byte[] messageBytes) {
		byte[] dataLengthBytes = Arrays.copyOfRange(messageBytes, 22, 24);
		int dataLength = ByteBuffer.wrap(dataLengthBytes).order(BIG_ENDIAN).getShort();
		int paddingLength = determinePadding(dataLength);
		byte[] expectedCheckSum = Arrays.copyOfRange(messageBytes, messageBytes.length - paddingLength - 6,
				messageBytes.length - paddingLength - 2);

		Adler32 adler32 = new Adler32();
		adler32.update(messageBytes, 2, messageBytes.length - paddingLength - 8);

		ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
		buffer.putInt(((int) adler32.getValue()));
		byte[] actualCheckSum = buffer.array();

		return Arrays.equals(expectedCheckSum, actualCheckSum);
	}

	private byte[] decryptMessage(byte[] messageBytes, byte[] key) {
		//LOGGER.debug("Encrypted Message: "+ printHexBinary(messageBytes));

		try {
			SecretKey myDesKey = new SecretKeySpec(key, "DESede");

			//get Cipher
			Cipher desCipher = Cipher.getInstance("DESede/ECB/NoPadding");

			// Initialize the cipher for decryption
			desCipher.init(Cipher.DECRYPT_MODE, myDesKey);

			short messageLength = ByteBuffer.wrap(messageBytes, 2, 2).getShort();
			// Decrypt the text
			byte[] textDecrypted = desCipher.doFinal(messageBytes, 19, messageLength - 21);

			byte[] decryptedMessage = ByteBuffer.allocate(messageLength)
					.put(ByteBuffer.wrap(messageBytes, 0, 19))
					.put(textDecrypted)
					.putShort((short) 0xAAAA).array();
			//LOGGER.debug("Decrypted Message: "+ printHexBinary(decryptedMessage));

			return decryptedMessage;
		} catch (Exception e) {
			throw new ZteMessageException("Failed to decryptMessage message", e);
		}

	}

	private void parsePublishMessage(BytesIterator iterator, ZteMessage message, MessageVersion messageVersion) {
		switch (message.getDataTypeMajor()) {
		case 0:
			tripDataParser.parseTripData(iterator, message);
			break;
		case 1:
			drivingDataParser.parseDrivingData(iterator, message);
			break;
		case 2: //GPS Data
			gpsParser.parseGPSData(iterator, message);
			break;
		case 3: //Vehicle Data
			vehicleDataParser.parseVehicleData(iterator, message);
			break;
		case 4: //Terminal Device Information
			deviceInformationParser.parseDeviceStateMessage(iterator, message);
			break;
		case 5: //Vehicle Secure Data
			vehicleSecureDataParser.parseVehicleSecureData(iterator, message, messageVersion);
			break;
		case 0xF0: //Device Report from platform server
			deviceToServerRequest.parseDeviceToServerRequest(iterator, message);
			break;
		case 0xF1: //Server Report from terminal device
		default:
			iterator.skip(message.getDataLength() - 2);
		}
	}

	private void parseDeviceInformation(BytesIterator iterator, ZteMessage message) {
		deviceInformationParser.parseDeviceStateMessage(iterator, message);
	}
}
