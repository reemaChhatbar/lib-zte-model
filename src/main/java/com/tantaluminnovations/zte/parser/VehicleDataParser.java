package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;

import java.nio.*;
import java.time.*;

public class VehicleDataParser {

	private final GPSParser gpsParser;

	protected VehicleDataParser(GPSParser gpsParser) {
		this.gpsParser = gpsParser;
	}

	protected void parseVehicleData(BytesIterator iterator, ZteMessage message) {
		switch (message.getDataTypeMinor()) {
		case 0: //Vin
			parseVinPayload(iterator, message);
			break;
		case 1: //Vehicle Data
			parseVehicleDataFlow(iterator, message);
			break;
		default:
			iterator.skip(message.getDataLength());
		}
	}

	protected void parseVinPayload(BytesIterator iterator, ZteMessage message) {
		VINPayload vinPayload = new VINPayload();
		iterator
				.nextInt(4, ByteOrder.BIG_ENDIAN, v -> vinPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, vinPayload::setProtocolType)
				.nextInt(1, vinPayload::setVinLength)
				.nextString(vinPayload.getVinLength(), vinPayload::setVin);
		message.setExtendedPayload(vinPayload);
	}

	protected void parseVehicleDataFlow(BytesIterator iterator, ZteMessage message) {
		VehicleDataFlowPayload vdfPayload = new VehicleDataFlowPayload();
		iterator
				.nextInt(4, ByteOrder.BIG_ENDIAN, v -> vdfPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, vdfPayload::setDataNumber)
				.nextBytesBinary(24, v -> vdfPayload.setGpsData(gpsParser.convertToGPSData(v)))
				.nextInt(2, ByteOrder.BIG_ENDIAN, vdfPayload::setRpm)
				.nextInt(2, ByteOrder.BIG_ENDIAN, vdfPayload::setSpeedKmH)
				.nextInt(2, ByteOrder.BIG_ENDIAN, v -> vdfPayload
						.setEngineCoolantTemperatureCelsius(v - 40)) //Engine Coolant Temperature in Degrees Celsius
				.nextInt(2, ByteOrder.BIG_ENDIAN,
						v -> vdfPayload.setThrottlePosition(v * 0.1)) //Throttle Position (in percent?)
				.nextInt(2, ByteOrder.BIG_ENDIAN,
						v -> vdfPayload.setEngineDutyPercentage(v * 0.1)) //Engine Duty in percent
				.nextInt(2, ByteOrder.BIG_ENDIAN,
						v -> vdfPayload.setIntakeAirFlowGramsPerSecond(v * 0.1)) //Intake Air flow in g/s
				.nextInt(2, ByteOrder.BIG_ENDIAN,
						v -> vdfPayload.setIntakeAirTemperatureCelsius(v - 40)) //Intake Air Temp in Degrees Celsius
				.nextInt(2, ByteOrder.BIG_ENDIAN, vdfPayload::setIntakeAirPressureKPA) //Intake Air Pressure in kpa
				.nextInt(1, v -> vdfPayload.setBatteryVoltage(v * 0.1)) //Battery Voltage
				.nextInt(2, ByteOrder.BIG_ENDIAN,
						v -> vdfPayload.setFuelLevelInput(v * 0.01)) //Fuel Level Input in percent
				.nextInt(2, ByteOrder.BIG_ENDIAN,
						vdfPayload::setNoDtcsDistanceTravelledKM) //Distance Travelled since DTCs cleared in Km
				.nextInt(2, ByteOrder.BIG_ENDIAN, v -> {
					if (v == 0) {
						vdfPayload.setMilState(false);
					} else if (v > 0 && v < 0xFFFE) {
						vdfPayload.setMilState(true);
						vdfPayload.setMilDistanceKm(v - 1);
					} else if (v == 0xFFFE) {
						vdfPayload.setMilState(true);
					}
				})
				.nextInt(4, ByteOrder.BIG_ENDIAN, vdfPayload::setHistoricalDistanceMetres)
				.nextInt(4, ByteOrder.BIG_ENDIAN, vdfPayload::setHistoricalFuelUsedMl)
				.nextInt(4, ByteOrder.BIG_ENDIAN, vdfPayload::setHistoricalDrivingTimeSecs)
				.skip(5); //Reserved
		message.setExtendedPayload(vdfPayload);
	}
}