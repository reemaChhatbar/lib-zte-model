package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;
import org.apache.commons.lang3.mutable.*;

import java.time.*;

import static java.nio.ByteOrder.*;

public class ConnectionParser {

	protected void parseConnectionMessage(BytesIterator iterator, ZteMessage message) {
		EstablishConnectionPayload establishConnectionPayload = new EstablishConnectionPayload();
		MutableInt mcuSoftVersionLength = new MutableInt();
		MutableInt modemSoftVersionLength = new MutableInt();
		iterator.nextInt(2, BIG_ENDIAN, establishConnectionPayload::setProtocolVersion)
				.nextBytesBinary(2, v -> {
					establishConnectionPayload.setMajorVersion(Integer.parseUnsignedInt(v.substring(0,4), 2));
					establishConnectionPayload.setMinorVersion(Integer.parseUnsignedInt(v.substring(4,10), 2));
					establishConnectionPayload.setRevisionValue(Integer.parseUnsignedInt(v.substring(10,16), 2));
				})
				.nextBytesBinary(1, v -> mcuSoftVersionLength.setValue(Integer.parseUnsignedInt(v.substring(2), 2)))
				.nextString(mcuSoftVersionLength.getValue(), establishConnectionPayload::setMcuSoftwareVersion)
				.nextBytesBinary(1, v -> modemSoftVersionLength.setValue(Integer.parseUnsignedInt(v.substring(2), 2)))
				.nextString(modemSoftVersionLength.getValue(), establishConnectionPayload::setModemSoftwareVersion)
				.nextInt(4, BIG_ENDIAN, v -> establishConnectionPayload.setReportTime(Instant.ofEpochSecond(v)))
				.skip(message.getDataLength() - (10 + mcuSoftVersionLength.getValue() + modemSoftVersionLength
						.getValue()));
		message.setExtendedPayload(establishConnectionPayload);
	}
}
