package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.ttp.parser.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;
import org.apache.commons.lang3.mutable.*;

import java.nio.*;
import java.time.*;

public class DeviceStateParser {

	private final GPSParser gpsParser;

	protected DeviceStateParser(GPSParser gpsParser) {
		this.gpsParser = gpsParser;
	}

	protected void parseDeviceStateMessage(BytesIterator iterator, ZteMessage message) {
		switch (message.getDataTypeMinor()) {
		case 0: //IMSI //ZTE Message spec V2.0+
			parseImsi(iterator, message);
			break;
		case 1: //Device Bug
			parseDeviceBug(iterator, message);
			break;
		case 2: //Sleep
			parseDeviceSleep(iterator, message);
			break;
		case 3: //Wake up
			parseDeviceWakeUp(iterator, message);
			break;
		case 4: //Can not locate for long time
			parseNoGpsData(iterator, message);
			break;
		case 5: //Power on after reboot
			parseDeviceReboot(iterator, message);
			break;
		case 6: //Upgrade status
			parseUpgradeStatus(iterator, message);
			break;
		case 7: //Accelerator calibration status
			parseAcceleratorCalibration(iterator, message);
			break;
		case 8: //Iccid //ZTE Message spec V2.0+
			parseIccid(iterator, message);
			break;
		case 11://SMS //ZTE Message spec V2.0+
			parseSMS(iterator, message);
			break;
		case 12: //Device info
			parseDeviceInformation(iterator, message);
			break;
		default:
			iterator.skip(message.getDataLength());
		}
	}

	private void parseImsi(BytesIterator iterator, ZteMessage message) {
		//ZTE Message spec V2.0+
		ImsiPayload imsiPayload = new ImsiPayload();
		MutableInt imsiLength = new MutableInt();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> imsiPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, imsiLength::setValue)
				.nextString(imsiLength.getValue(), imsiPayload::setImsi);
		message.setExtendedPayload(imsiPayload);
	}

	private void parseDeviceBug(BytesIterator iterator, ZteMessage message) {
		DeviceBugPayload dbPayload = new DeviceBugPayload();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> dbPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, dbPayload::setErrorType);
		message.setExtendedPayload(dbPayload);
	}

	private void parseDeviceSleep(BytesIterator iterator, ZteMessage message) {
		SleepPayload sleepPayload = new SleepPayload();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> sleepPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, v -> sleepPayload.setBatteryVoltage(v * 0.1))
				.nextBytesBinary(24, v -> sleepPayload.setGpsData(gpsParser.convertToGPSData(v)));
		message.setExtendedPayload(sleepPayload);
	}

	private void parseDeviceWakeUp(BytesIterator iterator, ZteMessage message) {
		WakeUpPayload wakeUpPayload = new WakeUpPayload();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> wakeUpPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, v -> wakeUpPayload.setBatteryVoltage(v * 0.1))
				.nextInt(1, wakeUpPayload::setTypeCode)
				.nextBytesBinary(24, v -> wakeUpPayload.setGpsData(gpsParser.convertToGPSData(v)));
		message.setExtendedPayload(wakeUpPayload);
	}

	private void parseNoGpsData(BytesIterator iterator, ZteMessage message) {
		ZteEventPayload reportPayload = new ZteEventPayload();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> reportPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextBytesBinary(24, v -> reportPayload.setGpsData(gpsParser.convertToGPSData(v))); //last known gps data
		message.setExtendedPayload(reportPayload);
	}

	private void parseDeviceReboot(BytesIterator iterator, ZteMessage message) {
		RebootPayload rebootPayload = new RebootPayload();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> rebootPayload.setPowerOnTime(Instant.ofEpochSecond(v)))
				.nextInt(4, ByteOrder.BIG_ENDIAN, v -> rebootPayload.setLastPowerDownTime(Instant.ofEpochSecond(v)))
				.nextInt(1, rebootPayload::setType);
		message.setExtendedPayload(rebootPayload);
	}

	private void parseUpgradeStatus(BytesIterator iterator, ZteMessage message) {
		UpgradeStatusPayload upgradeStatus = new UpgradeStatusPayload();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> upgradeStatus.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, upgradeStatus::setUpgradeStatus);
		message.setExtendedPayload(upgradeStatus);
	}

	private void parseAcceleratorCalibration(BytesIterator iterator, ZteMessage message) {
		AcceleratorStatusPayload acceleratorStatus = new AcceleratorStatusPayload();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> acceleratorStatus.setReportTime(Instant.ofEpochSecond(v)))
				.nextBool(acceleratorStatus::setRecalibrationSuccess);
		message.setExtendedPayload(acceleratorStatus);
	}

	private void parseIccid(BytesIterator iterator, ZteMessage message) {
		//ZTE Message spec V2.0+
		IccidPayload iccidPayload = new IccidPayload();
		MutableInt imsiLength = new MutableInt();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> iccidPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, imsiLength::setValue)
				.nextString(imsiLength.getValue(), iccidPayload::setIccid);
		message.setExtendedPayload(iccidPayload);
	}

	private void parseSMS(BytesIterator iterator, ZteMessage message) {
		//ZTE Message spec V2.0+
		SmsPayload smsPayload = new SmsPayload();
		MutableInt phoneLength = new MutableInt();
		MutableInt smsEncodingLength = new MutableInt();
		MutableInt smsLength = new MutableInt();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> smsPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(4, ByteOrder.BIG_ENDIAN, v -> smsPayload.setSmsTime(Instant.ofEpochSecond(v)))
				.nextInt(1, phoneLength::setValue)
				.nextString(phoneLength.getValue(), smsPayload::setPhone)
				.nextInt(1, smsEncodingLength::setValue)
				.nextString(smsEncodingLength.getValue(), smsPayload::setSmsEncoding)
				.nextInt(1, smsLength::setValue)
				.nextString(smsLength.getValue(), smsPayload::setSmsMessage)
				.nextBool(bool -> smsPayload.setSmsEnded(!bool));
		message.setExtendedPayload(smsPayload);
	}

	private void parseDeviceInformation(BytesIterator iterator, ZteMessage message) {
		DeviceInfoPayload deviceInfoPayload = new DeviceInfoPayload();

		MutableInt phoneLength = new MutableInt();
		MutableInt iccidLength = new MutableInt();
		MutableInt imsiLength = new MutableInt();
		MutableInt wifiMacLength = new MutableInt();
		MutableInt btMacLength = new MutableInt();
		iterator.nextInt(4, ByteOrder.BIG_ENDIAN, v -> deviceInfoPayload.setReportTime(Instant.ofEpochSecond(v)))
				.nextInt(1, phoneLength::setValue)
				.nextString(phoneLength.getValue(), deviceInfoPayload::setPhone)
				.nextInt(1, iccidLength::setValue)
				.nextString(iccidLength.getValue(), deviceInfoPayload::setIccid)
				.nextInt(1, imsiLength::setValue)
				.nextString(imsiLength.getValue(), deviceInfoPayload::setImsi)
				.nextInt(1, wifiMacLength::setValue)
				.nextString(wifiMacLength.getValue(), deviceInfoPayload::setWifiMac)
				.nextInt(1, btMacLength::setValue)
				.nextString(btMacLength.getValue(), deviceInfoPayload::setBtMac);
		message.setExtendedPayload(deviceInfoPayload);
	}
}