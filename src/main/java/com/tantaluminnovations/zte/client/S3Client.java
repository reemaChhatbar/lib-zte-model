package com.tantaluminnovations.zte.client;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.tantaluminnovations.zte.service.ZteMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

public class S3Client {

	private final Logger log = LoggerFactory.getLogger(ZteMessageDecoder.class);

	private final String AWS_BUCKET_NAME;
	private final AmazonS3 s3Client;

	public S3Client(String awsAccessKey, String awsSecretKey, String awsBucketName, Regions region) {
		BasicAWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
		s3Client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
				.withRegion(region)
				.build();
		 AWS_BUCKET_NAME = awsBucketName;
	}

	public Optional<byte[]> getFile(String fileName){
		log.debug("Fetching file portion, bucketName={}, fileName={}", AWS_BUCKET_NAME, fileName);
		return getS3FilePortion(fileName, 0, 0);
	}

	public Optional<byte[]> getFilePacket(String fileName, long fileStartPosition, long filePortionLength) {
		log.debug("Fetching file portion, bucketName={}, fileName={}, fileStartPosition={}, filePortionLength={}",
				AWS_BUCKET_NAME, fileName, fileStartPosition, filePortionLength);
		return getS3FilePortion(fileName, fileStartPosition, filePortionLength);
	}

	private Optional<byte[]> getS3FilePortion(String fileName, long fileStartPosition, long filePortionLength) {
		GetObjectRequest getObjectRequest = new GetObjectRequest(AWS_BUCKET_NAME, fileName);
		if(filePortionLength>0){
			getObjectRequest.setRange(fileStartPosition, fileStartPosition + filePortionLength - 1);
		} else if (fileStartPosition > 0){
			getObjectRequest.setRange(fileStartPosition);
		}
		S3Object object = s3Client.getObject(getObjectRequest);
		S3ObjectInputStream objectData = object.getObjectContent();
		byte[] file = null;
		try {
			long contentLength = object.getObjectMetadata().getContentLength();
			file = new byte[(int) contentLength];
			objectData.read(file);
			objectData.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Optional.ofNullable(file);
	}

	public Optional<ObjectMetadata> getFileMetadata(String fileName) {
		GetObjectMetadataRequest getObjectRequest = new GetObjectMetadataRequest(AWS_BUCKET_NAME, fileName);
		ObjectMetadata objectMetadata = s3Client.getObjectMetadata(getObjectRequest);
		return Optional.ofNullable(objectMetadata);
	}

	;

}
