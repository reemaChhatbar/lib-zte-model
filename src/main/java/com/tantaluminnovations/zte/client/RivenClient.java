package com.tantaluminnovations.zte.client;

import com.tantaluminnovations.zte.model.ChangeStatusRequest;
import com.tantaluminnovations.zte.model.Device;
import com.tantaluminnovations.zte.model.OutboundMessage;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@FeignClient(name = "micro-riven-tantalum")
public interface RivenClient {

	@RequestMapping(method = GET, value = "/internal/device/{deviceId}/message")
	List<OutboundMessage> getOutboundMessages(@PathVariable("deviceId") String deviceId);

	@RequestMapping(method = POST, value = "/internal/update/device/{deviceId}/queue")
	ResponseEntity<Void> queueDevicePolicies(@PathVariable("deviceId") String deviceId);

	@RequestMapping(method = POST, value = "/internal/update/device/{deviceId}/reapply")
	ResponseEntity<Void> reapplyDeviceSettings(@PathVariable("deviceId") String deviceId);

	@RequestMapping(method = PUT, value = "/internal/device/{deviceId}/message/{messageId}",
			produces = APPLICATION_JSON_UTF8_VALUE,
			consumes = APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<Void> changeUpdateStatus(@PathVariable("deviceId") String deviceId,
			@PathVariable("messageId") Short messageId,
			@RequestBody ChangeStatusRequest request);

	@RequestMapping(method = PUT, value = "/internal/software-update/device/{deviceId}/status",
			produces = APPLICATION_JSON_UTF8_VALUE,
			consumes = APPLICATION_JSON_UTF8_VALUE)
	ResponseEntity<Void> changeSoftwareUpdateStatus(
			@PathVariable("deviceId") String imei,
			@RequestBody ChangeStatusRequest request);

	@RequestMapping(method = GET, value = "/internal/device/{deviceId}")
	Device getDevice(@PathVariable("deviceId") String imei);
}
