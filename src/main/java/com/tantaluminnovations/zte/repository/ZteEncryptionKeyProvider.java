package com.tantaluminnovations.zte.repository;

import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.tantaluminnovations.zte.client.RivenClient;
import com.tantaluminnovations.zte.model.Device;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import static javax.xml.bind.DatatypeConverter.parseHexBinary;

/**
 * Singleton class that returns the encryption key required to decrypt messages sent by a 
 * device. The device is identified by its IMEI string.
 * 
 * @author ahe
 *
 */
public final class ZteEncryptionKeyProvider {

	private static final Logger log = LoggerFactory.getLogger(ZteEncryptionKeyProvider.class);
	
	private static ZteEncryptionKeyProvider instance; 
	
	private RivenClient rivenClient;
	
	private Cache<String,byte[]> cache;
	private static CacheManager cacheManager;
	
	private boolean cacheEnabled;
	private int cacheExpirySeconds; //element evicted if idle > cacheExpirySeconds
	private int cacheMaxEntries; //max entries before evicted elements are removed (192 bits = 24b * 10,000 ~240k)
	
	/**
	 * Get singleton instance of ZteEncryptionKeyProvider.
	 * 
	 * ZteEncryptionKeyProvider resolves a device identified by its IMEI into the 
	 * encryption key associated with the device as a byte[] array.
	 * 
	 * @param rivenClient
	 */
	public static ZteEncryptionKeyProvider getSingleton() {
		//Boilerplate Singleton pattern implementation
		if(instance == null) {
			instance = new ZteEncryptionKeyProvider();
		}
		return instance;
	}
	
	
	public ZteEncryptionKeyProvider init(Environment env, RivenClient rivenClient) {
		
		this.rivenClient = rivenClient;
		
		try {
			loadCacheProperties(env);
		} catch (IOException e) {
			log.error("Error loading cache properties.", e);
			cacheEnabled = false;
		}
		
		if(cacheEnabled) {
				
			CacheConfiguration<String, byte[]> cacheConfiguration = CacheConfigurationBuilder.newCacheConfigurationBuilder(
					String.class, byte[].class, ResourcePoolsBuilder.heap(cacheMaxEntries).offheap(50L, MemoryUnit.MB)) 
			    .withExpiry(Expirations.timeToIdleExpiration(Duration.of(cacheExpirySeconds, TimeUnit.SECONDS))) 
			    .build();
	
			cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
					.withCache("zteEncryptionKeyCache", cacheConfiguration)
					.build(true);
			
			cache = cacheManager.getCache("zteEncryptionKeyCache", String.class, byte[].class);
		}
		else {
			log.info("Caching disabled.");
		}
		return this;
	}


	private void loadCacheProperties(Environment env) throws IOException {
		cacheEnabled = env.getProperty("argon.zteEncryptionKeyProvider.cacheEnabled", Boolean.class);
		cacheExpirySeconds = env.getProperty("argon.zteEncryptionKeyProvider.cacheExpirySeconds", Integer.class);
		cacheMaxEntries = env.getProperty("argon.zteEncryptionKeyProvider.cacheMaxEntries", Integer.class);
		log.debug("Successfully loaded cache properies: cacheEnabled={}, cacheExpirySeconds={}, cacheMaxEntries={}", 
				cacheEnabled, cacheExpirySeconds, cacheMaxEntries);
	}
	
	
	private ZteEncryptionKeyProvider() {
		//Singleton pattern implementation
	}
	
	/**
	 * This method should be call on application stop or restart to clean up all cache resources.
	 */
	public static void shutDownCache() {
		if(cacheManager != null) {
			cacheManager.close();
		}
	}
	
	/**
	 * Return Optional containing the device's encryption key as a byte array. 
	 * If no encryption key can be found, an empty Optional is returned.
	 * 
	 * @param imei
	 * @return
	 */
	public Optional<byte[]> getEncryptionKey(final String imei) {

		if(this.rivenClient == null) {
			throw new IllegalStateException("RivenClient must be initialized.");
		}
		
		if(this.cacheEnabled && this.cache != null) {
	
			//add object to cache if not there
			if(!this.cache.containsKey(imei)) {
				byte[] encryptionKey = null;
				try {
					Device device = rivenClient.getDevice(imei);
					encryptionKey = parseHexBinary(
							device.getAdditionalParameters().getDeviceKey());

					if (encryptionKey != null && encryptionKey.length > 0) {
						//add non-empty element to cache
						this.cache.put(imei, encryptionKey);

						if (log.isDebugEnabled()) {
							log.debug("Caching encryption key {} for device IMEI {}", encryptionKey, imei);
						}
					} else {
						if (log.isDebugEnabled()) {
							log.debug("No encryption key found for device IMEI {}", imei);
						}
					}

				} catch (Exception e) {
					if (log.isDebugEnabled()) {
						log.debug("No encryption key found for device IMEI {}", imei);
					}
				}
				return Optional.ofNullable(encryptionKey); //return encryption key after trying to fetch it from DB
			}	
			else {//return encryption from from cache
				return Optional.of(cache.get(imei));
			}
		}
		else {//go to riven
			try {
				Device device = rivenClient.getDevice(imei);
				return Optional.ofNullable(parseHexBinary(
						device.getAdditionalParameters().getDeviceKey()));
			} catch (Exception e) {
				return Optional.empty();
			}
		}
	}

}
