package com.tantaluminnovations.zte.exception;

public class ZteMessageException extends RuntimeException {

	private static final long serialVersionUID = 6646325972927770759L;

	protected ZteMessageException() {

	}

	public ZteMessageException(String msg) {
		super(msg);
	}

	public ZteMessageException(String msg, Exception e){
		super(msg, e);
	}

	public ZteMessageException(Exception e){
		super(e);
	}

}