package com.tantaluminnovations.zte.service;

import static javax.xml.bind.DatatypeConverter.printHexBinary;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.tantaluminnovations.zte.model.ChangeStatusRequest;
import com.tantaluminnovations.zte.model.payload.UpgradeStatusPayload;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.util.StreamUtils;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.tantaluminnovations.comms.handlers.MessageDecoder;
import com.tantaluminnovations.comms.model.AckNak;
import com.tantaluminnovations.zte.client.RivenClient;
import com.tantaluminnovations.zte.client.S3Client;
import com.tantaluminnovations.zte.model.MessageStatus;
import com.tantaluminnovations.zte.model.ZteMessage;
import com.tantaluminnovations.zte.model.payload.SetParametersResponse;
import com.tantaluminnovations.zte.model.payload.UpdateRequestPayload;
import com.tantaluminnovations.zte.model.payload.UpdateVerificationPayload;
import com.tantaluminnovations.zte.parser.ZteParser;
import com.tantaluminnovations.zte.repository.ZteEncryptionKeyProvider;
import com.tantaluminnovations.zte.writer.DeviceToServerResponseWriter;

import no.finn.lambdacompanion.Pair;

public class ZteMessageDecoder implements MessageDecoder<ZteMessage> {

	private final Logger log = LoggerFactory.getLogger(ZteMessageDecoder.class);
	private final ZteEncryptionKeyProvider zteEncryptionKeyProvider;
	private final ZteParser zteParser;
	private final DeviceToServerResponseWriter zteWriter;
	private final RivenClient rivenClient;
	private final Map<String, byte[]> fileMap;
	private final S3Client s3Client;

	public ZteMessageDecoder(RivenClient rivenClient,
			Environment env, S3Client s3Client) {
		this.zteParser = new ZteParser();
		this.zteEncryptionKeyProvider = ZteEncryptionKeyProvider.getSingleton().init(env, rivenClient);
		this.zteWriter = new DeviceToServerResponseWriter();
		this.rivenClient = rivenClient;
		this.fileMap = new HashMap<>();
		this.s3Client = s3Client;
	}

	@Override
	public Pair<ZteMessage, AckNak> decode(byte[] bytes) {
		String hex = bytes != null ? printHexBinary(bytes) : StringUtils.EMPTY;
		log.debug("Received message rawHex={}", hex);

		final String IMEI = zteParser.getDeviceId(bytes);

		byte[] deviceKey = zteEncryptionKeyProvider.getEncryptionKey(IMEI)
				.orElseThrow(() -> new RuntimeException("No device key found"));

		ZteMessage message = zteParser.decryptAndParseMessage(bytes, deviceKey, Optional.empty());
		if (isDeviceToServerRequest(message)) {
			if (message.getDataTypeMinor() == 3) {
				return handleUpdateVerification(deviceKey, message);
			}
			if (message.getDataTypeMinor() == 1) {
				return handleUpdatePacketRequest(deviceKey, message);
			}
		}
		if (isPubAck(message)) {
			return handlePubAcknowledgement(message);
		}
		if (isDeviceUpdateStatusMessage(message)) {
			handleDeviceUpdateStatusMessage(message);

		}
		byte[] ack = zteWriter.writeAck(message, deviceKey);
		return new Pair<>(message, new AckNak(ack, message.getImei(), true));
	}

	private void handleDeviceUpdateStatusMessage(ZteMessage message) {
		log.debug("Got Device Update Message: {}", message);
		UpgradeStatusPayload extendedPayload = (UpgradeStatusPayload) message.getExtendedPayload();
		UpgradeStatusPayload.UpgradeState upgradeState = extendedPayload.getUpgradeStatus();
		ChangeStatusRequest request = new ChangeStatusRequest();
		if (upgradeState == UpgradeStatusPayload.UpgradeState.SUCCESS) {
			log.debug("Device {} update complete", message.getImei());
			request.setMessageStatus(MessageStatus.UPDATED);
		} else if (upgradeState == UpgradeStatusPayload.UpgradeState.DOWNLOADED) {
			log.debug("Device {} chunk complete", message.getImei());
			return;
		} else {
			log.debug("Device {} update failed", message.getImei());
			request.setMessageStatus(MessageStatus.FAILED);
		}

		rivenClient.changeSoftwareUpdateStatus(message.getImei(), request);
		log.debug("Device {} Software update status sent", message.getImei());
		if(upgradeState == UpgradeStatusPayload.UpgradeState.SUCCESS){
			rivenClient.reapplyDeviceSettings(message.getImei());
			log.debug("Device {} settings requeued", message.getImei());
		}

	}

	private boolean isPubAck(ZteMessage message) {
		return message.getFrameType() == 4 && message.getDataTypeMajor() == 241 && message.getDataTypeMinor() == 2;
	}

	private Pair<ZteMessage, AckNak> handleUpdateVerification(byte[] deviceKey, ZteMessage message) {
		UpdateVerificationPayload payload = (UpdateVerificationPayload) message.getExtendedPayload();
		String fileName = payload.getFileName();

		Optional<ObjectMetadata> fileMetadataOptional = s3Client.getFileMetadata(fileName);
		if (!fileMetadataOptional.isPresent()) {
			log.error("Could not find file={}, deviceId={}", fileName, message.getImei());
			return new Pair<>(message, new AckNak(new byte[0], message.getImei(), false));
		}
		ObjectMetadata fileMetadata = fileMetadataOptional.get();
		int size = (int) fileMetadata.getContentLength();
		log.debug("Writing update verification response, frameId={}, imei={}, fileName={}, fileSize={}",
				message.getFrameId(), message.getImei(), fileName, size);
		byte[] ack = zteWriter.writeUpdateVerificationAck(message.getImei(), deviceKey, message.getFrameId(), size,
				payload.getFileName());
		log.debug("Sending update verification response, frameId={}, imei={}, fileName={}, fileSize={}",
				message.getFrameId(), message.getImei(), fileName, size);
		return new Pair<>(message, new AckNak(ack, message.getImei(), true));
	}

	private Pair<ZteMessage, AckNak> handleUpdatePacketRequest(byte[] deviceKey, ZteMessage message) {
		UpdateRequestPayload payload = (UpdateRequestPayload) message.getExtendedPayload();
		String fileName = payload.getFileName();
		Integer fileStartPosition = payload.getFileStartPosition();
		Integer requestLength = payload.getRequestLength();

		Optional<byte[]> filePacketOptional = s3Client.getFilePacket(fileName, fileStartPosition, requestLength);
		if (!filePacketOptional.isPresent()) {
			log.error("Could not load file={}, deviceId={}", fileName, message.getImei());
			return new Pair<>(message, new AckNak(new byte[0], message.getImei(), false));
		}
		byte[] filePacket = filePacketOptional.get();
		int size = filePacket.length;
		log.debug("Writing update request response, frameId={}, imei={}, fileName={}, fileStartPosition={}, "
					+ "filePacketSize={}",
				message.getFrameId(), message.getImei(), fileName, fileStartPosition, filePacket.length);
		byte[] updatePacket = zteWriter.writeFileUpdate(message.getImei(), deviceKey, message.getFrameId(),
				fileStartPosition, filePacket);
		AckNak ack = new AckNak(updatePacket, message.getImei(), true);
		log.debug("Sending update request response, frameId={}, imei={}, fileName={}, fileStartPosition={}, "
					+ "filePacketSize={}",
				message.getFrameId(), message.getImei(), fileName, fileStartPosition, filePacket.length);
		return new Pair<>(message, ack);
	}

	private Pair<ZteMessage, AckNak> handlePubAcknowledgement(ZteMessage message) {
		log.info("Handling zte response, zteMessageInformation={}", message.toString());
		boolean updated = true;
		if (message.getDataTypeMajor() == 241 && message.getDataTypeMinor() == 2) {
			SetParametersResponse payload = (SetParametersResponse) message.getExtendedPayload();
			Map<Integer, Boolean> parameterUpdateStatuses = payload.getParameterUpdateStatuses();
			for (Integer key : parameterUpdateStatuses.keySet()) {
				if (!parameterUpdateStatuses.get(key)) {
					updated = parameterUpdateStatuses.get(key);
				}
			}
		} else {
			updated = false;
		}
		ChangeStatusRequest request = new ChangeStatusRequest();
		MessageStatus status = updated ? MessageStatus.UPDATED : MessageStatus.FAILED;
		request.setMessageStatus(status);
		log.info("Updating outbound status, status={}, messageId={}", status, message.getFrameId());
		rivenClient.changeUpdateStatus(message.getImei(), (short) message
				.getFrameId(), request);
		return new Pair<>(message, new AckNak(new byte[0], message.getImei(), false));
	}

	private boolean isDeviceToServerRequest(ZteMessage message) {
		return message.getFrameType() == 3
				&& message.getDataTypeMajor() == 240;
	}

	private boolean isDeviceUpdateStatusMessage(ZteMessage message) {
		return message.getFrameType() == 0x03
				&& message.getDataTypeMajor() == 0x04
				&& message.getDataTypeMinor() == 0x06;
	}

	private byte[] getFile(String fileName) throws IOException {
		if (fileMap.containsKey(fileName)) {
			return fileMap.get(fileName);
		}
		byte[] file;
		InputStream inputStream = ZteMessageDecoder.class.getResourceAsStream("/updates/" + fileName);
		file = StreamUtils.copyToByteArray(inputStream);
		fileMap.put(fileName, file);
		return file;
	}

}
