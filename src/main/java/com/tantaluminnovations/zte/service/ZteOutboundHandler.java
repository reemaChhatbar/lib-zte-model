package com.tantaluminnovations.zte.service;

import static com.tantaluminnovations.zte.model.PlatformUpdateProperty.TTM_SET_WIFI_ENABLED;
import static com.tantaluminnovations.zte.model.PlatformUpdateProperty.TTM_SET_WIFI_PASSWORD;
import static com.tantaluminnovations.zte.model.PlatformUpdateProperty.TTM_SET_WIFI_SSID;
import static javax.xml.bind.DatatypeConverter.printHexBinary;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.tantaluminnovations.zte.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.google.common.collect.ImmutableList;
import com.tantaluminnovations.comms.handlers.OutboundMessageHandler;
import com.tantaluminnovations.comms.model.InboundMessage;
import com.tantaluminnovations.zte.client.RivenClient;
import com.tantaluminnovations.zte.parser.ZteParser;
import com.tantaluminnovations.zte.repository.ZteEncryptionKeyProvider;
import com.tantaluminnovations.zte.writer.ServerToDeviceWriter;

public class ZteOutboundHandler implements OutboundMessageHandler {

	private static final long TWELVE_HOURS_IN_MILLIS = 12 * 60 * 60 * 1000;
	private final Logger log = LoggerFactory.getLogger(ZteOutboundHandler.class);

	private final ZteParser zteParser;
	private final ServerToDeviceWriter serverToDeviceWriter;
	private final ZteEncryptionKeyProvider zteEncryptionKeyProvider;
	private final RivenClient rivenClient;
	private final String serverAddress;

	public ZteOutboundHandler(RivenClient rivenClient,
			String serverAddress, Environment env) {

		this.zteParser = new ZteParser();
		this.zteEncryptionKeyProvider = ZteEncryptionKeyProvider.getSingleton().init(env, rivenClient);
		this.serverToDeviceWriter = new ServerToDeviceWriter();
		this.rivenClient = rivenClient;
		this.serverAddress = serverAddress;
	}

	@Override
	public List<byte[]> handle(InboundMessage inboundMessage) {
		byte[] bytes = inboundMessage.getBytes();

		final String IMEI = zteParser.getDeviceId(bytes);

		byte[] deviceKey = zteEncryptionKeyProvider.getEncryptionKey(IMEI)
				.orElseThrow(() -> new RuntimeException("No device key found"));

		// FIXME needs to be redone once policy and updates has been updated
		ZteMessageInformation zteMessageInformation = zteParser.decryptAndParseBasicMessage(bytes, deviceKey, Optional
				.empty());
		String imei = zteMessageInformation.getImei();
		int frameType = zteMessageInformation.getFrameType();
		int dataTypeMajor = zteMessageInformation.getDataTypeMajor() != null ? zteMessageInformation.getDataTypeMajor()
				: -1;
		int dataTypeMinor = zteMessageInformation.getDataTypeMinor() != null ? zteMessageInformation.getDataTypeMinor()
				: -1;

		if (isConnectionMessage(frameType) || isPingMessage(frameType) || isIgnitionOn(frameType, dataTypeMajor, dataTypeMinor)) {
			List<OutboundMessage> outbounds = rivenClient.getOutboundMessages(imei);
			List<byte[]> outboundList = new ArrayList<>();
			for (OutboundMessage outbound : outbounds) {
				try{
					handleOutboundMessage(deviceKey, imei, outboundList, outbound);
				} catch (Exception e){
					log.error("Failed to create outbound message, deviceUuid={}, outboundMessage={}", imei, outbound, e);
					ChangeStatusRequest statusRequest = new ChangeStatusRequest();
					statusRequest.setMessageStatus(MessageStatus.FAILED);
					rivenClient.changeUpdateStatus(imei, outbound.getMessageIdShort(), statusRequest);
				}
			}
			return outboundList;
		} else if (isIgnitionOff(frameType, dataTypeMajor, dataTypeMinor) || isDisconnectMessage(frameType)) {
			log.debug("Queueing device updates for deviceUuid={}", imei);
			rivenClient.queueDevicePolicies(imei);
		}
		return ImmutableList.of();
	}

	private void handleOutboundMessage(byte[] deviceKey, String imei, List<byte[]> outboundList,
			OutboundMessage outbound) {
		log.debug("Writing outboundMessage, {}", outbound.toString());
		Map<String, String> properties = outbound.getProperties();
		if (properties.containsKey(TTM_SET_WIFI_ENABLED.name())
			|| properties.containsKey(TTM_SET_WIFI_SSID.name())
			|| properties.containsKey(TTM_SET_WIFI_PASSWORD.name())) {
			byte[] outboundMessage = writeWifiSettingMessage(deviceKey, imei, outbound, properties);
			outboundList.add(outboundMessage);
			log.debug("Wifi outbound message added, outBoundMessageRaw={}", printHexBinary(outboundMessage));
		} else if (isPolicy(properties)) {
			writePolicyMessage(deviceKey, imei, outboundList, outbound, properties);
		} else if (isSoftwareUpdate(outbound)) {
			handleSoftwareUpdate(deviceKey, imei, outboundList, outbound, properties);
		} else {
			ChangeStatusRequest statusRequest = new ChangeStatusRequest();
			statusRequest.setMessageStatus(MessageStatus.NONAPPLICABLE);
			rivenClient.changeUpdateStatus(imei, outbound.getMessageIdShort(), statusRequest);
		}
	}

	private void handleSoftwareUpdate(byte[] deviceKey, String imei, List<byte[]> outboundList,
			OutboundMessage outbound, Map<String, String> properties) {
		String updateAgentVersion = properties.get(SoftwareUpdateProperty.AGENT_VERSION.name());
		Device device = rivenClient.getDevice(imei);
		if(updateAgentVersion.equalsIgnoreCase(device.getAgentVersion())){
			//Failsafe to catch devices that have successfully been updated but platform failed to be notified
			ChangeStatusRequest request = new ChangeStatusRequest();
			request.setMessageStatus(MessageStatus.UPDATED);
			rivenClient.changeSoftwareUpdateStatus(imei, request);
			rivenClient.reapplyDeviceSettings(imei);
		} else if (!isInProgress(outbound)) {
			outboundList.add(writeDeviceUpdate(deviceKey, imei, outbound));
			ChangeStatusRequest request = new ChangeStatusRequest();
			request.setMessageStatus(MessageStatus.SENT);
			rivenClient.changeUpdateStatus(imei, outbound.getMessageIdShort(),
					request);
		}
	}

	private void writePolicyMessage(byte[] deviceKey, String imei, List<byte[]> outboundList, OutboundMessage outbound,
			Map<String, String> properties) {
		List<ZteDeviceParameter> zteParameters = new ArrayList<>();
		for (String key : properties.keySet()) {
			ParameterType parameterType = ParameterType.safeValueOf(key);
			if (ParameterType.policyParameters().contains(parameterType)) {
				String value = properties.get(key);
				ZteDeviceParameter zteDeviceParameter = parameterType.permanentParameter(value);
				if (zteDeviceParameter != null)
					zteParameters.add(zteDeviceParameter);
			}
		}
		outboundList.add(serverToDeviceWriter.writeSetParameters(imei, outbound
				.getMessageIdShort(), deviceKey, zteParameters));
	}

	private byte[] writeDeviceUpdate(byte[] deviceKey, String imei,
			OutboundMessage outbound) {
		Map<String, String> properties = outbound.getProperties();
		String fileName = properties.get(SoftwareUpdateProperty.FILE_NAME.name());
		int frameId = outbound.getMessageIdShort();
		log.debug("Writing server to device update request, imei={}, fileName={}, frameId={}, serverAddress={}",
				imei, fileName, frameId, serverAddress);
		byte[] deviceUpdate = serverToDeviceWriter.writeDeviceUpdate(imei, frameId, deviceKey,
				serverAddress, fileName);
		return deviceUpdate;
	}

	private byte[] writeWifiSettingMessage(byte[] deviceKey, String imei, OutboundMessage outbound,
			Map<String, String> properties) {
		List<ZteDeviceParameter> zteParameters = new ArrayList<>();
		if (properties.containsKey(TTM_SET_WIFI_ENABLED.name())) {
			int value = Boolean.valueOf(properties.get(TTM_SET_WIFI_ENABLED.name())) ? 1 : 2;
			zteParameters.add(ParameterType.CHANGE_WIFI_ACCESS.permanentParameter(value));
		}
		if (properties.containsKey(TTM_SET_WIFI_SSID.name())) {
			zteParameters.add(ParameterType.CHANGE_WIFI_SSID
					.permanentParameter(properties.get(TTM_SET_WIFI_SSID.name())));
		}
		if (properties.containsKey(TTM_SET_WIFI_PASSWORD.name())) {
			zteParameters.add(ParameterType.CHANGE_WIFI_PASSWORD
					.permanentParameter(properties.get(TTM_SET_WIFI_PASSWORD.name())));
		}
		return serverToDeviceWriter.writeSetParameters(imei, outbound
				.getMessageIdShort(), deviceKey, zteParameters);
	}

	private boolean isPingMessage(int frameType) {
		return frameType == 0x0C;
	}

	private boolean isConnectionMessage(int frameType) {
		return frameType == 1;
	}

	private boolean isDisconnectMessage(int frameType) {
		return frameType == 0x0E;
	}

	private boolean isIgnitionOn(int frameType, int dataTypeMajor, int dataTypeMinor) {
		return frameType == 3 && dataTypeMajor == 0 && dataTypeMinor == 0x01;
	}

	private boolean isIgnitionOff(int frameType, int dataTypeMajor, int dataTypeMinor) {
		return frameType == 3 && dataTypeMajor == 0 && dataTypeMinor == 0x02;
	}

	private boolean isPolicy(Map<String, String> properties) {
		List<ParameterType> policyParameters = ParameterType.policyParameters();
		for (String key : properties.keySet()) {
			if (policyParameters.contains(ParameterType.safeValueOf(key))) {
				return true;
			}
		}
		return false;
	}

	private boolean isInProgress(OutboundMessage outbound) {
		return outbound.getStatus().equalsIgnoreCase("SENT")
				&& Instant.now().minusMillis(TWELVE_HOURS_IN_MILLIS).isBefore(outbound.getUpdated());
	}

	private boolean isSoftwareUpdate(OutboundMessage outbound) {
		return outbound.getOutboundMessageType().equalsIgnoreCase("SOFTWARE_UPDATE");
	}

}
