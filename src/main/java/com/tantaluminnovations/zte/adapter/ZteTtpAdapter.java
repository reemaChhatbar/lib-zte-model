package com.tantaluminnovations.zte.adapter;

import com.tantaluminnovations.ttp.adapter.*;
import com.tantaluminnovations.ttp.model.*;
import com.tantaluminnovations.ttp.model.Reason;
import com.tantaluminnovations.ttp.model.extended.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;

import java.time.*;
import java.util.*;
import java.util.stream.*;

import static com.tantaluminnovations.ttp.model.Reason.*;

public class ZteTtpAdapter implements TTPMessageAdapter<ZteMessage> {

	@Override
	public List<TTPMessage> toTTP(ZteMessage zteMessage) {
		return convertToTTPMessage(zteMessage);
	}

	public List<TTPMessage> convertToTTPMessage(ZteMessage zteMessage) {
		if(zteMessage.getFrameType() == 1){
			return convertConnectionMessage(zteMessage);
		}
		if (zteMessage.getDataTypeMajor() == null ||
			zteMessage.getDataTypeMinor() == null) {
			return unkownMessage(zteMessage);
		}
		switch (zteMessage.getDataTypeMajor()) {
		case 0:
			switch (zteMessage.getDataTypeMinor()) {
			case 0:
				return convertTripSummary(zteMessage);
			case 1:
				return convertIgnitionOnOff(zteMessage, IGNITION_ON);
			case 2:
				return convertIgnitionOnOff(zteMessage, IGNITION_OFF);
			default:
				return unkownMessage(zteMessage);
			}
		case 1:
			switch (zteMessage.getDataTypeMinor()) {
			case 0:
				return convertExcessiveDrivingEvent(zteMessage, EXCESSIVE_ACCELERATION);
			case 1:
				return convertExcessiveDrivingEvent(zteMessage, EXCESSIVE_DECELERATION);
			case 2:
				return convertExcessiveDrivingEvent(zteMessage, EXCESSIVE_ROTATION);
			case 3:
				return convertExcessiveIdle(zteMessage);
			default:
				return unkownMessage(zteMessage);
			}
		case 2:
			return convertGPSData(zteMessage);
		case 3:
			switch (zteMessage.getDataTypeMinor()) {
			case 0:
				return convertVin(zteMessage);
			case 1:
				return convertVehicleDataFlow(zteMessage);
			// flow
			default:
				return unkownMessage(zteMessage);
			}
		case 4:
			switch (zteMessage.getDataTypeMinor()){
			case 3:
				return Arrays.asList(convertEventMessage(zteMessage, WAKE_UP));
			default:
				return unkownMessage(zteMessage);
			}
		case 5:
			switch (zteMessage.getDataTypeMinor()) {
			case 0:
				return convertOBDFaultCodes(zteMessage);
			case 1:
				return convertLowBattery(zteMessage);
			case 2:
			case 8:
				return Arrays.asList(convertEventMessage(zteMessage, UNEXPECTED_VEHICLE_MOVEMENT));
			case 5:
				return convertSuspectedCollision(zteMessage);
			case 7:
				return Arrays.asList(convertEventMessage(zteMessage, LOSS_OF_PRIMARY_SUPPLY));
			default:
				return unkownMessage(zteMessage);
			}
		default:
			return unkownMessage(zteMessage);
		}

	}

	private List<TTPMessage> convertConnectionMessage(ZteMessage zteMessage) {
		EstablishConnectionPayload connectionPayload = (EstablishConnectionPayload) zteMessage.getExtendedPayload();
		Type1Message type1 = getType1Message(
				zteMessage,
				Reason.HEARTBEAT,
				getTTPMessageHeader(1, zteMessage.getFrameId()),
				Instant.now(),
				Optional.empty(),
				null);
		type1.setAgentVersion(connectionPayload.getMcuSoftwareVersion());
		return Arrays.asList(type1);
	}

	private List<TTPMessage> convertTripSummary(ZteMessage zteMessage) {
		TTPMessageHeader ttpMessageHeader = getTTPMessageHeader(1, zteMessage.getFrameId());

		TripSummaryPayload ztePayload = (TripSummaryPayload) zteMessage.getExtendedPayload();

		Type1Message tripStart = getType1Message(
				zteMessage,
				TRIP_START,
				ttpMessageHeader,
				ztePayload.getIgnitionOn(),
				Optional.of(ztePayload.getIgnitionOnGPSData()),
				null);

		RC4ExtendedPayload rc4ExtendedPayload = new RC4ExtendedPayload();

		//payload.setVersion();
		//payload.setTripMovingTimeSeconds();
		rc4ExtendedPayload.setTripIdleTimeSeconds(ztePayload.getIdleTimeSec());
		rc4ExtendedPayload.setTripMaxSpeedKph(ztePayload.getMaxSpeedKmH());
		//payload.setCo2ProducedGrams();
		rc4ExtendedPayload.setTripDistanceMeters(ztePayload.getDistanceMetres());
		rc4ExtendedPayload.setTripFuelMl(ztePayload.getFuelUsedMl());
		//payload.setWastedFuelWrongGearMl();
		//payload.setWastedFuelAccBreakingMl();
		//payload.setWastedFueldIdlingMl();
		//payload.setWastedFuelCongestionMl();
		//payload.setWastedFuelOverSpeedingMl();

		Type1Message tripEnd = getType1Message(
				zteMessage,
				TRIP_END,
				ttpMessageHeader,
				ztePayload.getIgnitionOff(),
				Optional.of(ztePayload.getIgnitionOffGPSData()),
				rc4ExtendedPayload);

		return Arrays.asList(tripStart, tripEnd);

	}

	private List<TTPMessage> convertIgnitionOnOff(ZteMessage zteMessage, Reason reason) {
		TTPMessageHeader ttpMessageHeader = getTTPMessageHeader(1, zteMessage.getFrameId());

		IgnitionOnOffPayload ztePayload = (IgnitionOnOffPayload) zteMessage.getExtendedPayload();

		Type1Message ignitionOnOffMessage = getType1Message(
				zteMessage,
				reason,
				ttpMessageHeader,
				ztePayload.getReportTime(),
				Optional.of(ztePayload.getGpsData()),
				null);

		List<DTC> obdDtcs = ztePayload.getDtcs();
		obdDtcs.addAll(ztePayload.getPrivateDtcs());

		RC116ExtendedPayload rc116ExtendedPayload = getrc116ExtendedPayload(obdDtcs);

		Type1Message obdMessage = getType1Message(
				zteMessage,
				OBD_FAULT_CODES,
				ttpMessageHeader,
				ztePayload.getReportTime(),
				Optional.of(ztePayload.getGpsData()),
				rc116ExtendedPayload);

		return Arrays.asList(ignitionOnOffMessage, obdMessage);
	}

	private RC116ExtendedPayload getrc116ExtendedPayload(List<DTC> obdDtcs) {
		RC116ExtendedPayload rc116ExtendedPayload = new RC116ExtendedPayload();

		List<String> dtcs = new ArrayList<>();
		List<String> ptcs = new ArrayList<>();

		for (DTC dtc : obdDtcs) {

			switch (dtc.getState()) {
			case 0:
			case 2:
				dtcs.add(dtc.getDtcCodeAsString());
				break;
			case 1:
			default:
				ptcs.add(dtc.getDtcCodeAsString());
			}
		}
		rc116ExtendedPayload.setDtcs(dtcs);
		rc116ExtendedPayload.setDtcCount(dtcs.size());

		rc116ExtendedPayload.setPtcs(ptcs);
		rc116ExtendedPayload.setPtcCount(ptcs.size());
		return rc116ExtendedPayload;
	}

	private List<TTPMessage> convertExcessiveDrivingEvent(ZteMessage zteMessage, Reason reason) {

		Type1Message ttpMessage = convertEventMessage(zteMessage, reason);

		RC42_43_44ExtendedPayload rc42_43_44ExtendedPayload = new RC42_43_44ExtendedPayload();

		ttpMessage.setExtendedPayload(rc42_43_44ExtendedPayload);

		return Arrays.asList(ttpMessage);
	}

	private List<TTPMessage> convertExcessiveIdle(ZteMessage zteMessage) {
		TTPMessageHeader ttpMessageHeader = getTTPMessageHeader(1, zteMessage.getFrameId());

		ZteEventPayload ztePayload = (ZteEventPayload) zteMessage.getExtendedPayload();

		Type1Message idleStart = getType1Message(
				zteMessage,
				IDLE_START,
				ttpMessageHeader,
				ztePayload.getReportTime().minusSeconds(180), //TODO make these times configurable based on device settings, zte default idle = 180 seconds
				Optional.of(ztePayload.getGpsData()),
				null);

		Type1Message excessiveIdle = getType1Message(
				zteMessage,
				EXCESSIVE_IDLE_START,
				ttpMessageHeader,
				ztePayload.getReportTime().minusSeconds(150),
				Optional.of(ztePayload.getGpsData()),
				null);

		Type1Message idleEnd = getType1Message(
				zteMessage,
				IDLE_END,
				ttpMessageHeader,
				ztePayload.getReportTime(),
				Optional.of(ztePayload.getGpsData()),
				null);

		return Arrays.asList(idleStart, excessiveIdle, idleEnd);
	}

	private List<TTPMessage> convertGPSData(ZteMessage zteMessage) {
		TTPMessageHeader ttpMessageHeader = getTTPMessageHeader(3, zteMessage.getFrameId());

		GPSDataPayload dataPayload = (GPSDataPayload) zteMessage.getExtendedPayload();

		RC130ExtendedPayload rc130ExtendedPayload = new RC130ExtendedPayload();

		List<GPSData> gpsDataPoints = dataPayload.getGpsDataPoints().stream()
				.sorted(Comparator.comparing(GPSData::getTime).reversed())
				.collect(Collectors.toList());
		
		if (gpsDataPoints.size() > 0) {
			rc130ExtendedPayload.setDeltaCount(gpsDataPoints.size());
			rc130ExtendedPayload.setLogs(convertGPSDataPoints(gpsDataPoints));
		}

		Type3Message type3Message = new Type3Message(ttpMessageHeader);

		type3Message.setUnitSerialNumber(zteMessage.getImei());
		type3Message.setReason(ENHANCED_HIGH_FREQUENCY_LOG);
		type3Message.setExtendedPayload(rc130ExtendedPayload);

		return Arrays.asList(type3Message);
	}

	private List<DeltaEnhancedV0> convertGPSDataPoints(List<GPSData> gpsDataPoints) {
		List<DeltaEnhancedV0> logs = new ArrayList<>();

		for (int i = 0; i < gpsDataPoints.size(); i++) {
			GPSData gpsData = gpsDataPoints.get(i);
			DeltaEnhancedV0 gpsLog = new DeltaEnhancedV0(
					gpsData.getLocation().getLat(), 
					gpsData.getLocation().getLng(),
					gpsData.getHeading(),
					gpsData.getSpeedKnots(), 
					gpsData.getGPSQuality(), 
					gpsData.getTime(),
					gpsData.getHdop(),
					gpsData.getVdop(),
					gpsData.getPdop(),
					gpsData.getSatelliteCount());
			logs.add(0, gpsLog);
		}
		return logs;
	}

	private List<TTPMessage> convertVin(ZteMessage zteMessage) {
		TTPMessageHeader ttpMessageHeader = getTTPMessageHeader(1, zteMessage.getFrameId());

		VINPayload ztePayload = (VINPayload) zteMessage.getExtendedPayload();

		RC114ExtendedPayload rc114ExtendedPayload = new RC114ExtendedPayload();

		rc114ExtendedPayload.setVin(ztePayload.getVin());

		Type1Message type1Message = getType1Message(
				zteMessage,
				VEHICLE_SIG,
				ttpMessageHeader,
				ztePayload.getReportTime(),
				Optional.empty(),
				rc114ExtendedPayload);

		return Arrays.asList(type1Message);
	}

	private List<TTPMessage> convertVehicleDataFlow(ZteMessage zteMessage) {
		Type1Message type1Message = convertEventMessage(zteMessage, HEARTBEAT); //TODO determine type of message for
		// vehicle
		// data flow
		VehicleDataFlowPayload ztePayload = (VehicleDataFlowPayload) zteMessage.getExtendedPayload();
		type1Message.setPrimaryVoltage(ztePayload.getBatteryVoltage());
		type1Message.setPreservedOdometer(ztePayload.getHistoricalDistanceMetres());

		return Arrays.asList(type1Message);
	}

	private void convertDeviceReboot(ZteMessage zteMessage) {

	}

	private List<TTPMessage> convertOBDFaultCodes(ZteMessage zteMessage) {
		TTPMessageHeader ttpMessageHeader = getTTPMessageHeader(1, zteMessage.getFrameId());

		DTCPayload ztePayload = (DTCPayload) zteMessage.getExtendedPayload();

		List<DTC> obdDtcs = ztePayload.getObdDtcs();
		obdDtcs.addAll(ztePayload.getPrivateDtcs());

		RC116ExtendedPayload rc116ExtendedPayload = getrc116ExtendedPayload(obdDtcs);

		Type1Message type1Message = getType1Message(
				zteMessage,
				OBD_FAULT_CODES,
				ttpMessageHeader,
				ztePayload.getReportTime(),
				Optional.empty(),
				rc116ExtendedPayload);

		return Arrays.asList(type1Message);
	}

	private List<TTPMessage> convertSuspectedCollision(ZteMessage zteMessage) {
		CollisionPayload collisionPayload = (CollisionPayload) zteMessage.getExtendedPayload();
		Type1Message type1Message;
		if (collisionPayload.getMagnitudeG() > 3.0) {
			type1Message = convertEventMessage(zteMessage, CRASH_DETECTION);
			RC103ExtendedPayload rc103ExtendedPayload = new RC103ExtendedPayload();
			rc103ExtendedPayload.setEndSpeed(collisionPayload.getGpsData().getSpeedKnots());
			//rc103ExtendedPayload.setImpactEnergyJ(collisionPayload.getMagnitudeG()); TODO convert g's to energy if
			// possible
			type1Message.setExtendedPayload(rc103ExtendedPayload);
		} else {
			type1Message = convertEventMessage(zteMessage, IMPACT_DETECTED);

			RC131ExtendedPayload rc131ExtendedPayload = new RC131ExtendedPayload();
			rc131ExtendedPayload.setEndSpeed(collisionPayload.getGpsData().getSpeedKnots());
			//rc131ExtendedPayload.setImpactEnergyJ(collisionPayload.getMagnitudeG()); TODO convert g's to energy if
			// possible
			type1Message.setExtendedPayload(rc131ExtendedPayload);
		}

		return Arrays.asList(type1Message);
	}

	private List<TTPMessage> convertLowBattery(ZteMessage zteMessage) {
		/*LowBatteryPayload lowBatteryPayload = (LowBatteryPayload) zteMessage.getExtendedPayload();
		Type1Message type1Message;
		type1Message = convertEventMessage(zteMessage, PRIMARY_SUPPLY_LOW);
		type1Message.setPrimaryVoltage(lowBatteryPayload.getVoltage());
		return Arrays.asList(type1Message);*/
		return Collections.EMPTY_LIST;
	}

	private Type1Message convertEventMessage(ZteMessage zteMessage, Reason reason) {
		TTPMessageHeader ttpMessageHeader = getTTPMessageHeader(1, zteMessage.getFrameId());

		ZteEventPayload ztePayload = (ZteEventPayload) zteMessage.getExtendedPayload();

		Type1Message type1Message = getType1Message(
				zteMessage,
				reason,
				ttpMessageHeader,
				ztePayload.getReportTime(),
				Optional.of(ztePayload.getGpsData()),
				null);
		return type1Message;
	}

	private Type1Message getType1Message(ZteMessage zteMessage, Reason reason, TTPMessageHeader ttpMessageHeader,
			Instant created, Optional<GPSData> gpsData, ExtendedPayload extendedPayload) {
		Type1Message ttpMessage = new Type1Message(ttpMessageHeader);
		ttpMessage.setUnitSerialNumber(zteMessage.getImei());
		ttpMessage.setCreated(created);
		ttpMessage.setReason(reason);
		ttpMessage.setExtendedPayload(extendedPayload);
		ttpMessage.setMessageAge(MessageAge.Live);
		if(gpsData.isPresent()){
			ttpMessage.setGpsSample(gpsData.get().getTime());
			ttpMessage.setHeading(gpsData.get().getHeading());
			ttpMessage.setLocation(gpsData.get().getLocation());
			ttpMessage.setSpeedResolutionUnit(SpeedResolutionUnit.Kph);
			ttpMessage.setTravelSpeed(gpsData.get().getSpeedKMpH().intValue());
			ttpMessage.setGpsQuality(gpsData.get().getGPSQuality());
		}
		return ttpMessage;
	}

	private List<TTPMessage> unkownMessage(ZteMessage zteMessage) {
		TTPMessageHeader ttpMessageHeader = getTTPMessageHeader(1, zteMessage.getFrameId());
		Type1Message type1Message = getType1Message(
				zteMessage, UNKNOWN, ttpMessageHeader, Instant.now(), Optional.empty(), null);
		return Arrays.asList(type1Message);
	}

	private TTPMessageHeader getTTPMessageHeader(int msgType, Integer msgId) {
		TTPMessageHeader ttpMessageHeader = new TTPMessageHeader();
		ttpMessageHeader.setMsgType(msgType);
		ttpMessageHeader.setHostMsgId(msgId);
		return ttpMessageHeader;
	}

}
