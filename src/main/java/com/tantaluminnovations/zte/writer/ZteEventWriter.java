package com.tantaluminnovations.zte.writer;

import com.tantaluminnovations.zte.model.DTC;
import com.tantaluminnovations.zte.model.GPSData;
import com.tantaluminnovations.zte.model.MessageVersion;
import com.tantaluminnovations.zte.model.payload.*;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.MoreObjects.firstNonNull;
import static javax.xml.bind.DatatypeConverter.parseHexBinary;
import static org.apache.commons.lang3.StringUtils.leftPad;
import static org.apache.commons.lang3.StringUtils.rightPad;

public class ZteEventWriter extends ZteWriter {

	public byte[] writeConnection(String imei, byte[] key, int frameId, EstablishConnectionPayload connectionPayload) {
		String mcuSoftwareVersion = connectionPayload.getMcuSoftwareVersion();
		String modemSoftwareVersion = connectionPayload.getModemSoftwareVersion();
		int dataLength = 10 + mcuSoftwareVersion.length() + modemSoftwareVersion.length();
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		String majorMinorRevisionBinary = leftPad(Integer.toBinaryString(connectionPayload.getMajorVersion()), 4, '0')
										  + leftPad(Integer.toBinaryString(connectionPayload.getMinorVersion()), 6, '0')
										  + leftPad(Integer.toBinaryString(connectionPayload.getRevisionValue()), 6, '0');
		byte[] data = ByteBuffer.allocate(dataLength)
				.putShort((short) connectionPayload.getProtocolVersion())
				.put((byte) Integer.parseInt(majorMinorRevisionBinary.substring(0, 8), 2))
				.put((byte) Integer.parseInt(majorMinorRevisionBinary.substring(8, 16), 2))
				.put((byte) mcuSoftwareVersion.length())
				.put(mcuSoftwareVersion.getBytes())
				.put((byte) modemSoftwareVersion.length())
				.put(modemSoftwareVersion.getBytes())
				.putInt((int) connectionPayload.getReportTime().getEpochSecond())
				.array();
		return writeMessage(imei, frameId, 0x01, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeDisconnect(String imei, byte[] key, int frameId, Instant reportTime) {
		int dataLength = 4;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.putInt((int) reportTime.getEpochSecond())
				.array();
		return writeMessage(imei, frameId, 0x0E, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writePing(String imei, byte[] key, int frameId) {
		int dataLength = 1;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0)
				.array();
		return writeMessage(imei, frameId, 0x0C, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeTripSummary(String imei, byte[] key, int frameId, TripSummaryPayload summaryPayload) {
		int dataLength = 84;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		String fuelUsedBinary = leftPad(Integer.toBinaryString(summaryPayload.getFuelUsedMl()), 24, '0');
		String distanceBinary = Integer.toBinaryString(summaryPayload.getDistanceSource())
								+ leftPad(Integer.toBinaryString(summaryPayload.getDistanceMetres()), 23, '0');
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0)
				.put((byte) 0)
				.putInt((int) summaryPayload.getIgnitionOn().getEpochSecond())
				.put(convertGpsDataToByteArray(summaryPayload.getIgnitionOnGPSData()))
				.putInt((int) summaryPayload.getIgnitionOff().getEpochSecond())
				.put(convertGpsDataToByteArray(summaryPayload.getIgnitionOffGPSData()))
				.put((byte) Integer.parseInt(distanceBinary.substring(0, 8), 2))
				.put((byte) Integer.parseInt(distanceBinary.substring(8, 16), 2))
				.put((byte) Integer.parseInt(distanceBinary.substring(16), 2))
				.put((byte) Integer.parseInt(fuelUsedBinary.substring(0, 8), 2))
				.put((byte) Integer.parseInt(fuelUsedBinary.substring(8, 16), 2))
				.put((byte) Integer.parseInt(fuelUsedBinary.substring(16), 2))
				.put(summaryPayload.getMaxSpeedKmH().byteValue())
				.putShort(summaryPayload.getIdleTimeSec().shortValue())
				.putShort(summaryPayload.getIdleFuelUsedMl().shortValue())
				.put(summaryPayload.getRapidAccelerationCount().byteValue())
				.put(summaryPayload.getRapidDecelerationCount().byteValue())
				.put(summaryPayload.getHardCorneringCount().byteValue())
				.putInt(summaryPayload.getHistoricalDistanceMetres())
				.putInt(summaryPayload.getHistoricalFuelUsedMl())
				.putInt(summaryPayload.getHistoricalTimeDrivingSecs())
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeIgnitionOnOff(String imei, byte[] key, int frameId, IgnitionOnOffPayload ignitionOnOffPayload,
			boolean igntionOn) {
		List<DTC> obdDtcs = ignitionOnOffPayload.getDtcs();
		List<DTC> privateDtcs = ignitionOnOffPayload.getPrivateDtcs();
		byte[] dtcData = convertDtcsToByteArray(obdDtcs, privateDtcs);

		int dataLength = 31 + dtcData.length;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0)
				.put((byte) (igntionOn ? 1 : 2))
				.put(ignitionOnOffPayload.getType().byteValue())
				.putInt((int) ignitionOnOffPayload.getReportTime().getEpochSecond())
				.put(convertGpsDataToByteArray(ignitionOnOffPayload.getGpsData()))
				.put(dtcData)
				.array();
		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeRapidAccel(String imei, byte[] key, int frameId, RapidAccelDecelPayload eventPayload) {
		return writeRapidAccelDecel(imei, key, frameId, eventPayload, 0);
	}

	public byte[] writeRapidDecel(String imei, byte[] key, int frameId, RapidAccelDecelPayload eventPayload) {
		return writeRapidAccelDecel(imei, key, frameId, eventPayload, 1);
	}

	private byte[] writeRapidAccelDecel(String imei, byte[] key, int frameId, RapidAccelDecelPayload eventPayload,
			int dataTypeMinor) {
		boolean isAccel = dataTypeMinor == 0;
		byte[] additionalData = ByteBuffer.allocate(3)
				.put(eventPayload.getInitialSpeedKmH().byteValue())
				.put(eventPayload.getFinalSpeedKmH().byteValue())
				.put((byte) (eventPayload.getAccelerationMpS2() * 10 * (isAccel ? 1 : -1)))
				.array();
		return writeDrivingEvent(imei, key, frameId, eventPayload, dataTypeMinor, additionalData);
	}

	public byte[] writeHardCornering(String imei, byte[] key, int frameId, HardCorneringPayload eventPayload) {
		byte[] additionalData = ByteBuffer.allocate(1)
				.put((byte) (eventPayload.getTurningAccelerationGpS2() * 10))
				.array();
		return writeDrivingEvent(imei, key, frameId, eventPayload, 2, additionalData);
	}

	public byte[] writeExceedIdle(String imei, byte[] key, int frameId, ZteEventPayload eventPayload) {
		return writeDrivingEvent(imei, key, frameId, eventPayload, 3, new byte[0]);
	}

	public byte[] writeDrivingTired(String imei, byte[] key, int frameId, ZteEventPayload eventPayload) {
		return writeDrivingEvent(imei, key, frameId, eventPayload, 4, new byte[0]);
	}

	private byte[] writeDrivingEvent(String imei, byte[] key, int frameId, ZteEventPayload eventPayload,
			int dataTypeMinor, byte[] additionalData) {
		return writeZteEventPayload(imei, key, frameId, eventPayload, 1, dataTypeMinor, additionalData);
	}

	public byte[] writeGpsData(String imei, byte[] key, int frameId, GPSDataPayload gpsDataPayload) {
		List<GPSData> gpsDataPoints = gpsDataPayload.getGpsDataPoints();
		int dataLength = 3 + (24 * gpsDataPoints.size());
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		ByteBuffer byteBuffer = ByteBuffer.allocate(dataLength)
				.put((byte) 2)
				.put((byte) 0)
				.put((byte) gpsDataPoints.size());
		for (GPSData gpsData : gpsDataPoints) {
			byteBuffer = byteBuffer.put(convertGpsDataToByteArray(gpsData));
		}
		byte[] data = byteBuffer
				.array();
		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeVinCode(String imei, byte[] key, int frameId, VINPayload vinPayload) {
		int dataLength = 8 + vinPayload.getVin().length();
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 3)
				.put((byte) 0)
				.putInt((int) vinPayload.getReportTime().getEpochSecond())
				.put(vinPayload.getProtocolType().byteValue())
				.put((byte) vinPayload.getVin().length())
				.put(vinPayload.getVin().getBytes())
				.array();
		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeVehicleDataFlow(String imei, byte[] key, int frameId,
			VehicleDataFlowPayload vehicleDataFlowPayload) {
		int dataLength = 71;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;

		int mil = 0xffff;
		if(vehicleDataFlowPayload.getMilState() != null) {
			boolean milDistanceIsReadable = vehicleDataFlowPayload.getMilDistanceKm() != null && vehicleDataFlowPayload.getMilDistanceKm() < 0xfffe;
			Integer milDistanceKm = milDistanceIsReadable ? vehicleDataFlowPayload.getMilDistanceKm() : 0xfffe;
			mil = vehicleDataFlowPayload.getMilState() ? milDistanceKm : 0;
		}
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 3)
				.put((byte) 1)
				.putInt((int) vehicleDataFlowPayload.getReportTime().getEpochSecond())
				.put(vehicleDataFlowPayload.getDataNumber().byteValue())
				.put(convertGpsDataToByteArray(vehicleDataFlowPayload.getGpsData()))
				.putShort(firstNonNull(vehicleDataFlowPayload.getRpm(), 0xFFFF).shortValue())
				.putShort(firstNonNull(vehicleDataFlowPayload.getSpeedKmH(), 0xFFFF).shortValue())
				.putShort((short) (firstNonNull(vehicleDataFlowPayload.getEngineCoolantTemperatureCelsius(), 0xffd7)
								   + 40))
				.putShort((short) (firstNonNull(vehicleDataFlowPayload.getThrottlePosition(), 6553.5) * 10))
				.putShort((short) (firstNonNull(vehicleDataFlowPayload.getEngineDutyPercentage(), 6553.5) * 10))
				.putShort((short) (firstNonNull(vehicleDataFlowPayload.getIntakeAirFlowGramsPerSecond(), 6553.5) * 10))
				.putShort((short) (firstNonNull(vehicleDataFlowPayload.getIntakeAirTemperatureCelsius(), 0xffd7) + 40))
				.putShort(firstNonNull(vehicleDataFlowPayload.getIntakeAirPressureKPA(), 0xFFFF).shortValue())
				.put((byte) (firstNonNull(vehicleDataFlowPayload.getBatteryVoltage(), 25.5) * 10))
				.putShort((short) (firstNonNull(vehicleDataFlowPayload.getFuelLevelInput(), 655.35) * 100))
				.putShort(firstNonNull(vehicleDataFlowPayload.getNoDtcsDistanceTravelledKM(), 0xFFFF).shortValue())
				.putShort((short) mil)
				.putInt(firstNonNull(vehicleDataFlowPayload.getHistoricalDistanceMetres(), 0))
				.putInt(firstNonNull(vehicleDataFlowPayload.getHistoricalFuelUsedMl(), 0))
				.putInt(firstNonNull(vehicleDataFlowPayload.getHistoricalDrivingTimeSecs(), 0))
				.putInt(0xFFFFFFFF) //Reserved
				.put((byte) 0xFF) //Reserved
				.array();
		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeDtcCode(String imei, byte[] key, int frameId, DTCPayload dtcPayload) {
		byte[] dtcByteArray = convertDtcsToByteArray(dtcPayload.getObdDtcs(), dtcPayload.getPrivateDtcs());
		int dataLength = 6 + dtcByteArray.length;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 5)
				.put((byte) 0)
				.putInt((int) dtcPayload.getReportTime().getEpochSecond())
				.put(dtcByteArray)
				.array();
		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeLowBattery(String imei, byte[] key, int frameId, LowBatteryPayload lowBatteryPayload) {
		byte[] additionalData = ByteBuffer.allocate(1)
				.put((byte) (lowBatteryPayload.getVoltage() * 10))
				.array();
		return writeZteEventPayload(imei, key, frameId, lowBatteryPayload, 5, 1, additionalData);
	}

	public byte[] writeVibrationAfterIgnitionOff(String imei, byte[] key, int frameId,
			VibrationPayload vibrationPayload) {
		byte[] additionalData = ByteBuffer.allocate(2)
				.putShort(vibrationPayload.getMagnitudeMG().shortValue())
				.array();
		return writeZteEventPayload(imei, key, frameId, vibrationPayload, 5, 2, additionalData);
	}

	public byte[] writeSuspectedCollision(String imei, byte[] key, int frameId, CollisionPayload collisionPayload) {
		byte[] additionalData = ByteBuffer.allocate(1)
				.put((byte) (collisionPayload.getMagnitudeG() * 10))
				.array();
		return writeZteEventPayload(imei, key, frameId, collisionPayload, 5, 5, additionalData);
	}

	public byte[] writeDeviceDisconnect(String imei, byte[] key, int frameId,
			DeviceDisconnectPayload deviceDisconnectPayload, Optional<MessageVersion> messageVersion) {
		byte[] additionalData = new byte[0];
		if (messageVersion.isPresent() && messageVersion.get().compareTo(new MessageVersion(2, 1)) >= 0) {
			additionalData = ByteBuffer.allocate(1)
					.put((byte) deviceDisconnectPayload.getDeviceUnpluggedState().getCode())
					.array();
		}
		return writeZteEventPayload(imei, key, frameId, deviceDisconnectPayload, 5, 7, additionalData);
	}

	public byte[] writeImsi(String imei, byte[] key, int frameId, ImsiPayload imsiPayload) {
		String imsi = imsiPayload.getImsi();
		int dataLength = 7 + imsi.length();
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 0)
				.putInt((int) imsiPayload.getReportTime().getEpochSecond())
				.put((byte) imsi.length())
				.put(imsi.getBytes())
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeIccid(String imei, byte[] key, int frameId, IccidPayload iccidPayload) {
		String iccid = iccidPayload.getIccid();
		int dataLength = 7 + iccid.length();
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 8)
				.putInt((int) iccidPayload.getReportTime().getEpochSecond())
				.put((byte) iccid.length())
				.put(iccid.getBytes())
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeDeviceBug(String imei, byte[] key, int frameId, DeviceBugPayload deviceBugPayload) {
		int dataLength = 7;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 1)
				.putInt((int) deviceBugPayload.getReportTime().getEpochSecond())
				.put((byte) deviceBugPayload.getErrorType().getCode())
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeSleepMode(String imei, byte[] key, int frameId, SleepPayload sleepPayload) {
		int dataLength = 31;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 2)
				.putInt((int) sleepPayload.getReportTime().getEpochSecond())
				.put((byte) (sleepPayload.getBatteryVoltage() * 10))
				.put(convertGpsDataToByteArray(sleepPayload.getGpsData()))
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeWakeup(String imei, byte[] key, int frameId, WakeUpPayload wakeUpPayload) {
		int dataLength = 32;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 3)
				.putInt((int) wakeUpPayload.getReportTime().getEpochSecond())
				.put((byte) (wakeUpPayload.getBatteryVoltage() * 10))
				.put((byte) wakeUpPayload.getTypeCode())
				.put(convertGpsDataToByteArray(wakeUpPayload.getGpsData()))
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeCanNotLocateDevice(String imei, byte[] key, int frameId, ZteEventPayload zteEventPayload) {
		return writeZteEventPayload(imei, key, frameId, zteEventPayload, 4, 4, new byte[0]);
	}

	public byte[] writePowerOnAfterReboot(String imei, byte[] key, int frameId, RebootPayload wakeUpPayload) {
		int dataLength = 11;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 5)
				.putInt((int) wakeUpPayload.getPowerOnTime().getEpochSecond())
				.putInt((int) wakeUpPayload.getLastPowerDownTime().getEpochSecond())
				.put((byte) wakeUpPayload.getType().getCode())
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeUpgradeState(String imei, byte[] key, int frameId, UpgradeStatusPayload upgradeStatus) {
		int dataLength = 7;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 6)
				.putInt((int) upgradeStatus.getReportTime().getEpochSecond())
				.put((byte) upgradeStatus.getUpgradeStatus().getCode())
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeAcceleratorCalibrationStatus(String imei, byte[] key, int frameId,
			AcceleratorStatusPayload acceleratorStatusPayload) {
		int dataLength = 7;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 7)
				.putInt((int) acceleratorStatusPayload.getReportTime().getEpochSecond())
				.put((byte) (acceleratorStatusPayload.isRecalibrationSuccess() ? 1 : 0))
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeSMS(String imei, byte[] key, int frameId, SmsPayload smsPayload) {
		String phone = smsPayload.getPhone();
		String smsEncoding = smsPayload.getSmsEncoding();
		String smsMessage = smsPayload.getSmsMessage();
		int dataLength = 14 + phone.length() + smsEncoding.length() + smsMessage.length();
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 11)
				.putInt((int) smsPayload.getReportTime().getEpochSecond())
				.putInt((int) smsPayload.getSmsTime().getEpochSecond())
				.put((byte) phone.length())
				.put(phone.getBytes())
				.put((byte) smsEncoding.length())
				.put(smsEncoding.getBytes())
				.put((byte) smsMessage.length())
				.put(smsMessage.getBytes())
				.put((byte) (smsPayload.isSmsEnded() ? 0 : 1))
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeDeviceInfo(String imei, byte[] key, int frameId, DeviceInfoPayload deviceInfoPayload) {
		String phone = deviceInfoPayload.getPhone();
		String imsi = deviceInfoPayload.getImsi();
		String iccid = deviceInfoPayload.getIccid();
		String wifiMac = deviceInfoPayload.getWifiMac();
		String btMac = deviceInfoPayload.getBtMac();
		int dataLength = 11 + phone.length() + imsi.length() + iccid.length() + wifiMac.length() + btMac.length();
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 4)
				.put((byte) 12)
				.putInt((int) deviceInfoPayload.getReportTime().getEpochSecond())
				.put((byte) phone.length())
				.put(phone.getBytes())
				.put((byte) iccid.length())
				.put(iccid.getBytes())
				.put((byte) imsi.length())
				.put(imsi.getBytes())
				.put((byte) wifiMac.length())
				.put(wifiMac.getBytes())
				.put((byte) btMac.length())
				.put(btMac.getBytes())
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	private byte[] writeZteEventPayload(String imei, byte[] key, int frameId, ZteEventPayload eventPayload,
			int dataTypeMajor, int dataTypeMinor, byte[] additionalData) {
		int dataLength = 30 + additionalData.length;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) dataTypeMajor)
				.put((byte) dataTypeMinor)
				.putInt((int) eventPayload.getReportTime().getEpochSecond())
				.put(convertGpsDataToByteArray(eventPayload.getGpsData()))
				.put(additionalData)
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	private byte[] convertDtcsToByteArray(List<DTC> obdDtcs, List<DTC> privateDtcs) {
		byte[] dtcData;
		int dtcLength = 2 + (3 * obdDtcs.size()) + (3 * privateDtcs.size());
		ByteBuffer byteBuffer = ByteBuffer.allocate(dtcLength)
				.put((byte) obdDtcs.size());
		for (DTC dtc : obdDtcs) {
			byteBuffer.put(parseHexBinary(dtc.getCode()))
					.put(dtc.getState().byteValue());
		}

		byteBuffer = byteBuffer.put((byte) privateDtcs.size());
		for (DTC dtc : privateDtcs) {
			byteBuffer.put(parseHexBinary(dtc.getCode()))
					.put(dtc.getState().byteValue());
		}
		dtcData = byteBuffer.array();
		return dtcData;
	}

	private byte[] convertGpsDataToByteArray(GPSData gpsData) {
		boolean positiveLng = gpsData.getLocation().getLng() >= 0;
		boolean positiveLat = gpsData.getLocation().getLat() >= 0;
		String binaryString = leftPad(Integer.toBinaryString((int) gpsData.getTime().getEpochSecond()), 32, '0')
							  + '0' //GPS data source, 0 indcates GPS, 1 indicates GSM
							  + Integer.toBinaryString(gpsData.getValidity())
							  + (positiveLng ? 0 : 1) //Longitude multiplier
							  + (positiveLat ? 1 : 0) //Latitude multiplier
							  + leftPad(Integer.toBinaryString(gpsData.getSatelliteCount()), 4, '0')
							  + leftPad(Integer.toBinaryString(gpsData.getAltitude() + 10000), 15, '0')
							  + leftPad(Integer.toBinaryString(
				(int) (gpsData.getLocation().getLng() * 100000 * (positiveLng ? 1 : -1))), 25, '0')
							  + leftPad(Integer.toBinaryString(
				(int) (gpsData.getLocation().getLat() * 100000 * (positiveLat ? 1 : -1))), 24, '0')
							  + leftPad(Integer.toBinaryString((int) (gpsData.getSpeedMpS() * 10)), 12, '0')
							  + "00" //reserved chars
							  + leftPad(Integer.toBinaryString(gpsData.getHeading()), 10, '0')
							  + leftPad(Integer.toBinaryString((int) (gpsData.getPdop() * 10)), 12, '0')
							  + leftPad(Integer.toBinaryString((int) (gpsData.getHdop() * 10)), 12, '0')
							  + leftPad(Integer.toBinaryString((int) (gpsData.getVdop() * 10)), 12, '0');

		binaryString = rightPad(binaryString, 192, '0'); // 3 and a half bytes of reserved chars at end

		ByteBuffer gpsByteBuffer = ByteBuffer.allocate(24);
		int position = 0;
		while (position < binaryString.length()) {
			gpsByteBuffer = gpsByteBuffer.put(
					(byte) Integer.parseInt(binaryString.substring(position, position + 8), 2));
			position = position + 8;
		}
		return gpsByteBuffer.array();
	}

}
