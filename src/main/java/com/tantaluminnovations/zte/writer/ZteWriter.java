package com.tantaluminnovations.zte.writer;

import com.tantaluminnovations.zte.exception.*;
import com.tantaluminnovations.zte.model.*;
import org.slf4j.*;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.nio.*;
import java.util.zip.*;

public class ZteWriter {

	private final Logger log = LoggerFactory.getLogger(ZteWriter.class);

	public static final int HEADER_LENGTH = 24;
	public static final int FOOTER_LENGTH = 6;

	public byte[] writeAck(ZteMessage zteMessage, byte[] deviceKey) {
		switch (zteMessage.getFrameType()) {
		case 1:
			return writeConnAck(zteMessage, deviceKey);
		case 3:
			if (zteMessage.getDataTypeMajor() == 240) {
				return getDeviceRequestResponses();
			}
			return writePubAck(zteMessage, deviceKey);
		case 12:
			return writePingResp(zteMessage, zteMessage.getImei(), deviceKey, zteMessage.getFrameId());
		default:
			return getDeviceRequestResponses();
		}
	}

	public byte[] writeConnAck(ZteMessage message, byte[] key) {
		int dataLength = 1;
		int paddingLength = determinePadding(dataLength);
		int messageLength = HEADER_LENGTH + dataLength + FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0x01) //0x01 authorises session 0x00 forbids session
				.array();

		byte[] ack = writeMessage(message.getImei(), message.getFrameId(), 0x02, messageLength, dataLength, data,
				paddingLength, key);

		return ack;
	}

	public byte[] writePubAck(ZteMessage message, byte[] key) {

		int dataLength = 2;
		int paddingLength = determinePadding(dataLength);
		int messageLength = HEADER_LENGTH + dataLength + FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put(message.getDataTypeMajor().byteValue())
				.put(message.getDataTypeMinor().byteValue())
				.array();

		byte[] ack = writeMessage(message.getImei(), message.getFrameId(), 0x04, messageLength, dataLength, data,
				paddingLength, key);

		return ack;
	}

	public byte[] writePingResp(ZteMessage message, String imei, byte[] key, int frameId) {
		int dataLength = message.getDataLength();
		int paddingLength = determinePadding(dataLength);
		int messageLength = HEADER_LENGTH + dataLength + FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0x01) //0x01 authorises session 0x00 forbids session
				.array();

		byte[] ack = writeMessage(imei, frameId, 0x0D, messageLength, dataLength, data,
				paddingLength, key);

		return ack;
	}

	protected byte[] writeMessage(String imei, int frameNumber, int frameType, int messageLength, int dataLength, byte[]
			data, int paddingLength, byte[] key) {
		ByteBuffer buffer = ByteBuffer.allocate(messageLength)
				.putShort((short) 0x5555) //Frame header
				.putShort((short) messageLength) //Message Length
				.put(imei.getBytes()) //IMEI
				.put((byte) frameType) //FrameType
				.putShort((short) frameNumber) //FrameNumber
				.putShort((short) dataLength) //DataLength
				.put(data); //Effective data

		Adler32 adler32 = new Adler32(); //Checksum
		adler32.update(buffer.array(), 2, messageLength-paddingLength-8);
		buffer.putInt((int) adler32.getValue());

		for(int i = 0; i<paddingLength; i++){
			buffer.put((byte) 0xFF); //Padding
		}
		buffer.putShort((short) 0xAAAA); //Frame End

		byte[] bytes = buffer.array();

		return encryptMessage(bytes, key, messageLength);
	}

	protected byte[] writeFileTransferMessage(String imei, int frameNumber, int frameType, int messageLength, int
			dataLength, byte[] data, int paddingLength, byte[] key, byte[] secondaryData) {

		int actualDataLength = dataLength + 4 + secondaryData.length;

		ByteBuffer buffer = ByteBuffer.allocate(messageLength)
				.putShort((short) 0x5555) //Frame header
				.putShort((short) messageLength) //Message Length
				.put(imei.getBytes()) //IMEI
				.put((byte) frameType) //FrameType
				.putShort((short) frameNumber) //FrameNumber
				.putShort((short) actualDataLength) //DataLength
				.put(data); //Effective data

		Adler32 adler32 = new Adler32(); //Checksum
		adler32.update(secondaryData, 0, secondaryData.length);
		buffer.putInt((int) adler32.getValue())
				.put(secondaryData);

		for(int i = 0; i<paddingLength; i++){
			buffer.put((byte) 0xFF); //Padding
		}
		buffer.putShort((short) 0xAAAA); //Frame End

		byte[] bytes = buffer.array();

		return encryptMessage(bytes, key, messageLength);
	}

	protected byte[] encryptMessage(byte[] message, byte[] key, int messageLength) {
		try{
			SecretKey desKey = new SecretKeySpec(key, "DESede");

			Cipher desCipher;

			// Create the cipher
			desCipher = Cipher.getInstance("DESede/ECB/NoPadding");

			// Initialize the cipher for encryption
			desCipher.init(Cipher.ENCRYPT_MODE, desKey);

			// Encrypt the text
			byte[] textEncrypted = desCipher.doFinal(message, 19, messageLength - 21);

			return ByteBuffer.allocate(messageLength)
					.put(ByteBuffer.wrap(message, 0, 19))
					.put(textEncrypted)
					.putShort((short) 0xAAAA)
					.array();
		} catch (Exception e) {
			throw new ZteMessageException("Failed to encrypt message", e);
		}
	}

	protected int determinePadding(int dataLength) {
		int paddingLength = 8 - Math.floorMod(9 + dataLength, 8);
		return paddingLength == 8 ? 0 : paddingLength;
	}

	protected String generateBitString(int value, int expectedLength) {
		String binary = Integer.toBinaryString(value);
		while (binary.length() < expectedLength) {
			binary = "0" + binary;
		}
		return binary;
	}

	private byte[] getDeviceRequestResponses() {
		return new byte[0]; //TODO write ack messages to device requests
	}
}
