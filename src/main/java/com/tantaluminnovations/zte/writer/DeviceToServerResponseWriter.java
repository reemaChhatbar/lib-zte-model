package com.tantaluminnovations.zte.writer;

import com.tantaluminnovations.zte.exception.*;
import com.tantaluminnovations.zte.model.*;
import org.slf4j.*;

import java.nio.*;
import java.util.zip.*;

import static javax.xml.bind.DatatypeConverter.printHexBinary;

public class DeviceToServerResponseWriter extends ZteWriter{

	private final Logger log = LoggerFactory.getLogger(DeviceToServerResponseWriter.class);

	private static final int PUB_ACK_FRAME_TYPE = 0x04;
	private static final byte DATA_TYPE_MAJOR = (byte) 240;
	private static final byte FILE_UPDATE_DATA_TYPE_MINOR = 1;
	private static final byte AGPS_DATA_TYPE_MINOR = 2;
	private static final byte FILE_UPDATE_VERIFICATION_DATA_TYPE_MINOR = 3;

	public byte[] writeFileUpdate(String imei, byte[] key, int frameId, int fileStartPosition, byte[] filePacket){

		if(filePacket.length > 960){
			throw new ZteMessageException("File chunk too large for message, filePacketSizeBytes="+filePacket.length);
		}

		int secondaryDataLength = 4 + filePacket.length;
		byte[] secondaryData = ByteBuffer.allocate(secondaryDataLength)
				.putInt(fileStartPosition)
				.put(filePacket)
				.array();

		int dataLength = 2;

		byte[] data = ByteBuffer.allocate(dataLength)
				.put(DATA_TYPE_MAJOR)
				.put(FILE_UPDATE_DATA_TYPE_MINOR)
				.array();

		int actualDataLength = dataLength + 4 + secondaryData.length;

		CRC32 crc32 = new CRC32(); //Checksum
		crc32.update(secondaryData, 0, secondaryData.length);
		byte[] actualData = ByteBuffer.allocate(actualDataLength)
				.put(data)
				.putInt((int) crc32.getValue())
				.put(secondaryData)
				.array();

		int paddingLength = determinePadding(actualDataLength);
		int messageLength = HEADER_LENGTH + FOOTER_LENGTH + paddingLength + actualDataLength;

		log.debug("File Update writer for deviceId={}, dataLength={}, messageLength={}, "
				  + "filePacketSize={}", imei, actualDataLength, messageLength, filePacket.length);

		byte[] ack = writeMessage(imei, frameId, PUB_ACK_FRAME_TYPE, messageLength, actualDataLength, actualData,
				paddingLength, key);
		log.debug("Written File Update Response message, imei={}, frameId={}, fileStartPosition={}, rawMessage={}",
				imei, frameId, fileStartPosition, printHexBinary(ack));

		return ack;
	}

	public byte[] writeAGpsResp(ZteMessage message, byte[] key, byte[] aGpsData, int aGpsDataStartPosition) {

		int secondaryDataLength = aGpsData.length + 4;
		byte[] secondaryData = ByteBuffer.allocate(secondaryDataLength)
				.putShort((short) aGpsData.length)
				.putShort((short) aGpsDataStartPosition)
				.put(aGpsData)
				.array();

		int dataLength = 2;
		int paddingLength = determinePadding(dataLength+ secondaryDataLength);
		int messageLength = HEADER_LENGTH + dataLength + FOOTER_LENGTH + paddingLength + secondaryDataLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put(DATA_TYPE_MAJOR)
				.put(AGPS_DATA_TYPE_MINOR)
				.array();

		byte[] ack = writeFileTransferMessage(message.getImei(), message.getFrameId(), PUB_ACK_FRAME_TYPE, messageLength, dataLength, data,
				paddingLength, key, secondaryData);
		return ack;
	}

	public byte[] writeUpdateVerificationAck(String imei, byte[] key, int frameId, int fileSize, String fileName) {
		int dataLength = 7+fileName.length();
		int paddingLength = determinePadding(dataLength);
		int messageLength = HEADER_LENGTH + dataLength + FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put(DATA_TYPE_MAJOR)
				.put(FILE_UPDATE_VERIFICATION_DATA_TYPE_MINOR)
				.put((byte) fileName.length())
				.put(fileName.getBytes())
				.putInt(fileSize)
				.array();

		byte[] ack = writeMessage(imei, frameId, PUB_ACK_FRAME_TYPE, messageLength, dataLength, data,
				paddingLength, key);
		log.debug("Written File Update Verification Response message, imei={}, frameId={}, fileName={}, "
				  + "fileSizeBytes={}, rawMessage={}",
				imei, frameId, fileName, fileSize, printHexBinary(ack));
		return ack;
	}
}