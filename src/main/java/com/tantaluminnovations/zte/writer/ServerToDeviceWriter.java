package com.tantaluminnovations.zte.writer;

import com.tantaluminnovations.zte.model.*;

import java.nio.*;
import java.util.*;
import java.util.stream.*;

public class ServerToDeviceWriter extends ZteWriter {


	//Server to Device Requests

	public byte[] writeDeviceUpdate(String imei, int frameId, byte[] key, String serverAddress, String fileName) {

		int dataLength = 4 + serverAddress.length() + fileName.length();
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0xF1)
				.put((byte) 0x00)
				.put((byte) serverAddress.length())
				.put(serverAddress.getBytes())
				.put((byte) fileName.length())
				.put(fileName.getBytes())
				.array();
		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeVehicleDetection(String imei, int frameId, byte[] key, String serverAddress, String fileName) {

		int dataLength = 2;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0xF1)
				.put((byte) 0x01)
				.array();
		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeSetParameters(String imei, int frameId, byte[] key, List<ZteDeviceParameter> zteDeviceParameters) {

		byte[] parameters = writeParameters(zteDeviceParameters);
		int dataLength = 2 + parameters.length;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0xF1)
				.put((byte) 0x02)
				.put(parameters)
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	private byte[] writeParameters(List<ZteDeviceParameter> zteDeviceParameters) {
		int totalParameterLength = zteDeviceParameters.stream().mapToInt
				(ZteDeviceParameter::getLength).sum();
		ByteBuffer buffer = ByteBuffer.allocate(totalParameterLength);
		for (ZteDeviceParameter zteDeviceParameter : zteDeviceParameters) {
			if (zteDeviceParameter != null) {
				String typeBinary = generateBitString(zteDeviceParameter.getState(), 4);
				String idBinary = generateBitString(zteDeviceParameter.getParameterId(), 12);
				String timeSecondsBinary = generateBitString(zteDeviceParameter.getTimeSec(), 16);
				Integer settingNo = Integer.valueOf(typeBinary + idBinary + timeSecondsBinary, 2);
				buffer.putInt(settingNo);
				if (zteDeviceParameter.getValueClass() == String.class) {
					String parameterValue = (String) zteDeviceParameter.getValue();
					buffer.put((byte) parameterValue.length())
							.put(parameterValue.getBytes());
				} else {
					Integer parameterValue;
					if (zteDeviceParameter.getValueClass() == Double.class) {
						Double value = (Double) zteDeviceParameter.getValue();
						parameterValue = (int) (value / zteDeviceParameter.getValuePrecision());
					} else {
						parameterValue = (int) zteDeviceParameter.getValue();
					}
					switch (zteDeviceParameter.getValueLength()) {
					case 1:
						buffer.put(parameterValue.byteValue());
						break;
					case 2:
						buffer.putShort(parameterValue.shortValue());
						break;
					case 4:
						buffer.putInt(parameterValue);
						break;
					default:
						for (int i = zteDeviceParameter.getValueLength() - 4; i > 0; i--) {
							buffer.put((byte) 0);
						}
						buffer.putInt(parameterValue);
					}

				}
			}
		}
		return buffer.array();
	}

	public byte[] writeParametersInquiry(String imei, int frameId, byte[] key, List<ParameterType> parameterTypes) {
		parameterTypes = parameterTypes.stream().filter(pt -> pt.getParameterId() > 0).collect(Collectors.toList());
		ByteBuffer buffer = ByteBuffer.allocate(parameterTypes.size() * 2);
		for (ParameterType zteDeviceParameter : parameterTypes) {
			String typeBinary = generateBitString(0, 4);
			String idBinary = generateBitString(zteDeviceParameter.getParameterId(), 12);
			Short settingNo = Short.valueOf(typeBinary + idBinary, 2);
			buffer.putShort(settingNo);
		}

		byte[] parameters = buffer.array();
		int dataLength = 2 + parameters.length;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0xF1)
				.put((byte) 0x03)
				.put(parameters)
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeFullParameterInquiry(String imei, int frameId, byte[] key) {
		return writeParametersInquiry(imei, frameId, key, ParameterType.allParameters());
	}

	public byte[] writeLogFileInquiry(String imei, int frameId, byte[] key) {

		int dataLength = 3;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0xF1)
				.put((byte) 0x04)
				.put((byte) 0x00) //All Logs
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeSetVehicleInfo(String imei, int frameId, byte[] key, int manufacturerId, int vehicleMode,
			int enginetype, double fuelConsumptionLitres, int powerConsumptionKW, double displacementLitres,
			int modelYear) {

		int dataLength = 11;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0xF1)
				.put((byte) 0x06)
				.put((byte) manufacturerId) //All Logs
				.put((byte) vehicleMode)
				.put((byte) enginetype)
				.putShort((short) (fuelConsumptionLitres * 100))
				.put((byte) powerConsumptionKW)
				.put((byte) (displacementLitres * 10))
				.putShort((short) modelYear)
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}

	public byte[] writeRecalibrateAccelerator(String imei, int frameId, byte[] key) {

		int dataLength = 2;
		int paddingLength = determinePadding(dataLength);
		int messageLength = ZteWriter.HEADER_LENGTH + dataLength + ZteWriter.FOOTER_LENGTH + paddingLength;
		byte[] data = ByteBuffer.allocate(dataLength)
				.put((byte) 0xF1)
				.put((byte) 0x07)
				.array();

		return writeMessage(imei, frameId, 0x03, messageLength, dataLength, data, paddingLength, key);
	}
}