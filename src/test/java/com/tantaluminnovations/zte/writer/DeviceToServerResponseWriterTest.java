package com.tantaluminnovations.zte.writer;

import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.parser.*;
import org.apache.commons.lang3.*;
import org.junit.*;

import javax.crypto.*;
import java.security.*;
import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

public class DeviceToServerResponseWriterTest {

	DeviceToServerResponseWriter zteWriter = new DeviceToServerResponseWriter();
	ZteParser zteParser = new ZteParser();

	Random random = new Random();

	@Test
	public void testWriteConnAckMessage() throws NoSuchAlgorithmException {
		KeyGenerator keygenerator = KeyGenerator.getInstance("DESede");
		SecretKey key = keygenerator.generateKey();

		ZteMessage zteMessage = new ZteMessage();
		int frameId = random.nextInt(0xFFFF);
		zteMessage.setFrameId(frameId);
		String imei = RandomStringUtils.randomNumeric(15);
		zteMessage.setImei(imei);

		byte[] byteMessage = zteWriter.writeConnAck(zteMessage, key.getEncoded());

		ZteMessage decodedMessage = zteParser.decryptAndParseMessage(byteMessage, key.getEncoded(),
				Optional.of(new MessageVersion(2, 1)));

		assertThat(decodedMessage.getFrameType(), is(0x02));
		assertThat(decodedMessage.getImei(), equalTo(imei));
		assertThat(decodedMessage.getFrameId(), equalTo(frameId));
	}

	@Test
	public void testWritePubAckMessage() throws NoSuchAlgorithmException {
		KeyGenerator keygenerator = KeyGenerator.getInstance("DESede");
		SecretKey key = keygenerator.generateKey();

		ZteMessage zteMessage = new ZteMessage();
		zteMessage.setFrameId(random.nextInt(0xFFFF));
		zteMessage.setImei(RandomStringUtils.randomNumeric(15));
		zteMessage.setDataTypeMajor(random.nextInt(0xF2));
		zteMessage.setDataTypeMinor(random.nextInt(0x03));

		byte[] byteMessage = zteWriter.writePubAck(zteMessage, key.getEncoded());

		ZteMessage decodedMessage = zteParser.decryptAndParseMessage(byteMessage, key.getEncoded(),
				Optional.of(new MessageVersion(2, 1)));

		assertThat(decodedMessage.getFrameType(), is(0x04));
		assertThat(decodedMessage.getImei(), equalTo(zteMessage.getImei()));
		assertThat(decodedMessage.getFrameId(), equalTo(zteMessage.getFrameId()));
		assertThat(decodedMessage.getDataTypeMajor(), equalTo(zteMessage.getDataTypeMajor()));
		assertThat(decodedMessage.getDataTypeMinor(), equalTo(zteMessage.getDataTypeMinor()));
	}

}
