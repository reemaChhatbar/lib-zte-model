package com.tantaluminnovations.zte.writer;

import com.tantaluminnovations.ttp.model.LatLng;
import com.tantaluminnovations.zte.model.DTC;
import com.tantaluminnovations.zte.model.GPSData;
import com.tantaluminnovations.zte.model.MessageVersion;
import com.tantaluminnovations.zte.model.ZteMessage;
import com.tantaluminnovations.zte.model.payload.*;
import com.tantaluminnovations.zte.parser.ZteParser;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static javax.xml.bind.DatatypeConverter.parseHexBinary;
import static javax.xml.bind.DatatypeConverter.printHexBinary;
import static org.junit.Assert.*;

public class ZteEventWriterTest {

	ZteEventWriter eventWriter = new ZteEventWriter();
	ZteParser zteParser = new ZteParser();

	private final String IMEI = "861473123459876";
	private final byte[] KEY = parseHexBinary("bda116778b82a28abd072252b0b75a4b901faa3296dd1043");

	@Test
	public void testWriteConnection(){
		EstablishConnectionPayload expectedPayload = new EstablishConnectionPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setProtocolVersion(2);
		expectedPayload.setMajorVersion(1);
		expectedPayload.setMinorVersion(14);
		expectedPayload.setRevisionValue(63);
		expectedPayload.setMcuSoftwareVersion("MCU_TEST_SOFTWAREV0.0.1B");
		expectedPayload.setModemSoftwareVersion("MODEM_TEST_SOFTWAREV0.0.1B");
		expectedPayload.setReportTime(reportTime);

		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeConnection(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());
		assertOnZteEventMessage(zteMessage, frameId, 1, null, null);
		assertTrue(zteMessage.getExtendedPayload() instanceof EstablishConnectionPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteDisconnect(){
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeDisconnect(IMEI, KEY, frameId, reportTime);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());
		assertOnZteEventMessage(zteMessage, frameId, 0x0E, null, null);
	}

	@Test
	public void testWritePing(){
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writePing(IMEI, KEY, frameId);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());
		assertOnZteEventMessage(zteMessage, frameId, 0x0C, null, null);
	}

	@Test
	public void testWriteTripSummary() {
		TripSummaryPayload expectedPayload = new TripSummaryPayload();
		Instant ignitionOff = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		Instant ignitionOn = ignitionOff.minusSeconds(900);
		expectedPayload.setIgnitionOn(ignitionOn);
		expectedPayload.setIgnitionOnGPSData(timeGeneratedGpsData(ignitionOn));
		expectedPayload.setIgnitionOff(ignitionOff);
		expectedPayload.setIgnitionOffGPSData(timeGeneratedGpsData(ignitionOff));
		expectedPayload.setDistanceMetres(13500);
		expectedPayload.setReportTime(ignitionOff);
		expectedPayload.setFuelUsedMl(2500);
		expectedPayload.setMaxSpeedKmH(90);
		expectedPayload.setIdleTimeSec(30);
		expectedPayload.setIdleFuelUsedMl(50);
		expectedPayload.setRapidAccelerationCount(6);
		expectedPayload.setRapidDecelerationCount(3);
		expectedPayload.setHardCorneringCount(2);
		expectedPayload.setHistoricalDistanceMetres(21225219);
		expectedPayload.setHistoricalFuelUsedMl(4124952);
		expectedPayload.setHistoricalTimeDrivingSecs(9510414);
		expectedPayload.setDistanceSource(0);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeTripSummary(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());
		assertOnZteEventMessage(zteMessage, frameId, 3, 0, 0);
		assertTrue(zteMessage.getExtendedPayload() instanceof TripSummaryPayload);

		TripSummaryPayload tripSummaryPayload = (TripSummaryPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getIgnitionOn(), tripSummaryPayload.getIgnitionOn());
		assertGpsDataEqual(expectedPayload.getIgnitionOnGPSData(), tripSummaryPayload.getIgnitionOnGPSData());
		assertEquals(expectedPayload.getIgnitionOff(), tripSummaryPayload.getIgnitionOff());
		assertGpsDataEqual(expectedPayload.getIgnitionOffGPSData(), tripSummaryPayload.getIgnitionOffGPSData());
		assertEquals(expectedPayload.getDistanceMetres(), tripSummaryPayload.getDistanceMetres());
		assertEquals(expectedPayload.getReportTime(), tripSummaryPayload.getReportTime());
		assertEquals(expectedPayload.getFuelUsedMl(), tripSummaryPayload.getFuelUsedMl());
		assertEquals(expectedPayload.getMaxSpeedKmH(), tripSummaryPayload.getMaxSpeedKmH());
		assertEquals(expectedPayload.getIdleTimeSec(), tripSummaryPayload.getIdleTimeSec());
		assertEquals(expectedPayload.getIdleFuelUsedMl(), tripSummaryPayload.getIdleFuelUsedMl());
		assertEquals(expectedPayload.getRapidAccelerationCount(), tripSummaryPayload.getRapidAccelerationCount());
		assertEquals(expectedPayload.getRapidDecelerationCount(), tripSummaryPayload.getRapidDecelerationCount());
		assertEquals(expectedPayload.getHardCorneringCount(), tripSummaryPayload.getHardCorneringCount());
		assertEquals(expectedPayload.getHistoricalDistanceMetres(), tripSummaryPayload.getHistoricalDistanceMetres());
		assertEquals(expectedPayload.getHistoricalFuelUsedMl(), tripSummaryPayload.getHistoricalFuelUsedMl());
		assertEquals(expectedPayload.getHistoricalTimeDrivingSecs(), tripSummaryPayload.getHistoricalTimeDrivingSecs());
		assertEquals(expectedPayload.getDistanceSource(), tripSummaryPayload.getDistanceSource());
	}

	@Test
	public void testWriteIgnitionOn() {
		IgnitionOnOffPayload expectedPayload = new IgnitionOnOffPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setType(0);
		expectedPayload.setDtcs(generateDtcList());
		expectedPayload.setPrivateDtcs(generateDtcList());
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeIgnitionOnOff(IMEI, KEY, frameId, expectedPayload, true);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 0, 1);
		assertTrue(zteMessage.getExtendedPayload() instanceof IgnitionOnOffPayload);
		IgnitionOnOffPayload ignitionOnOffPayload = (IgnitionOnOffPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), ignitionOnOffPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), ignitionOnOffPayload.getGpsData());
		assertEquals(expectedPayload.getType(), ignitionOnOffPayload.getType());
		assertEquals(expectedPayload.getDtcs(), ignitionOnOffPayload.getDtcs());
		assertEquals(expectedPayload.getPrivateDtcs(), ignitionOnOffPayload.getPrivateDtcs());
	}

	@Test
	public void testWriteIgnitionOff() {
		IgnitionOnOffPayload expectedPayload = new IgnitionOnOffPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setType(0);
		expectedPayload.setDtcs(generateDtcList());
		expectedPayload.setPrivateDtcs(generateDtcList());
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeIgnitionOnOff(IMEI, KEY, frameId, expectedPayload, false);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 0, 2);
		assertTrue(zteMessage.getExtendedPayload() instanceof IgnitionOnOffPayload);
		IgnitionOnOffPayload ignitionOnOffPayload = (IgnitionOnOffPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), ignitionOnOffPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), ignitionOnOffPayload.getGpsData());
		assertEquals(expectedPayload.getType(), ignitionOnOffPayload.getType());
		assertEquals(expectedPayload.getDtcs(), ignitionOnOffPayload.getDtcs());
		assertEquals(expectedPayload.getPrivateDtcs(), ignitionOnOffPayload.getPrivateDtcs());
	}

	@Test
	public void testWriteRapidAccel() {
		RapidAccelDecelPayload expectedPayload = new RapidAccelDecelPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setInitialSpeedKmH(10);
		expectedPayload.setFinalSpeedKmH(40);
		expectedPayload.setAccelerationMpS2(4.8);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeRapidAccel(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 1, 0);
		assertTrue(zteMessage.getExtendedPayload() instanceof RapidAccelDecelPayload);
		RapidAccelDecelPayload rapidAccelDecelPayload = (RapidAccelDecelPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), rapidAccelDecelPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), rapidAccelDecelPayload.getGpsData());
		assertEquals(expectedPayload.getInitialSpeedKmH(), rapidAccelDecelPayload.getInitialSpeedKmH());
		assertEquals(expectedPayload.getFinalSpeedKmH(), rapidAccelDecelPayload.getFinalSpeedKmH());
		assertEquals(expectedPayload.getAccelerationMpS2(), rapidAccelDecelPayload.getAccelerationMpS2());

	}

	@Test
	public void testWriteRapidDecel() {
		RapidAccelDecelPayload expectedPayload = new RapidAccelDecelPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setInitialSpeedKmH(10);
		expectedPayload.setFinalSpeedKmH(40);
		expectedPayload.setAccelerationMpS2(-4.8);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeRapidDecel(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 1, 1);
		assertTrue(zteMessage.getExtendedPayload() instanceof RapidAccelDecelPayload);
		RapidAccelDecelPayload rapidAccelDecelPayload = (RapidAccelDecelPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), rapidAccelDecelPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), rapidAccelDecelPayload.getGpsData());
		assertEquals(expectedPayload.getInitialSpeedKmH(), rapidAccelDecelPayload.getInitialSpeedKmH());
		assertEquals(expectedPayload.getFinalSpeedKmH(), rapidAccelDecelPayload.getFinalSpeedKmH());
		assertEquals(expectedPayload.getAccelerationMpS2(), rapidAccelDecelPayload.getAccelerationMpS2());
	}

	@Test
	public void testWriteHardCornering() {
		HardCorneringPayload expectedPayload = new HardCorneringPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setTurningAccelerationGpS2(4.2);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeHardCornering(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 1, 2);
		assertTrue(zteMessage.getExtendedPayload() instanceof HardCorneringPayload);
		HardCorneringPayload hardCorneringPayload = (HardCorneringPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), hardCorneringPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), hardCorneringPayload.getGpsData());
		assertEquals(expectedPayload.getTurningAccelerationGpS2(), hardCorneringPayload.getTurningAccelerationGpS2());
	}

	@Test
	public void testWriteExceedIdle() {
		ZteEventPayload expectedPayload = new ZteEventPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeExceedIdle(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 1, 3);
		assertTrue(zteMessage.getExtendedPayload() instanceof ZteEventPayload);
		ZteEventPayload zteEventPayload = (ZteEventPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), zteEventPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), zteEventPayload.getGpsData());
	}

	@Test
	public void testWriteDrivingTired() {
		ZteEventPayload expectedPayload = new ZteEventPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeDrivingTired(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 1, 4);
		assertTrue(zteMessage.getExtendedPayload() instanceof ZteEventPayload);
		ZteEventPayload zteEventPayload = (ZteEventPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), zteEventPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), zteEventPayload.getGpsData());
	}

	@Test
	public void testWriteGpsData() {
		GPSDataPayload expectedPayload = new GPSDataPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		int size = (int) ((reportTime.getEpochSecond() % 5) + 10);
		List<GPSData> expectedDataPoints = new ArrayList<>();
		for(int i = 0; i < size; i++){
			expectedDataPoints.add(0, timeGeneratedGpsData(reportTime.minusSeconds(i)));
		}
		expectedPayload.setGpsDataPoints(expectedDataPoints);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);
		byte[] bytes = eventWriter.writeGpsData(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());
		assertOnZteEventMessage(zteMessage, frameId, 3, 2, 0);
		assertTrue(zteMessage.getExtendedPayload() instanceof GPSDataPayload);
		GPSDataPayload gpsDataPayload = (GPSDataPayload) zteMessage.getExtendedPayload();
		List<GPSData> actualDataPoints = gpsDataPayload.getGpsDataPoints();
		for (int j = 0; j < actualDataPoints.size() ; j++){
			assertGpsDataEqual(expectedDataPoints.get(j), actualDataPoints.get(j));
		}
	}

	@Test
	public void testWriteVinCode() {
		VINPayload expectedPayload = new VINPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		int vinLength = 17;
		expectedPayload.setVinLength(vinLength);
		expectedPayload.setVin(RandomStringUtils.randomAlphanumeric(vinLength));
		expectedPayload.setProtocolType(0);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeVinCode(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 3, 0);
		assertTrue(zteMessage.getExtendedPayload() instanceof VINPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteVehicleDataFlow() {
		VehicleDataFlowPayload expectedPayload = new VehicleDataFlowPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setDataNumber(1);
		expectedPayload.setRpm(1200);
		expectedPayload.setSpeedKmH(45);
		expectedPayload.setMilState(false);
		expectedPayload.setMilDistanceKm(0);
		expectedPayload.setHistoricalDistanceMetres(21225219);
		expectedPayload.setHistoricalFuelUsedMl(4124952);
		expectedPayload.setHistoricalDrivingTimeSecs(9510414);
		expectedPayload.setEngineCoolantTemperatureCelsius(80);
		expectedPayload.setThrottlePosition(42.5);
		expectedPayload.setEngineDutyPercentage(63.4);
		expectedPayload.setIntakeAirFlowGramsPerSecond(2.4);
		expectedPayload.setIntakeAirTemperatureCelsius(25);
		expectedPayload.setIntakeAirPressureKPA(112);
		expectedPayload.setBatteryVoltage(12.1);
		expectedPayload.setFuelLevelInput(83.2);
		expectedPayload.setNoDtcsDistanceTravelledKM(223);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);
		byte[] bytes = eventWriter.writeVehicleDataFlow(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());
		assertOnZteEventMessage(zteMessage, frameId, 3, 3, 1);
		assertTrue(zteMessage.getExtendedPayload() instanceof VehicleDataFlowPayload);
		VehicleDataFlowPayload vehicleDataFlowPayload = (VehicleDataFlowPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), vehicleDataFlowPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), vehicleDataFlowPayload.getGpsData());
		assertEquals(expectedPayload.getDataNumber(), vehicleDataFlowPayload.getDataNumber());
		assertEquals(expectedPayload.getRpm(), vehicleDataFlowPayload.getRpm());
		assertEquals(expectedPayload.getSpeedKmH(), vehicleDataFlowPayload.getSpeedKmH());
		assertEquals(expectedPayload.getMilState(), vehicleDataFlowPayload.getMilState());
		if(vehicleDataFlowPayload.getMilState() && expectedPayload.getMilDistanceKm() < 0xfffe){
			assertEquals(expectedPayload.getMilDistanceKm(), vehicleDataFlowPayload.getMilDistanceKm());
		}
		assertEquals(expectedPayload.getHistoricalDistanceMetres(), vehicleDataFlowPayload.getHistoricalDistanceMetres());
		assertEquals(expectedPayload.getHistoricalFuelUsedMl(), vehicleDataFlowPayload.getHistoricalFuelUsedMl());
		assertEquals(expectedPayload.getHistoricalDrivingTimeSecs(), vehicleDataFlowPayload.getHistoricalDrivingTimeSecs());
		assertEquals(expectedPayload.getEngineCoolantTemperatureCelsius(), vehicleDataFlowPayload.getEngineCoolantTemperatureCelsius());
		assertEquals(expectedPayload.getThrottlePosition(), vehicleDataFlowPayload.getThrottlePosition(), 0.01);
		assertEquals(expectedPayload.getEngineDutyPercentage(), vehicleDataFlowPayload.getEngineDutyPercentage(), 0.01);
		assertEquals(expectedPayload.getIntakeAirFlowGramsPerSecond(), vehicleDataFlowPayload.getIntakeAirFlowGramsPerSecond(), 0.01);
		assertEquals(expectedPayload.getIntakeAirTemperatureCelsius(), vehicleDataFlowPayload.getIntakeAirTemperatureCelsius());
		assertEquals(expectedPayload.getIntakeAirPressureKPA(), vehicleDataFlowPayload.getIntakeAirPressureKPA());
		assertEquals(expectedPayload.getBatteryVoltage(), vehicleDataFlowPayload.getBatteryVoltage(), 0.01);
		assertEquals(expectedPayload.getFuelLevelInput(), vehicleDataFlowPayload.getFuelLevelInput());
		assertEquals(expectedPayload.getNoDtcsDistanceTravelledKM(), vehicleDataFlowPayload.getNoDtcsDistanceTravelledKM());
	}

	@Test
	public void testWriteDtcCode() {
		DTCPayload expectedPayload = new DTCPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		List<DTC> obdDtcs = generateDtcList();
		expectedPayload.setObdDtcs(obdDtcs);
		expectedPayload.setObdDtcCount(obdDtcs.size());
		List<DTC> privateDtcs = generateDtcList();
		expectedPayload.setPrivateDtcCount(privateDtcs.size());
		expectedPayload.setPrivateDtcs(privateDtcs);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);
		byte[] bytes = eventWriter.writeDtcCode(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());
		assertOnZteEventMessage(zteMessage, frameId, 3, 5, 0);
		assertTrue(zteMessage.getExtendedPayload() instanceof DTCPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteLowBattery() {
		LowBatteryPayload expectedPayload = new LowBatteryPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setVoltage(10.3);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeLowBattery(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 5, 1);
		assertTrue(zteMessage.getExtendedPayload() instanceof LowBatteryPayload);
		LowBatteryPayload lowBatteryPayload = (LowBatteryPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), lowBatteryPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), lowBatteryPayload.getGpsData());
		assertEquals(expectedPayload.getVoltage(), lowBatteryPayload.getVoltage());

	}

	@Test
	public void testWriteVibrationAfterIgnitionOff() {
		VibrationPayload expectedPayload = new VibrationPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setMagnitudeMG(239);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeVibrationAfterIgnitionOff(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 5, 2);
		assertTrue(zteMessage.getExtendedPayload() instanceof VibrationPayload);
		VibrationPayload vibrationPayload = (VibrationPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), vibrationPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), vibrationPayload.getGpsData());
		assertEquals(expectedPayload.getMagnitudeMG(), vibrationPayload.getMagnitudeMG());
	}

	@Test
	public void testWriteSuspectedCollision() {
		CollisionPayload expectedPayload = new CollisionPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setMagnitudeG(5.7);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeSuspectedCollision(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 5, 5);
		assertTrue(zteMessage.getExtendedPayload() instanceof CollisionPayload);
		CollisionPayload collisionPayload = (CollisionPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), collisionPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), collisionPayload.getGpsData());
		assertEquals(expectedPayload.getMagnitudeG(), collisionPayload.getMagnitudeG());
	}

	@Test
	public void testWriteDeviceDisconnect() {
		DeviceDisconnectPayload expectedPayload = new DeviceDisconnectPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeDeviceDisconnect(IMEI, KEY, frameId, expectedPayload, Optional.empty());

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 5, 7);
		assertTrue(zteMessage.getExtendedPayload() instanceof ZteEventPayload);
		DeviceDisconnectPayload deviceDisconnectPayload = (DeviceDisconnectPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), deviceDisconnectPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), deviceDisconnectPayload.getGpsData());
		assertEquals(expectedPayload.getDeviceUnpluggedState(), deviceDisconnectPayload.getDeviceUnpluggedState());
	}

	@Test
	public void testWriteDeviceDisconnectV2point1() {
		DeviceDisconnectPayload expectedPayload = new DeviceDisconnectPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setDeviceUnpluggedState(DeviceDisconnectPayload.DeviceUnpluggedState.VEHICLE_IGNITION_STILL_ON);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);
		MessageVersion messageVersion = new MessageVersion(2, 1);

		byte[] bytes = eventWriter.writeDeviceDisconnect(IMEI, KEY, frameId, expectedPayload, Optional.of(
				messageVersion));

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.of(messageVersion));

		assertOnZteEventMessage(zteMessage, frameId, 3, 5, 7);
		assertTrue(zteMessage.getExtendedPayload() instanceof ZteEventPayload);
		DeviceDisconnectPayload deviceDisconnectPayload = (DeviceDisconnectPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), deviceDisconnectPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), deviceDisconnectPayload.getGpsData());
		assertEquals(expectedPayload.getDeviceUnpluggedState(), deviceDisconnectPayload.getDeviceUnpluggedState());
	}

	@Test
	public void testWriteImsi() {
		ImsiPayload expectedPayload = new ImsiPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setImsi(RandomStringUtils.randomNumeric(15));
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeImsi(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 0);
		assertTrue(zteMessage.getExtendedPayload() instanceof ImsiPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteIccid() {
		IccidPayload expectedPayload = new IccidPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setIccid(RandomStringUtils.randomNumeric(15));
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeIccid(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 8);
		assertTrue(zteMessage.getExtendedPayload() instanceof IccidPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteDeviceBug() {
		DeviceBugPayload expectedPayload = new DeviceBugPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setErrorType(DeviceBugPayload.ErrorType.CAN_BUS_COMMUNICATION);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeDeviceBug(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 1);
		assertTrue(zteMessage.getExtendedPayload() instanceof DeviceBugPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteSleepMode() {
		SleepPayload expectedPayload = new SleepPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setBatteryVoltage(11.8);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeSleepMode(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 2);
		assertTrue(zteMessage.getExtendedPayload() instanceof SleepPayload);
		SleepPayload sleepPayload = (SleepPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), sleepPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), sleepPayload.getGpsData());
		assertEquals(expectedPayload.getBatteryVoltage(), sleepPayload.getBatteryVoltage(), 0.01);
	}

	@Test
	public void testWriteWakeup() {
		WakeUpPayload expectedPayload = new WakeUpPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime));
		expectedPayload.setBatteryVoltage(11.8);
		expectedPayload.setTypeCode(1);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeWakeup(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 3);
		assertTrue(zteMessage.getExtendedPayload() instanceof WakeUpPayload);
		WakeUpPayload wakeUpPayload = (WakeUpPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), wakeUpPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), wakeUpPayload.getGpsData());
		assertEquals(expectedPayload.getBatteryVoltage(), wakeUpPayload.getBatteryVoltage(), 0.01);
		assertEquals(expectedPayload.getTypeCode(), wakeUpPayload.getTypeCode());
	}

	@Test
	public void testWriteCanNotLocateDevice() {
		ZteEventPayload expectedPayload = new ZteEventPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setGpsData(timeGeneratedGpsData(reportTime.minusSeconds(100)));
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeCanNotLocateDevice(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 4);
		assertTrue(zteMessage.getExtendedPayload() instanceof ZteEventPayload);
		ZteEventPayload zteEventPayload = (ZteEventPayload) zteMessage.getExtendedPayload();
		assertEquals(expectedPayload.getReportTime(), zteEventPayload.getReportTime());
		assertGpsDataEqual(expectedPayload.getGpsData(), zteEventPayload.getGpsData());
	}

	@Test
	public void testWritePowerOnAfterReboot() {
		RebootPayload expectedPayload = new RebootPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setPowerOnTime(reportTime);
		expectedPayload.setLastPowerDownTime(reportTime.minusSeconds(9000));
		expectedPayload.setType(RebootPayload.RebootType.NORMAL);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writePowerOnAfterReboot(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 5);
		assertTrue(zteMessage.getExtendedPayload() instanceof RebootPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteUpgradeState() {
		UpgradeStatusPayload expectedPayload = new UpgradeStatusPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setUpgradeStatus(UpgradeStatusPayload.UpgradeState.DOWNLOADED);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeUpgradeState(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 6);
		assertTrue(zteMessage.getExtendedPayload() instanceof UpgradeStatusPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteAcceleratorCalibrationStatus() {
		AcceleratorStatusPayload expectedPayload = new AcceleratorStatusPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setRecalibrationSuccess(true);
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeAcceleratorCalibrationStatus(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 7);
		assertTrue(zteMessage.getExtendedPayload() instanceof AcceleratorStatusPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteSMS() {
		SmsPayload expectedPayload = new SmsPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setPhone("+44" + RandomStringUtils.randomNumeric(10));
		expectedPayload.setSmsEncoding(RandomStringUtils.randomAlphanumeric(16));
		expectedPayload.setSmsEnded(true);
		expectedPayload.setSmsMessage("A tester message");
		expectedPayload.setSmsTime(reportTime.minusSeconds(1));
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeSMS(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 11);
		assertTrue(zteMessage.getExtendedPayload() instanceof SmsPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	@Test
	public void testWriteDeviceInfo() {
		DeviceInfoPayload expectedPayload = new DeviceInfoPayload();
		Instant reportTime = Instant.ofEpochSecond(Instant.now().getEpochSecond());
		expectedPayload.setReportTime(reportTime);
		expectedPayload.setBtMac(RandomStringUtils.randomNumeric(10));
		expectedPayload.setWifiMac(RandomStringUtils.randomNumeric(10));
		expectedPayload.setIccid(RandomStringUtils.randomNumeric(15));
		expectedPayload.setImsi(RandomStringUtils.randomNumeric(15));
		expectedPayload.setPhone("+44" + RandomStringUtils.randomNumeric(10));
		int frameId = (int) (System.currentTimeMillis() % Short.MAX_VALUE);

		byte[] bytes = eventWriter.writeDeviceInfo(IMEI, KEY, frameId, expectedPayload);

		ZteMessage zteMessage = zteParser.decryptAndParseMessage(bytes, KEY, Optional.empty());

		assertOnZteEventMessage(zteMessage, frameId, 3, 4, 12);
		assertTrue(zteMessage.getExtendedPayload() instanceof DeviceInfoPayload);
		assertEquals(expectedPayload, zteMessage.getExtendedPayload());
	}

	private void assertOnZteEventMessage(ZteMessage zteMessage, int frameId, int expectedFrameType,
			Integer expectedMajor, Integer expectedMinor) {
		assertEquals(IMEI, zteMessage.getImei());
		assertEquals(frameId, zteMessage.getFrameId());
		assertEquals(expectedFrameType, zteMessage.getFrameType());
		assertEquals(expectedMajor, zteMessage.getDataTypeMajor());
		assertEquals(expectedMinor, zteMessage.getDataTypeMinor());
	}

	private void assertGpsDataEqual(GPSData expected, GPSData actual) {
		assertEquals(expected.getLocation().getLat(), actual.getLocation().getLat(), 0.0001);
		assertEquals(expected.getLocation().getLng(), actual.getLocation().getLng(), 0.0001);
		assertEquals(expected.getHeading(), actual.getHeading());
		assertEquals(expected.getAltitude(), actual.getAltitude());
		assertEquals(expected.getTime(), actual.getTime());
		assertEquals(expected.getValidity(), actual.getValidity());
		assertEquals(expected.getSpeedMpS(), actual.getSpeedMpS(), 0.1);
		assertEquals(expected.getPdop(), actual.getPdop(), 0.1);
		assertEquals(expected.getHdop(), actual.getHdop(), 0.1);
		assertEquals(expected.getVdop(), actual.getVdop(), 0.1);
		assertEquals(expected.getSatelliteCount(), actual.getSatelliteCount());

	}

	private GPSData timeGeneratedGpsData(Instant gpsTime) {
		GPSData gpsData = new GPSData();
		gpsData.setAltitude((int) (gpsTime.getEpochSecond() % 50));
		gpsData.setHdop(1.0);
		gpsData.setHeading((int) (gpsTime.toEpochMilli() % 360));
		gpsData.setLocation(
				new LatLng(90.0 - ((gpsTime.toEpochMilli() % 1799999) / 10000.0), 180.0 -((gpsTime.toEpochMilli() % 359999) / 10000d)));
		gpsData.setPdop(1.0);
		gpsData.setSatelliteCount(5);
		gpsData.setSpeedMpS((gpsTime.toEpochMilli() % 279) / 10d);
		gpsData.setTime(gpsTime);
		gpsData.setValidity((int) (gpsTime.toEpochMilli() % 2));
		gpsData.setVdop(1.0);
		return gpsData;
	}

	private List<DTC> generateDtcList() {
		ArrayList<DTC> dtcs = new ArrayList<>();
		int size = (int) (Instant.now().getEpochSecond() % 3);
		for (int i = 0; i < size; i++){
			DTC dtc = new DTC();
			dtc.setCode(RandomStringUtils.randomNumeric(4));
			dtc.setState((int) (Instant.now().toEpochMilli() % 3));
			dtcs.add(dtc);
		}
		return dtcs;
	}
}