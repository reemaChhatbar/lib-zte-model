package com.tantaluminnovations.zte.repository;

import static javax.xml.bind.DatatypeConverter.printHexBinary;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;

import com.tantaluminnovations.ttp.model.FuelType;
import com.tantaluminnovations.zte.client.RivenClient;
import com.tantaluminnovations.zte.model.Device;
import com.tantaluminnovations.zte.model.DeviceType;
import com.tantaluminnovations.zte.model.ZTEParameters;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
/*
 * Use for integration testing
@SpringBootTest(classes=ArgonApp.class,
	properties= {
			"spring.cloud.consul.host=consul.service.consul",
			"cassandra.hosts=cassandra-2-node-0.node.consul",
			"kafka.brokers=kafka-2-0.service.consul:31000",
			"kafka.publish.enabled=false",
			"cluster_name=telematics-shared-ci-1",
			"cluster_id=telematics_shared_ci_1",
			"platformId=telematics",
			"projectId=shared",			
			"environment=ci",
			"namespace=1",
			})
*/
public class ZteEncryptionKeyProviderTest {

	@Mock
	private RivenClient rivenClient;

	private MockEnvironment env = new MockEnvironment();

	private ZteEncryptionKeyProvider beingTested;

	@Before
	public void setup() {
		env.setProperty("argon.zteEncryptionKeyProvider.cacheEnabled", "true");
		env.setProperty("argon.zteEncryptionKeyProvider.cacheExpirySeconds", "3600");
		env.setProperty("argon.zteEncryptionKeyProvider.cacheMaxEntries", "40000");
		beingTested = ZteEncryptionKeyProvider.getSingleton().init(env, rivenClient);
	}

	@Test
	public void testLookupAndAddToCache() throws IOException {

		final String IMEI = "861251030008038";
		final byte[] ENCRYPTION_KEY = new byte[16]; //fake 192 bit encryption key per DES-3

		when(rivenClient.getDevice(IMEI)).thenReturn(getTestDevice(IMEI, ENCRYPTION_KEY));

		Optional<byte[]> result = beingTested.getEncryptionKey(IMEI);

		assertNotNull(result);
		assertTrue(Arrays.equals(ENCRYPTION_KEY, result.get()));

		verify(rivenClient, times(1)).getDevice(IMEI);
	}

	@Test
	public void testCacheLookup() throws IOException {

		final String IMEI = "861473030186693";
		final byte[] ENCRYPTION_KEY = new byte[16];  //fake 192 bit encryption key per DES-3

		when(rivenClient.getDevice(IMEI)).thenReturn(getTestDevice(IMEI, ENCRYPTION_KEY));

		Optional<byte[]> result = beingTested.getEncryptionKey(IMEI); //first lookup
		assertNotNull(result);
		assertTrue(Arrays.equals(ENCRYPTION_KEY, result.get()));

		result = beingTested.getEncryptionKey(IMEI); //second lookup
		assertNotNull(result);
		assertTrue(Arrays.equals(ENCRYPTION_KEY, result.get()));

		verify(rivenClient, times(1)).getDevice(IMEI);
	}

	@Test(expected = IllegalStateException.class)
	public void testNullRivenCLient() throws IOException {
		ZteEncryptionKeyProvider zteEncryptionKeyProvider = ZteEncryptionKeyProvider.getSingleton().init(env, null);
		zteEncryptionKeyProvider.getEncryptionKey("861473030186693");
	}

	@Test
	public void testKeyNotFound() throws IOException {

		final String BAD_IMEI = "UNKNOWN_DEVICE_IMEI";

		when(rivenClient.getDevice(BAD_IMEI)).thenReturn(null);

		Optional<byte[]> result = beingTested.getEncryptionKey(BAD_IMEI);

		assertNotNull(result);

		assertTrue(!result.isPresent());

		verify(rivenClient, times(1)).getDevice(BAD_IMEI);
	}

	@After
	public void teardown() {
		beingTested.shutDownCache();
	}

	private Device getTestDevice(String imei, byte[] encryptionKey) {
		ZTEParameters additionalParameters = new ZTEParameters();
		additionalParameters.setDeviceKey(printHexBinary(encryptionKey));
		Device device = new Device();
		device.setDeviceUuid(imei);
		device.setAdditionalParameters(additionalParameters);
		device.setAgentVersion("agentVersion");
		return device;
	}
}
