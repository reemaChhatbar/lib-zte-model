package com.tantaluminnovations.zte.model;

import com.fasterxml.jackson.databind.*;
import org.apache.commons.lang3.*;
import org.junit.*;

import java.io.*;
import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

public class ZteMessageTest {

	private ObjectMapper objectMapper = new ObjectMapper();
	private Random random = new Random();

	@Test
	public void canSerialiseAndDeserialiseZTEMessageToJson() throws IOException {
		ZteMessage original = generateZteMessage();

		String json = objectMapper.writeValueAsString(original);

		ZteMessage actual = objectMapper.readValue(json, ZteMessage.class);

		assertThat(actual, equalTo(original));
	}

	@Test
	public void canSerialiseAndDeserialiseZTEMessageInformationToJson() throws IOException {
		ZteMessageInformation original = new ZteMessageInformation(generateZteMessage());
		original.setEncryptedBytes(new byte[0]);
		original.setDecryptedBytes(new byte[0]);
		original.setClientHost("localhost");
		original.setClientPort(random.nextInt(100000));

		String json = objectMapper.writeValueAsString(original);

		ZteMessageInformation actual = objectMapper.readValue(json, ZteMessageInformation.class);

		assertThat(actual, equalTo(original));
	}

	private ZteMessage generateZteMessage() {
		ZteMessage original = new ZteMessage();
		original.setFrameType(random.nextInt(8));
		original.setImei(RandomStringUtils.randomNumeric(15));
		original.setFrameId(random.nextInt(10000));
		original.setDataTypeMajor(random.nextInt(242));
		original.setDataTypeMinor(random.nextInt(6));
		return original;
	}

}
