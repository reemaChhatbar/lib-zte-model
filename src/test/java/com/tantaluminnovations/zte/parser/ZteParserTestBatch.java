package com.tantaluminnovations.zte.parser;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.datatype.jsr310.*;
import com.tantaluminnovations.ttp.model.TTPMessage;
import com.tantaluminnovations.zte.adapter.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.repository.*;
import com.tantaluminnovations.zte.service.ZteMessageDecoder;
import com.tantaluminnovations.zte.util.*;
import com.tantaluminnovations.zte.writer.*;
import org.junit.*;
import org.springframework.util.*;

import java.io.*;
import java.util.*;

import static javax.xml.bind.DatatypeConverter.*;

public class ZteParserTestBatch {

	//This class is for help debugging zte model, tests are ignored because they are not acceptance tests!

	ZteParser zteParser = new ZteParser();
	ZteTtpAdapter ztettpAdapter = new ZteTtpAdapter();
	ZteWriter zteWriter = new ZteWriter();
	ServerToDeviceWriter serverToDeviceWriter = new ServerToDeviceWriter();
	DeviceToServerResponseWriter deviceToServerResponseWriter = new DeviceToServerResponseWriter();

	ObjectMapper mapper = new ObjectMapper()
		.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
		.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
		.disable(DeserializationFeature.ACCEPT_FLOAT_AS_INT)
		.setSerializationInclusion(JsonInclude.Include.NON_NULL)
		.registerModules(new JavaTimeModule());


	@Ignore @Test
	public void testArgonMessages() throws JsonProcessingException {
		byte[] key = parseHexBinary("27006aede4cd5b2ec4e69cf17b83edd7af9407ae14cb3753");
		for (String hex: HexMessages.argonMessages) {
			testArgonMessage(hex, key, Optional.of(new MessageVersion(2, 1)));
		}
	}

	private void testArgonMessage(String hexString, byte[] key, Optional<MessageVersion> messageVersion) throws JsonProcessingException {
		System.out.println("Argon message Hex String: " + hexString);
		ZteMessageInformation messageInfo = zteParser.decryptAndParseBasicMessage(parseHexBinary(hexString), key,
				messageVersion);
		System.out.println("MessageInfo: "+ mapper.writeValueAsString(messageInfo));
		ZteMessage ztemessage = zteParser.decryptAndParseMessage(parseHexBinary(hexString), key,
				messageVersion);
		//System.out.println("Argon message decoded, majorType="+ztemessage.getDataTypeMajor()+" minorType="+ztemessage.getDataTypeMinor());
		System.out.println("Argon message decoded: " + mapper.writeValueAsString(ztemessage));

		/*assertThat(ztemessage.getImei(), notNullValue());
		if(ztemessage.getFrameType() == 3){
			List<TTPMessage> ttpMessages = ztettpAdapter.convertToTTPMessage(ztemessage);
			ttpMessages.stream()
					.forEach(ttpMessage -> {
						try {
							System.out.println("Argon message adapted to ttp: " + mapper
									.writeValueAsString(ttpMessage));
						} catch (JsonProcessingException e) {
							e.printStackTrace();
						}
					});
		} else {
			System.out.println("Argon message is not a publication message");
		}*/
	}

	@Ignore @Test
	public void testIndividualArgonMessage() throws JsonProcessingException {
		System.out.println(Integer.class.isAssignableFrom(String.class));
		byte[] key = parseHexBinary("27006aede4cd5b2ec4e69cf17b83edd7af9407ae14cb3753");
		String hexMessage = "555500553836313235313033303030383033387A604A3E8CABBBE0AC59DDB06F8488D9F47BD2A8825540D6BA96BA8F212260918234A41E35E149481604F9581F225B7F6D16943F0191C26C2ADA5A41C09E4928AAAA" ;
		System.out.println(zteParser.getDeviceId(parseHexBinary(hexMessage)));
		TTPMessage type1 = ztettpAdapter.convertToTTPMessage(
				zteParser.decryptAndParseMessage(parseHexBinary(hexMessage), key, Optional.empty())).get(0);
		System.out.println(type1.asType1().getAgentVersion());
	}

	@Ignore @Test
	public void testDecryptIndividualArgonMessage(){
		byte[] key = parseHexBinary("a31d82583f273c92b393ee6bd058264c9f0f8e2997fe4f68");
		ZteMessageInformation decryptMesage = zteParser.decryptAndParseBasicMessage(parseHexBinary("55550075383631343733303330313835343438121625E7839B352BD285BF165ABB62EDD0E29FF574FBC364C3E43973CE6311B7585E2E79B1E7CF4FB9400132B0B9A4DDA87614F7529040FB74285B4CF4BC6BA5419E5AA4BEE8E400D70E9AB8EE3AF932CB2B9518C548D9635D5E0F93A07342AFAAAA"), key,
				Optional.empty());
		byte[] decryptBytes = decryptMesage.getDecryptedBytes();
		String decryptString = printHexBinary(decryptBytes);
		System.out.println(decryptString);
	}

	@Ignore @Test
	public void testWriteMessage() throws IOException {
		byte[] key = parseHexBinary("a0560465509522a05c6b426427494e49b9c978d67c4159b7");
		String imei = "861473030188343";
		byte[] file = getFile("tester_VM6200S.bin");
		int i = 0;
		byte[] filePacket = Arrays.copyOfRange(file, i, i + 960);
		byte[] bytes = deviceToServerResponseWriter.writeFileUpdate(imei, key, 1, 220192, filePacket);
		//byte[] bytes = serverToDeviceWriter.writeFullParameterInquiry(imei, 12, key);
		String hexMessage = printHexBinary(bytes);
		System.out.println(hexMessage);
		ZteMessageInformation zteMessageInformation = zteParser.decryptAndParseBasicMessage(bytes, key,
				Optional.empty());
		System.out.println(zteMessageInformation);

	}

	private byte[] getFile(String fileName) throws IOException {
		byte[] file;
		InputStream inputStream = ZteMessageDecoder.class.getResourceAsStream("/" + fileName);
		file = StreamUtils.copyToByteArray(inputStream);
		return file;
	}



}
