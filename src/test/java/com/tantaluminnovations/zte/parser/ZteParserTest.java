package com.tantaluminnovations.zte.parser;

import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;
import org.junit.*;

import java.util.*;

import static javax.xml.bind.DatatypeConverter.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class ZteParserTest {

	public static final String IMEI = "861251030008038";
	private static final String IMEI2 = "861473030186693";
	private static final String IMEI3 = "861473030187204";

	ZteParser zteParser = new ZteParser();

	private byte[] key = parseHexBinary("27006aede4cd5b2ec4e69cf17b83edd7af9407ae14cb3753"); //IMEI = 861251030008038
	private byte[] key2 = parseHexBinary("2f18747f9372c841b848f10660e95fc91f40c6894a8b8ef7"); //IMEI = 861473030186693
	private byte[] key3 = parseHexBinary("b5a9b2f97eead9bcd13e5edfc5ca2216b2a2bf9fac48a1a7"); //IMEI = 861473030187204

	String connectionMessage = "555500553836313235313033303030383033387A604A3E8CABBBE0AC59DDB06F8488D9F47BD2A8825540D6BA96BA8F212260918234A41E35E149481604F9581F225B7F6D16943F0191C26C2ADA5A41C09E4928AAAA";

	//Publish Messages
	//Trip
	String ignitionOn = "55550055383631323531303330303038303338F7BFB21C36BF52A70DD5304461441CF5566DF9F590AE9C5D8A851573B815A0635303360CE9864C2F8E35A9CD4EDFD81138750C9B8E90707F93FE076A239431D2AAAA";
	String ignitionOff = "555500553836313235313033303030383033382AD868E154D8F72EEA996BAF95752E2A1CC82E569B0F5EFF75B642550800FF90A1CA1DC9D0BF4EF38E35A9CD4EDFD811912EEBAC5C933F63C1BDA290CDBA76B0AAAA";
	String tripSummary = "55550075383631323531303330303038303338377BBD587F7981ABF939B987F5E89AA0C22D376E83CE6C273855A199C1E3BDFFF1E697F0BD09831B18CCF368B1C55DA54FC63767E5114E7A62953B91D171121A770D3EB2626B810A8A4C43FDE854830317AD614981C0CE1F073960B6409D91CEAAAA";
	//Driving Event
	String suddenAcceleration = "555500453836313437333033303138363639332DD9D07506761957236FB084BB161C13833AD5B6C093DFA8D941CBFD4BF5D6D3ED2E372C49E0277822C2230240AC3A28AAAA";
	String suddenDeceleration = "55550045383631343733303330313837323034665466666BF9FE53065E07AAB7C4E101928E230B18F08DBD8BD6015AABB28107BFC1CAC7B48BB3178E700DB7EBF2C560AAAA";
	String sharpTurn = "";
	String exceedIdle = "5555003D383631343733303330313836363933F594E99179D9E8E9450CFB216F9B51A3CD5947786BA0C6569FBC685294306C0C71E8A744094CD512AAAA";
	String drivingTired = "";
	//GpsData
	String gpsData = "";
	//Vehicle Data
	String vin = "5555003D383631323531303330303038303338A7274EDD85CDADEBFF2BB5B7A54E6AC589622D25294F7603FD443442D0B79F853C28DFF190296173AAAA";
	String vehicleDataFlow = "55550065383631323531303330303038303338E44A7C555F80D95AB55E4FE4F11FF85C566DF9F590AE9C5D8A851573B815A06358C870B47E2DDF0F93F9DF0A09DF6C537F28504474EA76783BCACE7D473D0CDD7615C4CA90BB43CC040D3C945641785CAAAA";
	//Vehicle Security
	String dtcCodes = "5555002D383631343733303330313837323034FFD4CC2B132F7C2F366CB50E1A02B77F7ADABC8075A0403FAAAA";
	String lowVoltage = "5555003D383631343733303330313837323034F2D9EC040E51E170D1186D0388D98A4E4FE85F05B3A2B67A44DCBFB2863E622B4DB9C7DA84CA9B80AAAA";
	String vibrationWhilstOff = "5555004538363132353130333030303830333844A2256293EB86E359A69FF74E349BC2C72414CF8244FA9EE9D843A4080059E938CBD4AC036B61DD753CC722DB57B454AAAA";
	String suspectedCollision = "";
	String deviceDisconnected = "5555003D38363132353130333030303830333879FA439EDDCCD5E27BF9CFBCB421BE1B6E0F18BE1E3BF3F08D41F28A65AB614DE895570D19893730AAAA";
	String suspectedTow = "";
	//Device State Messages;
	String deviceBug = "";
	String deviceSleep = "5555003D383631323531303330303038303338BE11F78D72F1FAD1C9912C62C48DC206359C754C495E9902931779C16EBF0EAC9165C4D2B942FFBDAAAA";
	String deviceWakeup = "55550045383631323531303330303038303338973B0F8727E5C8E8121BAA4872286320B2FEDB1C44B7E3C3D88ECD4D8D61A40B9C4BA61944CAD179932C09170E5C60D0AAAA";
	String noGpsData = "5555003D383631323531303330303038303338A84740DEB90B6233E613B4E34DB237F7C22D376E83CE6C273855A199C1E3BDFF3BCC6661E50786A2AAAA";
	String deviceReboot = "5555002D383631323531303330303038303338608BD90D702769753C42E1320017C5CE5B525176B2039136AAAA";
	String upgradeStatus = "";
	String acceleratorCalibrationStatus = "";
	String deviceInfo = "555500753836313235313033303030383033382DF980506DEC6A020C51DCD61B3FF8130F63EA2D1D9121543E51B4329DA44062E5697F5EAFEE228C81E3ED6E89A959D3209386EF5B7EE7733B5BDA87B20F415F8F1152F82038C7D15AA69213BDB65C02273108477541533F9395509AA400BEE1AAAA";
	//Device Request to server messages
	String updateRequest = "";
	String agpsRequest = "5555002D383631323531303330303038303338C4758361C4E5F880088FF415C3D8CDF039DBDC82E65E1042AAAA";

	//Server Request to Device Responses


	@Test
	public void testGetDeviceKey(){
		String imei = zteParser.getDeviceId(parseHexBinary(connectionMessage));

		assertThat(imei, equalTo(IMEI));
	}

	@Test
	public void testParseConnectionMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(connectionMessage), key,
				Optional.of(new MessageVersion(2, 1)));

		assertThat(message.getImei(), equalTo(IMEI));
		assertThat(message.getFrameType(),  is(0x01));
	}

	@Ignore @Test
	public void testSeparatedDecryptAndParseBasicMessage(){
		ZteMessage expected = zteParser.decryptAndParseMessage(parseHexBinary(tripSummary), key,
				Optional.of(new MessageVersion(2, 1)));

		ZteMessageInformation zteMessageInformation = zteParser.decryptAndParseBasicMessage(parseHexBinary(tripSummary), key,
				Optional.empty());
		ZteMessage actual = zteParser.parseUnencryptedMessage(zteMessageInformation.getDecryptedBytes(),
				Optional.of(new MessageVersion(2, 1)));

		assertThat(expected, equalTo(actual));
	}

	@Test
	public void testParseTripSummaryMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(ignitionOn), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 0, 1, IgnitionOnOffPayload.class);
	}

	@Test
	public void testParseIgnitionOnMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(ignitionOn), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 0, 1, IgnitionOnOffPayload.class);
	}

	@Test
	public void testParseIgnitionOffMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(ignitionOff), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 0, 2, IgnitionOnOffPayload.class);
	}

	@Test
	public void testParseHardAccelerationMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(suddenAcceleration), key2,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI2, 1, 0, RapidAccelDecelPayload.class);
	}

	@Test
	public void testParseHardDecelerationMessage() {
		System.out.println(zteParser.getDeviceId(parseHexBinary(suddenDeceleration)));
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(suddenDeceleration), key3,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI3, 1, 1, RapidAccelDecelPayload.class);
	}

	@Test
	public void testParseExceedIdleMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(exceedIdle), key2,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI2, 1, 3, ZteEventPayload.class);
	}

	@Test
	public void testParseVinMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(vin), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 3, 0, VINPayload.class);
	}

	@Test
	public void testParseVehicleDataFlowMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(vehicleDataFlow), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 3, 1, VehicleDataFlowPayload.class);
	}

	@Test
	public void testParseDeviceSleepMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(deviceSleep), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 4, 2, SleepPayload.class);
	}

	@Test
	public void testParseDeviceWakeUpMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(deviceWakeup), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 4, 3, WakeUpPayload.class);
	}

	@Test
	public void testParseNoGpsDataMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(noGpsData), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 4, 4, ZteEventPayload.class);
	}

	@Test
	public void testParseDeviceRebootMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(deviceReboot), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 4, 5, RebootPayload.class);
	}

	@Test
	public void testParseDeviceInformationMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(deviceInfo), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 4, 12, DeviceInfoPayload.class);
	}

	@Test
	public void testParseVibrationAfterIgnitionOffMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(vibrationWhilstOff), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 5, 2, VibrationPayload.class);
	}

	@Test
	public void testParseDeviceDisconnectedMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(deviceDisconnected), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 5, 7, DeviceDisconnectPayload.class);
	}

	@Test
	public void testParseAgpsRequestMessage() {
		ZteMessage message = zteParser.decryptAndParseMessage(parseHexBinary(agpsRequest), key,
				Optional.of(new MessageVersion(2, 1)));

		assertOnPublicationMessage(message, IMEI, 240, 2, AgpsRequestPayload.class);
	}

	private void assertOnPublicationMessage(ZteMessage message, String imei, int dataTypeMajor, int dataTypeMinor, Class payloadType) {
		assertThat(message.getImei(), equalTo(imei));
		assertThat(message.getFrameType(),  is(0x03));
		assertThat(message.getDataTypeMajor(), is(dataTypeMajor));
		assertThat(message.getDataTypeMinor(), is(dataTypeMinor));
		assertThat(message.getExtendedPayload(), instanceOf(payloadType));
	}



}