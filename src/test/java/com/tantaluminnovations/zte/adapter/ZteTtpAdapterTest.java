package com.tantaluminnovations.zte.adapter;

import com.tantaluminnovations.ttp.model.*;
import com.tantaluminnovations.ttp.model.Reason;
import com.tantaluminnovations.ttp.model.extended.*;
import com.tantaluminnovations.zte.model.*;
import com.tantaluminnovations.zte.model.payload.*;
import org.apache.commons.lang3.*;
import org.junit.*;

import java.time.*;
import java.util.*;

import static com.tantaluminnovations.ttp.model.Reason.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.IsInstanceOf.*;

public class ZteTtpAdapterTest {

	Random random = new Random();
	private ZteTtpAdapter adapter = new ZteTtpAdapter();

	@Test
	public void shouldConvertTripSummaryFromZteToTtp(){
		int dataTypeMajor = 0;
		int dataTypeMinor = 0;

		TripSummaryPayload ztePayload = new TripSummaryPayload();
		Instant ignitionOffTime = Instant.now();
		Instant ignitionOnTime = ignitionOffTime.minusSeconds(random.nextInt(10000));
		ztePayload.setIgnitionOn(ignitionOnTime);
		GPSData ignitionOnGpsData = getRandomGpsData(ignitionOnTime);
		ztePayload.setIgnitionOnGPSData(ignitionOnGpsData);
		ztePayload.setIgnitionOff(ignitionOffTime);
		GPSData ignitionOffGpsData = getRandomGpsData(ignitionOffTime);
		ztePayload.setIgnitionOffGPSData(ignitionOffGpsData);
		ztePayload.setDistanceSource(random.nextInt(2));
		ztePayload.setDistanceMetres(random.nextInt(250000));
		ztePayload.setMaxSpeedKmH(random.nextInt(80));
		ztePayload.setFuelUsedMl(random.nextInt(25000));
		ztePayload.setIdleTimeSec(random.nextInt(500));
		ztePayload.setIdleFuelUsedMl(random.nextInt(1250));
		ztePayload.setRapidAccelerationCount(random.nextInt(10));
		ztePayload.setRapidDecelerationCount(random.nextInt(10));
		ztePayload.setHardCorneringCount(random.nextInt(10));
		ztePayload.setHistoricalDistanceMetres(random.nextInt());
		ztePayload.setHistoricalFuelUsedMl(random.nextInt());
		ztePayload.setHistoricalTimeDrivingSecs(random.nextInt());

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);

		for (TTPMessage actual: messages) {
			switch (actual.getReason()){
			case TRIP_START:
				assertOnTtpMessage(actual, zteMessage, TRIP_START, ignitionOnTime, ignitionOnGpsData);
				break;
			case TRIP_END:
				assertOnTtpMessage(actual, zteMessage, TRIP_END, ignitionOffTime, ignitionOffGpsData);
				assertThat(actual.getExtendedPayload(), instanceOf(RC4ExtendedPayload.class));
				RC4ExtendedPayload actualPayload = (RC4ExtendedPayload) actual.getExtendedPayload();
				assertThat(actualPayload.getTripDistanceMeters(), is((double) ztePayload.getDistanceMetres()));
				assertThat(actualPayload.getTripFuelMl(), is(ztePayload.getFuelUsedMl().doubleValue()));
				assertThat(actualPayload.getTripIdleTimeSeconds(), is(ztePayload.getIdleTimeSec()));
				assertThat(actualPayload.getTripMaxSpeedKph(), is(ztePayload.getMaxSpeedKmH()));
				break;
			default:
				throw new AssertionError("Unexpected Reason - "+actual.getReason());
			}
		}

	}

	@Test
	public void shouldConvertIgnitionOnFromZteToTtp(){
		int dataTypeMajor = 0;
		int dataTypeMinor = 1;

		IgnitionOnOffPayload ztePayload = new IgnitionOnOffPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		ztePayload.setType(random.nextInt(2));
		ztePayload.setDtcs(getDtcs("0"));
		ztePayload.setPrivateDtcs(getDtcs("1"));

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		for (TTPMessage actual: messages) {
			switch (actual.getReason()){
			case IGNITION_ON:
				assertOnTtpMessage(actual, zteMessage, IGNITION_ON, eventTime, gpsData);
				break;
			case OBD_FAULT_CODES:
				assertOnTtpMessage(actual, zteMessage, OBD_FAULT_CODES, eventTime, gpsData);
				//TODO assert On DTC data
				break;
			default:
				throw new AssertionError("Unexpected Reason - "+actual.getReason());
			}
		}


	}

	@Test
	public void shouldConvertIgnitionOffFromZteToTtp(){
		int dataTypeMajor = 0;
		int dataTypeMinor = 2;

		IgnitionOnOffPayload ztePayload = new IgnitionOnOffPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		ztePayload.setType(random.nextInt(2));
		ztePayload.setDtcs(getDtcs("0"));
		ztePayload.setPrivateDtcs(getDtcs("1"));

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		for (TTPMessage actual: messages) {
			switch (actual.getReason()){
			case IGNITION_OFF:
				assertOnTtpMessage(actual, zteMessage, IGNITION_OFF, eventTime, gpsData);
				break;
			case OBD_FAULT_CODES:
				assertOnTtpMessage(actual, zteMessage, OBD_FAULT_CODES, eventTime, gpsData);
				//TODO assert On DTC data
				break;
			default:
				throw new AssertionError("Unexpected Reason - "+actual.getReason());
			}
		}
	}

	@Test
	public void shouldConvertRapidAccelerationFromZteToTtp(){
		int dataTypeMajor = 1;
		int dataTypeMinor = 0;

		RapidAccelDecelPayload ztePayload = new RapidAccelDecelPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		ztePayload.setInitialSpeedKmH(random.nextInt(30));
		ztePayload.setFinalSpeedKmH(random.nextInt(50)+30);
		ztePayload.setAccelerationMpS2((random.nextInt(20)+20) * 0.1);

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);

		assertOnTtpMessage(actual, zteMessage, EXCESSIVE_ACCELERATION, eventTime, gpsData);

		assertThat(actual.getExtendedPayload(), instanceOf(RC42_43_44ExtendedPayload.class));
		RC42_43_44ExtendedPayload actualPayload = (RC42_43_44ExtendedPayload) actual.getExtendedPayload();
	}

	@Test
	public void shouldConvertRapidDecelerationFromZteToTtp(){
		int dataTypeMajor = 1;
		int dataTypeMinor = 1;

		RapidAccelDecelPayload ztePayload = new RapidAccelDecelPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		ztePayload.setInitialSpeedKmH(random.nextInt(30));
		ztePayload.setFinalSpeedKmH(random.nextInt(50)+30);
		ztePayload.setAccelerationMpS2((random.nextInt(20)+20) * 0.1);

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, EXCESSIVE_DECELERATION, eventTime, gpsData);

		assertThat(actual.getExtendedPayload(), instanceOf(RC42_43_44ExtendedPayload.class));
		RC42_43_44ExtendedPayload actualPayload = (RC42_43_44ExtendedPayload) actual.getExtendedPayload();
	}

	@Test
	public void shouldConvertHardCorneringFromZteToTtp(){
		int dataTypeMajor = 1;
		int dataTypeMinor = 2;

		HardCorneringPayload ztePayload = new HardCorneringPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		ztePayload.setTurningAccelerationGpS2((random.nextInt(20)+20) * 0.1);

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, EXCESSIVE_ROTATION, eventTime, gpsData);

		assertThat(actual.getExtendedPayload(), instanceOf(RC42_43_44ExtendedPayload.class));
		RC42_43_44ExtendedPayload actualPayload = (RC42_43_44ExtendedPayload) actual.getExtendedPayload();
	}

	@Test
	public void shouldConvertGpsDataFromZteToTtp(){
		int dataTypeMajor = 2;
		int dataTypeMinor = 0;

		GPSDataPayload ztePayload = new GPSDataPayload();
		Instant messageTime = Instant.now();
		GPSData messageGps = getRandomGpsData(messageTime);
		GPSData lastGps = messageGps;
		List<GPSData> gpsDataPoints = new ArrayList<>();
		gpsDataPoints.add(lastGps);
		for(int i = 1; i < random.nextInt(60)+1 ; i++){
			double newLat = lastGps.getLocation().getLat() + (random.nextInt(20) * 0.0001);
			double newLng = lastGps.getLocation().getLng() + (random.nextInt(20) * 0.0001);
			LatLng latLng = new LatLng(newLat, newLng);
			lastGps = getRandomGpsData(messageTime.minusSeconds(i), latLng);
			gpsDataPoints.add(lastGps);
		}
		ztePayload.setGpsDataPoints(gpsDataPoints);

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, ENHANCED_HIGH_FREQUENCY_LOG, messageTime, messageGps);

		assertThat(actual, instanceOf(Type3Message.class));
		assertThat(actual.getExtendedPayload(), instanceOf(RC130ExtendedPayload.class));
		RC130ExtendedPayload actualPayload = (RC130ExtendedPayload) actual.getExtendedPayload();

		assertThat(actualPayload.getDeltaCount(), is(gpsDataPoints.size()));
		assertThat(actualPayload.getLogs().size(), is(gpsDataPoints.size()));
	}

	@Test
	public void shouldConvertVinFromZteToTtp(){
		int dataTypeMajor = 3;
		int dataTypeMinor = 0;

		VINPayload ztePayload = new VINPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		ztePayload.setVinLength(17);
		ztePayload.setVin(RandomStringUtils.randomAlphanumeric(17));
		ztePayload.setProtocolType(random.nextInt(100));

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, VEHICLE_SIG, eventTime, null);

		assertThat(actual.getExtendedPayload(), instanceOf(RC114ExtendedPayload.class));
		RC114ExtendedPayload actualPayload = (RC114ExtendedPayload) actual.getExtendedPayload();
	}

	@Test
	public void shouldConvertDTCsFromZteToTtp(){
		int dataTypeMajor = 5;
		int dataTypeMinor = 0;

		DTCPayload ztePayload = new DTCPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);

		List<DTC> obdDtcs = getDtcs("0");
		ztePayload.setObdDtcs(obdDtcs);
		ztePayload.setObdDtcCount(obdDtcs.size());
		List<DTC> privateDtcs = getDtcs("1");
		ztePayload.setPrivateDtcs(privateDtcs);
		ztePayload.setPrivateDtcCount(privateDtcs.size());

		List<DTC> allDTcs = new ArrayList<>(obdDtcs);
		allDTcs.addAll(privateDtcs);
		int dtcCount = (int) allDTcs.stream().filter(dtc -> dtc.getState() == 0 || dtc.getState() == 2).count();
		int ptcCount = allDTcs.size() - dtcCount;

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, OBD_FAULT_CODES, eventTime, null);

		assertThat(actual.getExtendedPayload(), instanceOf(RC116ExtendedPayload.class));
		RC116ExtendedPayload actualPayload = (RC116ExtendedPayload) actual.getExtendedPayload();

		assertThat(actualPayload.getDtcCount(), is(dtcCount));
		assertThat(actualPayload.getDtcs().size(), is(dtcCount));
		assertThat(actualPayload.getPtcCount(), is(ptcCount));
		assertThat(actualPayload.getPtcs().size(), is(ptcCount));
	}

	@Test @Ignore //Conversion of low battery messages has been removed for now
	public void shouldConvertLowBatteryFromZteToTtp(){
		int dataTypeMajor = 5;
		int dataTypeMinor = 1;

		LowBatteryPayload ztePayload = new LowBatteryPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		double voltage = (random.nextInt(12) + 109) * 0.1;
		ztePayload.setVoltage(voltage);

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, PRIMARY_SUPPLY_LOW, eventTime, gpsData);
	}

	@Test @Ignore //Conversion of low battery messages has been removed for now
	public void shouldConvertNoBatteryFromZteToTtp(){
		int dataTypeMajor = 5;
		int dataTypeMinor = 1;

		LowBatteryPayload ztePayload = new LowBatteryPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		double voltage = random.nextInt(108) * 0.1;
		ztePayload.setVoltage(voltage);

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);

		assertOnTtpMessage(actual, zteMessage, LOSS_OF_PRIMARY_SUPPLY, eventTime, gpsData);
	}

	@Test
	public void shouldConvertVibrationAfterIgnitionOffFromZteToTtp(){
		int dataTypeMajor = 5;
		int dataTypeMinor = 2;

		VibrationPayload ztePayload = new VibrationPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		ztePayload.setMagnitudeMG(random.nextInt(1000));

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, UNEXPECTED_VEHICLE_MOVEMENT, eventTime, gpsData);
	}

	@Test
	public void shouldConvertSuspectedCollisionBelow3gFromZteToTtp(){
		int dataTypeMajor = 5;
		int dataTypeMinor = 5;

		CollisionPayload ztePayload = new CollisionPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		ztePayload.setMagnitudeG(random.nextInt(30)*0.1);

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, IMPACT_DETECTED, eventTime, gpsData);
		assertThat(actual.getExtendedPayload(), instanceOf(RC131ExtendedPayload.class));
	}

	@Test
	public void shouldConvertSuspectedCollisionAbove3gFromZteToTtp(){
		int dataTypeMajor = 5;
		int dataTypeMinor = 5;

		CollisionPayload ztePayload = new CollisionPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);
		ztePayload.setMagnitudeG((random.nextInt(30)*0.1) + 3.0);

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, CRASH_DETECTION, eventTime, gpsData);
		assertThat(actual.getExtendedPayload(), instanceOf(RC103ExtendedPayload.class));
	}

	@Test
	public void shouldConvertDeviceDisconnectedFromZteToTtp(){
		int dataTypeMajor = 5;
		int dataTypeMinor = 7;

		ZteEventPayload ztePayload = new ZteEventPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		GPSData gpsData = getRandomGpsData(eventTime);
		ztePayload.setGpsData(gpsData);

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, LOSS_OF_PRIMARY_SUPPLY, eventTime, gpsData);
	}

	@Test
	public void shouldConvertSuspectdTowFromZteToTtp(){
		int dataTypeMajor = 5;
		int dataTypeMinor = 8;

		ZteEventPayload ztePayload = new ZteEventPayload();
		Instant eventTime = Instant.now();
		ztePayload.setReportTime(eventTime);
		ztePayload.setGpsData(getRandomGpsData(eventTime));

		ZteMessage zteMessage = getZTEMessage(dataTypeMajor, dataTypeMinor, ztePayload);

		List<TTPMessage> messages = adapter.convertToTTPMessage(zteMessage);
		TTPMessage actual = messages.get(0);;

		assertOnTtpMessage(actual, zteMessage, UNEXPECTED_VEHICLE_MOVEMENT, eventTime, null);
	}

	private List<DTC> getDtcs(String firstChar) {
		List<DTC> obdDtcs = new ArrayList<>();
		for(int i =0; i < random.nextInt(10)+1; i++){
			DTC dtc = new DTC();
			String code = firstChar + RandomStringUtils.randomNumeric(3);
			dtc.setCode(code);
			int state = random.nextInt(3);
			dtc.setState(state);
			obdDtcs.add(dtc);
		}
		return obdDtcs;
	}

	//TODO: fix this properly
	private void assertOnTtpMessage(TTPMessage actual, ZteMessage zteMessage, Reason tripEnd, Instant created,
			GPSData gpsData) {
		assertThat(actual.getHeader().getHostMsgId(), is(zteMessage.getFrameId()));
		assertThat(actual.getUnitSerialNumber(), is(zteMessage.getImei()));
		assertThat(actual.getReason(), is(tripEnd));
	}

	private ZteMessage getZTEMessage(int dataTypeMajor, int dataTypeMinor, ZteExtendedPayload payload) {
		ZteMessage zteMessage = new ZteMessage();
		zteMessage.setFrameId(random.nextInt(10000));
		zteMessage.setImei(RandomStringUtils.randomNumeric(15));
		zteMessage.setDataTypeMajor(dataTypeMajor);
		zteMessage.setDataTypeMinor(dataTypeMinor);
		zteMessage.setExtendedPayload(payload);
		return zteMessage;
	}

	private GPSData getRandomGpsData(Instant ignitionOnTime) {
		LatLng location = new LatLng((random.nextInt(1800000) * 0.0001) - 90.0,
				(random.nextInt(3600000) * 0.0001) - 180.0);
		return getRandomGpsData(ignitionOnTime, location);
	}

	private GPSData getRandomGpsData(Instant ignitionOnTime, LatLng location) {
		GPSData gpsData = new GPSData();
		gpsData.setTime(ignitionOnTime);
		gpsData.setHeading(random.nextInt(360));
		gpsData.setLocation(location);
		gpsData.setAltitude(random.nextInt(10000));
		gpsData.setSatelliteCount(random.nextInt(6));
		gpsData.setSpeedMpS(random.nextInt(222)*0.1);
		gpsData.setValidity(random.nextInt(2));
		return gpsData;
	}

}